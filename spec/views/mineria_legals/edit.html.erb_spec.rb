require 'spec_helper'

describe "mineria_legals/edit" do
  before(:each) do
    @mineria_legal = assign(:mineria_legal, stub_model(MineriaLegal,
      :AMENAZA_ID => 1,
      :MIN_LEG_CONSECIONADO => "MyString",
      :MIN_LEG_CODIGO => "MyString",
      :MIN_LEG_EXT_CONSECION => 1.5,
      :MIN_LEG_EXPLOTACION => 1.5,
      :MIN_LEG_TIPO => "MyString",
      :MIN_LEG_ESTADO => "MyString"
    ))
  end

  it "renders the edit mineria_legal form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => mineria_legals_path(@mineria_legal), :method => "post" do
      assert_select "input#mineria_legal_AMENAZA_ID", :name => "mineria_legal[AMENAZA_ID]"
      assert_select "input#mineria_legal_MIN_LEG_CONSECIONADO", :name => "mineria_legal[MIN_LEG_CONSECIONADO]"
      assert_select "input#mineria_legal_MIN_LEG_CODIGO", :name => "mineria_legal[MIN_LEG_CODIGO]"
      assert_select "input#mineria_legal_MIN_LEG_EXT_CONSECION", :name => "mineria_legal[MIN_LEG_EXT_CONSECION]"
      assert_select "input#mineria_legal_MIN_LEG_EXPLOTACION", :name => "mineria_legal[MIN_LEG_EXPLOTACION]"
      assert_select "input#mineria_legal_MIN_LEG_TIPO", :name => "mineria_legal[MIN_LEG_TIPO]"
      assert_select "input#mineria_legal_MIN_LEG_ESTADO", :name => "mineria_legal[MIN_LEG_ESTADO]"
    end
  end
end
