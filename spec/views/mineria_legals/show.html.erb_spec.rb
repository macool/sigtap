require 'spec_helper'

describe "mineria_legals/show" do
  before(:each) do
    @mineria_legal = assign(:mineria_legal, stub_model(MineriaLegal,
      :AMENAZA_ID => 1,
      :MIN_LEG_CONSECIONADO => "Min Leg Consecionado",
      :MIN_LEG_CODIGO => "Min Leg Codigo",
      :MIN_LEG_EXT_CONSECION => 1.5,
      :MIN_LEG_EXPLOTACION => 1.5,
      :MIN_LEG_TIPO => "Min Leg Tipo",
      :MIN_LEG_ESTADO => "Min Leg Estado"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Min Leg Consecionado/)
    rendered.should match(/Min Leg Codigo/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/Min Leg Tipo/)
    rendered.should match(/Min Leg Estado/)
  end
end
