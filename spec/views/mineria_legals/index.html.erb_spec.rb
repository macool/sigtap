require 'spec_helper'

describe "mineria_legals/index" do
  before(:each) do
    assign(:mineria_legals, [
      stub_model(MineriaLegal,
        :AMENAZA_ID => 1,
        :MIN_LEG_CONSECIONADO => "Min Leg Consecionado",
        :MIN_LEG_CODIGO => "Min Leg Codigo",
        :MIN_LEG_EXT_CONSECION => 1.5,
        :MIN_LEG_EXPLOTACION => 1.5,
        :MIN_LEG_TIPO => "Min Leg Tipo",
        :MIN_LEG_ESTADO => "Min Leg Estado"
      ),
      stub_model(MineriaLegal,
        :AMENAZA_ID => 1,
        :MIN_LEG_CONSECIONADO => "Min Leg Consecionado",
        :MIN_LEG_CODIGO => "Min Leg Codigo",
        :MIN_LEG_EXT_CONSECION => 1.5,
        :MIN_LEG_EXPLOTACION => 1.5,
        :MIN_LEG_TIPO => "Min Leg Tipo",
        :MIN_LEG_ESTADO => "Min Leg Estado"
      )
    ])
  end

  it "renders a list of mineria_legals" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Min Leg Consecionado".to_s, :count => 2
    assert_select "tr>td", :text => "Min Leg Codigo".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Min Leg Tipo".to_s, :count => 2
    assert_select "tr>td", :text => "Min Leg Estado".to_s, :count => 2
  end
end
