require 'spec_helper'

describe "sitio_turisticos/index" do
  before(:each) do
    assign(:sitio_turisticos, [
      stub_model(SitioTuristico,
        :GEO_ID => 1,
        :AREA_ID => 2,
        :TUR_NOMBRE => "Tur Nombre",
        :TUR_ATRACTIVO_PRINCIPAL => "Tur Atractivo Principal",
        :TUR_OTROS_ATRACTIVOS => "Tur Otros Atractivos",
        :TUR_TIPO_CLIMA => "Tur Tipo Clima",
        :TUR_EPOCA_LLUVIAS => "Tur Epoca Lluvias",
        :TUR_CAP_CARGA => 3,
        :TUR_SITIO_INGRESO => "Tur Sitio Ingreso",
        :TUR_DIST_SENDERO => 1.5,
        :TUR_DIST_CARRETERA => 1.5,
        :TUR_DIST_OTRO => 1.5,
        :TUR_DIST_ADMIN_CERCANO => 1.5,
        :TUR_ACAMPAR_NUM => 4,
        :TUR_ALOJAMIENTO_NUM => 5,
        :TUR_SENDERO_AUTO_NUM => 6,
        :TUR_OTROS_SENDEROS_NUM => 7,
        :TUR_CENTRO_INTERP_NUM => 8
      ),
      stub_model(SitioTuristico,
        :GEO_ID => 1,
        :AREA_ID => 2,
        :TUR_NOMBRE => "Tur Nombre",
        :TUR_ATRACTIVO_PRINCIPAL => "Tur Atractivo Principal",
        :TUR_OTROS_ATRACTIVOS => "Tur Otros Atractivos",
        :TUR_TIPO_CLIMA => "Tur Tipo Clima",
        :TUR_EPOCA_LLUVIAS => "Tur Epoca Lluvias",
        :TUR_CAP_CARGA => 3,
        :TUR_SITIO_INGRESO => "Tur Sitio Ingreso",
        :TUR_DIST_SENDERO => 1.5,
        :TUR_DIST_CARRETERA => 1.5,
        :TUR_DIST_OTRO => 1.5,
        :TUR_DIST_ADMIN_CERCANO => 1.5,
        :TUR_ACAMPAR_NUM => 4,
        :TUR_ALOJAMIENTO_NUM => 5,
        :TUR_SENDERO_AUTO_NUM => 6,
        :TUR_OTROS_SENDEROS_NUM => 7,
        :TUR_CENTRO_INTERP_NUM => 8
      )
    ])
  end

  it "renders a list of sitio_turisticos" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Tur Nombre".to_s, :count => 2
    assert_select "tr>td", :text => "Tur Atractivo Principal".to_s, :count => 2
    assert_select "tr>td", :text => "Tur Otros Atractivos".to_s, :count => 2
    assert_select "tr>td", :text => "Tur Tipo Clima".to_s, :count => 2
    assert_select "tr>td", :text => "Tur Epoca Lluvias".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Tur Sitio Ingreso".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => 7.to_s, :count => 2
    assert_select "tr>td", :text => 8.to_s, :count => 2
  end
end
