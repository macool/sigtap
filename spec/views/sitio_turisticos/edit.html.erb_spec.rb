require 'spec_helper'

describe "sitio_turisticos/edit" do
  before(:each) do
    @sitio_turistico = assign(:sitio_turistico, stub_model(SitioTuristico,
      :GEO_ID => 1,
      :AREA_ID => 1,
      :TUR_NOMBRE => "MyString",
      :TUR_ATRACTIVO_PRINCIPAL => "MyString",
      :TUR_OTROS_ATRACTIVOS => "MyString",
      :TUR_TIPO_CLIMA => "MyString",
      :TUR_EPOCA_LLUVIAS => "MyString",
      :TUR_CAP_CARGA => 1,
      :TUR_SITIO_INGRESO => "MyString",
      :TUR_DIST_SENDERO => 1.5,
      :TUR_DIST_CARRETERA => 1.5,
      :TUR_DIST_OTRO => 1.5,
      :TUR_DIST_ADMIN_CERCANO => 1.5,
      :TUR_ACAMPAR_NUM => 1,
      :TUR_ALOJAMIENTO_NUM => 1,
      :TUR_SENDERO_AUTO_NUM => 1,
      :TUR_OTROS_SENDEROS_NUM => 1,
      :TUR_CENTRO_INTERP_NUM => 1
    ))
  end

  it "renders the edit sitio_turistico form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => sitio_turisticos_path(@sitio_turistico), :method => "post" do
      assert_select "input#sitio_turistico_GEO_ID", :name => "sitio_turistico[GEO_ID]"
      assert_select "input#sitio_turistico_AREA_ID", :name => "sitio_turistico[AREA_ID]"
      assert_select "input#sitio_turistico_TUR_NOMBRE", :name => "sitio_turistico[TUR_NOMBRE]"
      assert_select "input#sitio_turistico_TUR_ATRACTIVO_PRINCIPAL", :name => "sitio_turistico[TUR_ATRACTIVO_PRINCIPAL]"
      assert_select "input#sitio_turistico_TUR_OTROS_ATRACTIVOS", :name => "sitio_turistico[TUR_OTROS_ATRACTIVOS]"
      assert_select "input#sitio_turistico_TUR_TIPO_CLIMA", :name => "sitio_turistico[TUR_TIPO_CLIMA]"
      assert_select "input#sitio_turistico_TUR_EPOCA_LLUVIAS", :name => "sitio_turistico[TUR_EPOCA_LLUVIAS]"
      assert_select "input#sitio_turistico_TUR_CAP_CARGA", :name => "sitio_turistico[TUR_CAP_CARGA]"
      assert_select "input#sitio_turistico_TUR_SITIO_INGRESO", :name => "sitio_turistico[TUR_SITIO_INGRESO]"
      assert_select "input#sitio_turistico_TUR_DIST_SENDERO", :name => "sitio_turistico[TUR_DIST_SENDERO]"
      assert_select "input#sitio_turistico_TUR_DIST_CARRETERA", :name => "sitio_turistico[TUR_DIST_CARRETERA]"
      assert_select "input#sitio_turistico_TUR_DIST_OTRO", :name => "sitio_turistico[TUR_DIST_OTRO]"
      assert_select "input#sitio_turistico_TUR_DIST_ADMIN_CERCANO", :name => "sitio_turistico[TUR_DIST_ADMIN_CERCANO]"
      assert_select "input#sitio_turistico_TUR_ACAMPAR_NUM", :name => "sitio_turistico[TUR_ACAMPAR_NUM]"
      assert_select "input#sitio_turistico_TUR_ALOJAMIENTO_NUM", :name => "sitio_turistico[TUR_ALOJAMIENTO_NUM]"
      assert_select "input#sitio_turistico_TUR_SENDERO_AUTO_NUM", :name => "sitio_turistico[TUR_SENDERO_AUTO_NUM]"
      assert_select "input#sitio_turistico_TUR_OTROS_SENDEROS_NUM", :name => "sitio_turistico[TUR_OTROS_SENDEROS_NUM]"
      assert_select "input#sitio_turistico_TUR_CENTRO_INTERP_NUM", :name => "sitio_turistico[TUR_CENTRO_INTERP_NUM]"
    end
  end
end
