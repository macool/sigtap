require 'spec_helper'

describe "sitio_turisticos/show" do
  before(:each) do
    @sitio_turistico = assign(:sitio_turistico, stub_model(SitioTuristico,
      :GEO_ID => 1,
      :AREA_ID => 2,
      :TUR_NOMBRE => "Tur Nombre",
      :TUR_ATRACTIVO_PRINCIPAL => "Tur Atractivo Principal",
      :TUR_OTROS_ATRACTIVOS => "Tur Otros Atractivos",
      :TUR_TIPO_CLIMA => "Tur Tipo Clima",
      :TUR_EPOCA_LLUVIAS => "Tur Epoca Lluvias",
      :TUR_CAP_CARGA => 3,
      :TUR_SITIO_INGRESO => "Tur Sitio Ingreso",
      :TUR_DIST_SENDERO => 1.5,
      :TUR_DIST_CARRETERA => 1.5,
      :TUR_DIST_OTRO => 1.5,
      :TUR_DIST_ADMIN_CERCANO => 1.5,
      :TUR_ACAMPAR_NUM => 4,
      :TUR_ALOJAMIENTO_NUM => 5,
      :TUR_SENDERO_AUTO_NUM => 6,
      :TUR_OTROS_SENDEROS_NUM => 7,
      :TUR_CENTRO_INTERP_NUM => 8
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/Tur Nombre/)
    rendered.should match(/Tur Atractivo Principal/)
    rendered.should match(/Tur Otros Atractivos/)
    rendered.should match(/Tur Tipo Clima/)
    rendered.should match(/Tur Epoca Lluvias/)
    rendered.should match(/3/)
    rendered.should match(/Tur Sitio Ingreso/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/4/)
    rendered.should match(/5/)
    rendered.should match(/6/)
    rendered.should match(/7/)
    rendered.should match(/8/)
  end
end
