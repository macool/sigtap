require 'spec_helper'

describe "actividad_y_manejos/index" do
  before(:each) do
    assign(:actividad_y_manejos, [
      stub_model(ActividadYManejo,
        :AGR_ID => 1,
        :AGR_ACT_TIPO => "Agr Act Tipo",
        :AGR_ACT_MANEJO => "Agr Act Manejo"
      ),
      stub_model(ActividadYManejo,
        :AGR_ID => 1,
        :AGR_ACT_TIPO => "Agr Act Tipo",
        :AGR_ACT_MANEJO => "Agr Act Manejo"
      )
    ])
  end

  it "renders a list of actividad_y_manejos" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Agr Act Tipo".to_s, :count => 2
    assert_select "tr>td", :text => "Agr Act Manejo".to_s, :count => 2
  end
end
