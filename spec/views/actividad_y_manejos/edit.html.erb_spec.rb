require 'spec_helper'

describe "actividad_y_manejos/edit" do
  before(:each) do
    @actividad_y_manejo = assign(:actividad_y_manejo, stub_model(ActividadYManejo,
      :AGR_ID => 1,
      :AGR_ACT_TIPO => "MyString",
      :AGR_ACT_MANEJO => "MyString"
    ))
  end

  it "renders the edit actividad_y_manejo form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => actividad_y_manejos_path(@actividad_y_manejo), :method => "post" do
      assert_select "input#actividad_y_manejo_AGR_ID", :name => "actividad_y_manejo[AGR_ID]"
      assert_select "input#actividad_y_manejo_AGR_ACT_TIPO", :name => "actividad_y_manejo[AGR_ACT_TIPO]"
      assert_select "input#actividad_y_manejo_AGR_ACT_MANEJO", :name => "actividad_y_manejo[AGR_ACT_MANEJO]"
    end
  end
end
