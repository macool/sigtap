require 'spec_helper'

describe "actividad_y_manejos/new" do
  before(:each) do
    assign(:actividad_y_manejo, stub_model(ActividadYManejo,
      :AGR_ID => 1,
      :AGR_ACT_TIPO => "MyString",
      :AGR_ACT_MANEJO => "MyString"
    ).as_new_record)
  end

  it "renders new actividad_y_manejo form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => actividad_y_manejos_path, :method => "post" do
      assert_select "input#actividad_y_manejo_AGR_ID", :name => "actividad_y_manejo[AGR_ID]"
      assert_select "input#actividad_y_manejo_AGR_ACT_TIPO", :name => "actividad_y_manejo[AGR_ACT_TIPO]"
      assert_select "input#actividad_y_manejo_AGR_ACT_MANEJO", :name => "actividad_y_manejo[AGR_ACT_MANEJO]"
    end
  end
end
