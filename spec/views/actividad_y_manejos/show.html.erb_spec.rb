require 'spec_helper'

describe "actividad_y_manejos/show" do
  before(:each) do
    @actividad_y_manejo = assign(:actividad_y_manejo, stub_model(ActividadYManejo,
      :AGR_ID => 1,
      :AGR_ACT_TIPO => "Agr Act Tipo",
      :AGR_ACT_MANEJO => "Agr Act Manejo"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Agr Act Tipo/)
    rendered.should match(/Agr Act Manejo/)
  end
end
