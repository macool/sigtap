require 'spec_helper'

describe "actividad_agropecuaria/edit" do
  before(:each) do
    @actividad_agropecuarium = assign(:actividad_agropecuarium, stub_model(ActividadAgropecuarium,
      :AMENAZA_ID => 1,
      :ARG_TIPO => "MyString",
      :ARG_EXTENSION => 1.5
    ))
  end

  it "renders the edit actividad_agropecuarium form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => actividad_agropecuaria_path(@actividad_agropecuarium), :method => "post" do
      assert_select "input#actividad_agropecuarium_AMENAZA_ID", :name => "actividad_agropecuarium[AMENAZA_ID]"
      assert_select "input#actividad_agropecuarium_ARG_TIPO", :name => "actividad_agropecuarium[ARG_TIPO]"
      assert_select "input#actividad_agropecuarium_ARG_EXTENSION", :name => "actividad_agropecuarium[ARG_EXTENSION]"
    end
  end
end
