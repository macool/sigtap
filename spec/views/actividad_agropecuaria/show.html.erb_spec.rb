require 'spec_helper'

describe "actividad_agropecuaria/show" do
  before(:each) do
    @actividad_agropecuarium = assign(:actividad_agropecuarium, stub_model(ActividadAgropecuarium,
      :AMENAZA_ID => 1,
      :ARG_TIPO => "Arg Tipo",
      :ARG_EXTENSION => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Arg Tipo/)
    rendered.should match(/1.5/)
  end
end
