require 'spec_helper'

describe "actividad_agropecuaria/index" do
  before(:each) do
    assign(:actividad_agropecuaria, [
      stub_model(ActividadAgropecuarium,
        :AMENAZA_ID => 1,
        :ARG_TIPO => "Arg Tipo",
        :ARG_EXTENSION => 1.5
      ),
      stub_model(ActividadAgropecuarium,
        :AMENAZA_ID => 1,
        :ARG_TIPO => "Arg Tipo",
        :ARG_EXTENSION => 1.5
      )
    ])
  end

  it "renders a list of actividad_agropecuaria" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Arg Tipo".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
