require 'spec_helper'

describe "actividad_agropecuaria/new" do
  before(:each) do
    assign(:actividad_agropecuarium, stub_model(ActividadAgropecuarium,
      :AMENAZA_ID => 1,
      :ARG_TIPO => "MyString",
      :ARG_EXTENSION => 1.5
    ).as_new_record)
  end

  it "renders new actividad_agropecuarium form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => actividad_agropecuaria_path, :method => "post" do
      assert_select "input#actividad_agropecuarium_AMENAZA_ID", :name => "actividad_agropecuarium[AMENAZA_ID]"
      assert_select "input#actividad_agropecuarium_ARG_TIPO", :name => "actividad_agropecuarium[ARG_TIPO]"
      assert_select "input#actividad_agropecuarium_ARG_EXTENSION", :name => "actividad_agropecuarium[ARG_EXTENSION]"
    end
  end
end
