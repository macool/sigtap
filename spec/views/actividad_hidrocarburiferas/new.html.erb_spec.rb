require 'spec_helper'

describe "actividad_hidrocarburiferas/new" do
  before(:each) do
    assign(:actividad_hidrocarburifera, stub_model(ActividadHidrocarburifera,
      :AMENAZA_ID => 1,
      :ACT_HID_CONCESIONADO => "MyString",
      :ACT_HID_CODIGO => "MyString",
      :ACT_HID_EXT_CONCESION => 1.5,
      :ACT_HID_EXT_DENTRO_AREA => 1.5,
      :ACT_HID_TOTAL_EXPLOTACION => 1.5,
      :ACT_HID_EXPL_DENTRO_AREA => 1.5,
      :ACT_HID_ESTADO_CONCESION => "MyString",
      :ACT_HID_SISTEMA_TRANSPORTE => "MyString"
    ).as_new_record)
  end

  it "renders new actividad_hidrocarburifera form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => actividad_hidrocarburiferas_path, :method => "post" do
      assert_select "input#actividad_hidrocarburifera_AMENAZA_ID", :name => "actividad_hidrocarburifera[AMENAZA_ID]"
      assert_select "input#actividad_hidrocarburifera_ACT_HID_CONCESIONADO", :name => "actividad_hidrocarburifera[ACT_HID_CONCESIONADO]"
      assert_select "input#actividad_hidrocarburifera_ACT_HID_CODIGO", :name => "actividad_hidrocarburifera[ACT_HID_CODIGO]"
      assert_select "input#actividad_hidrocarburifera_ACT_HID_EXT_CONCESION", :name => "actividad_hidrocarburifera[ACT_HID_EXT_CONCESION]"
      assert_select "input#actividad_hidrocarburifera_ACT_HID_EXT_DENTRO_AREA", :name => "actividad_hidrocarburifera[ACT_HID_EXT_DENTRO_AREA]"
      assert_select "input#actividad_hidrocarburifera_ACT_HID_TOTAL_EXPLOTACION", :name => "actividad_hidrocarburifera[ACT_HID_TOTAL_EXPLOTACION]"
      assert_select "input#actividad_hidrocarburifera_ACT_HID_EXPL_DENTRO_AREA", :name => "actividad_hidrocarburifera[ACT_HID_EXPL_DENTRO_AREA]"
      assert_select "input#actividad_hidrocarburifera_ACT_HID_ESTADO_CONCESION", :name => "actividad_hidrocarburifera[ACT_HID_ESTADO_CONCESION]"
      assert_select "input#actividad_hidrocarburifera_ACT_HID_SISTEMA_TRANSPORTE", :name => "actividad_hidrocarburifera[ACT_HID_SISTEMA_TRANSPORTE]"
    end
  end
end
