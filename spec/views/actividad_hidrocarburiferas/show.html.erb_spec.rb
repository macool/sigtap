require 'spec_helper'

describe "actividad_hidrocarburiferas/show" do
  before(:each) do
    @actividad_hidrocarburifera = assign(:actividad_hidrocarburifera, stub_model(ActividadHidrocarburifera,
      :AMENAZA_ID => 1,
      :ACT_HID_CONCESIONADO => "Act Hid Concesionado",
      :ACT_HID_CODIGO => "Act Hid Codigo",
      :ACT_HID_EXT_CONCESION => 1.5,
      :ACT_HID_EXT_DENTRO_AREA => 1.5,
      :ACT_HID_TOTAL_EXPLOTACION => 1.5,
      :ACT_HID_EXPL_DENTRO_AREA => 1.5,
      :ACT_HID_ESTADO_CONCESION => "Act Hid Estado Concesion",
      :ACT_HID_SISTEMA_TRANSPORTE => "Act Hid Sistema Transporte"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Act Hid Concesionado/)
    rendered.should match(/Act Hid Codigo/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/Act Hid Estado Concesion/)
    rendered.should match(/Act Hid Sistema Transporte/)
  end
end
