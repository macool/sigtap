require 'spec_helper'

describe "actividad_hidrocarburiferas/index" do
  before(:each) do
    assign(:actividad_hidrocarburiferas, [
      stub_model(ActividadHidrocarburifera,
        :AMENAZA_ID => 1,
        :ACT_HID_CONCESIONADO => "Act Hid Concesionado",
        :ACT_HID_CODIGO => "Act Hid Codigo",
        :ACT_HID_EXT_CONCESION => 1.5,
        :ACT_HID_EXT_DENTRO_AREA => 1.5,
        :ACT_HID_TOTAL_EXPLOTACION => 1.5,
        :ACT_HID_EXPL_DENTRO_AREA => 1.5,
        :ACT_HID_ESTADO_CONCESION => "Act Hid Estado Concesion",
        :ACT_HID_SISTEMA_TRANSPORTE => "Act Hid Sistema Transporte"
      ),
      stub_model(ActividadHidrocarburifera,
        :AMENAZA_ID => 1,
        :ACT_HID_CONCESIONADO => "Act Hid Concesionado",
        :ACT_HID_CODIGO => "Act Hid Codigo",
        :ACT_HID_EXT_CONCESION => 1.5,
        :ACT_HID_EXT_DENTRO_AREA => 1.5,
        :ACT_HID_TOTAL_EXPLOTACION => 1.5,
        :ACT_HID_EXPL_DENTRO_AREA => 1.5,
        :ACT_HID_ESTADO_CONCESION => "Act Hid Estado Concesion",
        :ACT_HID_SISTEMA_TRANSPORTE => "Act Hid Sistema Transporte"
      )
    ])
  end

  it "renders a list of actividad_hidrocarburiferas" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Act Hid Concesionado".to_s, :count => 2
    assert_select "tr>td", :text => "Act Hid Codigo".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Act Hid Estado Concesion".to_s, :count => 2
    assert_select "tr>td", :text => "Act Hid Sistema Transporte".to_s, :count => 2
  end
end
