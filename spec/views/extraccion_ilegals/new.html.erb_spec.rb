require 'spec_helper'

describe "extraccion_ilegals/new" do
  before(:each) do
    assign(:extraccion_ilegal, stub_model(ExtraccionIlegal,
      :AMENAZA_ID => 1,
      :EXT_ILE_TIPO => "MyString",
      :EXT_ILE_MAT_TIPO => "MyString",
      :EXT_ILE_USOS => "MyString",
      :EXT_ILE_CANTIDAD => 1
    ).as_new_record)
  end

  it "renders new extraccion_ilegal form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => extraccion_ilegals_path, :method => "post" do
      assert_select "input#extraccion_ilegal_AMENAZA_ID", :name => "extraccion_ilegal[AMENAZA_ID]"
      assert_select "input#extraccion_ilegal_EXT_ILE_TIPO", :name => "extraccion_ilegal[EXT_ILE_TIPO]"
      assert_select "input#extraccion_ilegal_EXT_ILE_MAT_TIPO", :name => "extraccion_ilegal[EXT_ILE_MAT_TIPO]"
      assert_select "input#extraccion_ilegal_EXT_ILE_USOS", :name => "extraccion_ilegal[EXT_ILE_USOS]"
      assert_select "input#extraccion_ilegal_EXT_ILE_CANTIDAD", :name => "extraccion_ilegal[EXT_ILE_CANTIDAD]"
    end
  end
end
