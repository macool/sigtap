require 'spec_helper'

describe "extraccion_ilegals/index" do
  before(:each) do
    assign(:extraccion_ilegals, [
      stub_model(ExtraccionIlegal,
        :AMENAZA_ID => 1,
        :EXT_ILE_TIPO => "Ext Ile Tipo",
        :EXT_ILE_MAT_TIPO => "Ext Ile Mat Tipo",
        :EXT_ILE_USOS => "Ext Ile Usos",
        :EXT_ILE_CANTIDAD => 2
      ),
      stub_model(ExtraccionIlegal,
        :AMENAZA_ID => 1,
        :EXT_ILE_TIPO => "Ext Ile Tipo",
        :EXT_ILE_MAT_TIPO => "Ext Ile Mat Tipo",
        :EXT_ILE_USOS => "Ext Ile Usos",
        :EXT_ILE_CANTIDAD => 2
      )
    ])
  end

  it "renders a list of extraccion_ilegals" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Ext Ile Tipo".to_s, :count => 2
    assert_select "tr>td", :text => "Ext Ile Mat Tipo".to_s, :count => 2
    assert_select "tr>td", :text => "Ext Ile Usos".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
