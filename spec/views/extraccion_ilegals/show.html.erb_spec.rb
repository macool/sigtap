require 'spec_helper'

describe "extraccion_ilegals/show" do
  before(:each) do
    @extraccion_ilegal = assign(:extraccion_ilegal, stub_model(ExtraccionIlegal,
      :AMENAZA_ID => 1,
      :EXT_ILE_TIPO => "Ext Ile Tipo",
      :EXT_ILE_MAT_TIPO => "Ext Ile Mat Tipo",
      :EXT_ILE_USOS => "Ext Ile Usos",
      :EXT_ILE_CANTIDAD => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Ext Ile Tipo/)
    rendered.should match(/Ext Ile Mat Tipo/)
    rendered.should match(/Ext Ile Usos/)
    rendered.should match(/2/)
  end
end
