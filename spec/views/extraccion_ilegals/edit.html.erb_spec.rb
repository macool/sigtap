require 'spec_helper'

describe "extraccion_ilegals/edit" do
  before(:each) do
    @extraccion_ilegal = assign(:extraccion_ilegal, stub_model(ExtraccionIlegal,
      :AMENAZA_ID => 1,
      :EXT_ILE_TIPO => "MyString",
      :EXT_ILE_MAT_TIPO => "MyString",
      :EXT_ILE_USOS => "MyString",
      :EXT_ILE_CANTIDAD => 1
    ))
  end

  it "renders the edit extraccion_ilegal form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => extraccion_ilegals_path(@extraccion_ilegal), :method => "post" do
      assert_select "input#extraccion_ilegal_AMENAZA_ID", :name => "extraccion_ilegal[AMENAZA_ID]"
      assert_select "input#extraccion_ilegal_EXT_ILE_TIPO", :name => "extraccion_ilegal[EXT_ILE_TIPO]"
      assert_select "input#extraccion_ilegal_EXT_ILE_MAT_TIPO", :name => "extraccion_ilegal[EXT_ILE_MAT_TIPO]"
      assert_select "input#extraccion_ilegal_EXT_ILE_USOS", :name => "extraccion_ilegal[EXT_ILE_USOS]"
      assert_select "input#extraccion_ilegal_EXT_ILE_CANTIDAD", :name => "extraccion_ilegal[EXT_ILE_CANTIDAD]"
    end
  end
end
