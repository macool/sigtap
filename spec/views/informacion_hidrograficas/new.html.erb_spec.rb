require 'spec_helper'

describe "informacion_hidrograficas/new" do
  before(:each) do
    assign(:informacion_hidrografica, stub_model(InformacionHidrografica,
      :AREA_ID => 1,
      :HIDRO_CUENCAS_PRESENTES => "MyString",
      :HIDRO_RIOS_PRINCIPALES => "MyString",
      :HIDRO_HUMEDALES => "MyText"
    ).as_new_record)
  end

  it "renders new informacion_hidrografica form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => informacion_hidrograficas_path, :method => "post" do
      assert_select "input#informacion_hidrografica_AREA_ID", :name => "informacion_hidrografica[AREA_ID]"
      assert_select "input#informacion_hidrografica_HIDRO_CUENCAS_PRESENTES", :name => "informacion_hidrografica[HIDRO_CUENCAS_PRESENTES]"
      assert_select "input#informacion_hidrografica_HIDRO_RIOS_PRINCIPALES", :name => "informacion_hidrografica[HIDRO_RIOS_PRINCIPALES]"
      assert_select "textarea#informacion_hidrografica_HIDRO_HUMEDALES", :name => "informacion_hidrografica[HIDRO_HUMEDALES]"
    end
  end
end
