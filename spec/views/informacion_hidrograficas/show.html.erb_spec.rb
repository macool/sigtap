require 'spec_helper'

describe "informacion_hidrograficas/show" do
  before(:each) do
    @informacion_hidrografica = assign(:informacion_hidrografica, stub_model(InformacionHidrografica,
      :AREA_ID => 1,
      :HIDRO_CUENCAS_PRESENTES => "Hidro Cuencas Presentes",
      :HIDRO_RIOS_PRINCIPALES => "Hidro Rios Principales",
      :HIDRO_HUMEDALES => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Hidro Cuencas Presentes/)
    rendered.should match(/Hidro Rios Principales/)
    rendered.should match(/MyText/)
  end
end
