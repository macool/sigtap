require 'spec_helper'

describe "informacion_hidrograficas/index" do
  before(:each) do
    assign(:informacion_hidrograficas, [
      stub_model(InformacionHidrografica,
        :AREA_ID => 1,
        :HIDRO_CUENCAS_PRESENTES => "Hidro Cuencas Presentes",
        :HIDRO_RIOS_PRINCIPALES => "Hidro Rios Principales",
        :HIDRO_HUMEDALES => "MyText"
      ),
      stub_model(InformacionHidrografica,
        :AREA_ID => 1,
        :HIDRO_CUENCAS_PRESENTES => "Hidro Cuencas Presentes",
        :HIDRO_RIOS_PRINCIPALES => "Hidro Rios Principales",
        :HIDRO_HUMEDALES => "MyText"
      )
    ])
  end

  it "renders a list of informacion_hidrograficas" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Hidro Cuencas Presentes".to_s, :count => 2
    assert_select "tr>td", :text => "Hidro Rios Principales".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
