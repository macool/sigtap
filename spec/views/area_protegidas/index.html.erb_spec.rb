require 'spec_helper'

describe "area_protegidas/index" do
  before(:each) do
    assign(:area_protegidas, [
      stub_model(AreaProtegida,
        :AREA_NOMBRE => "Area Nombre",
        :AREA_TIPO => "Area Tipo",
        :AREA_OBJETIVOS => "Area Objetivos",
        :BIO_ID => 1,
        :GEO_ID => 2,
        :CLIMA_ID => 3,
        :CAT_ID => 4
      ),
      stub_model(AreaProtegida,
        :AREA_NOMBRE => "Area Nombre",
        :AREA_TIPO => "Area Tipo",
        :AREA_OBJETIVOS => "Area Objetivos",
        :BIO_ID => 1,
        :GEO_ID => 2,
        :CLIMA_ID => 3,
        :CAT_ID => 4
      )
    ])
  end

  it "renders a list of area_protegidas" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Area Nombre".to_s, :count => 2
    assert_select "tr>td", :text => "Area Tipo".to_s, :count => 2
    assert_select "tr>td", :text => "Area Objetivos".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
  end
end
