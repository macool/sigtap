require 'spec_helper'

describe "area_protegidas/new" do
  before(:each) do
    assign(:area_protegida, stub_model(AreaProtegida,
      :AREA_NOMBRE => "MyString",
      :AREA_TIPO => "MyString",
      :AREA_OBJETIVOS => "MyString",
      :BIO_ID => 1,
      :GEO_ID => 1,
      :CLIMA_ID => 1,
      :CAT_ID => 1
    ).as_new_record)
  end

  it "renders new area_protegida form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => area_protegidas_path, :method => "post" do
      assert_select "input#area_protegida_AREA_NOMBRE", :name => "area_protegida[AREA_NOMBRE]"
      assert_select "input#area_protegida_AREA_TIPO", :name => "area_protegida[AREA_TIPO]"
      assert_select "input#area_protegida_AREA_OBJETIVOS", :name => "area_protegida[AREA_OBJETIVOS]"
      assert_select "input#area_protegida_BIO_ID", :name => "area_protegida[BIO_ID]"
      assert_select "input#area_protegida_GEO_ID", :name => "area_protegida[GEO_ID]"
      assert_select "input#area_protegida_CLIMA_ID", :name => "area_protegida[CLIMA_ID]"
      assert_select "input#area_protegida_CAT_ID", :name => "area_protegida[CAT_ID]"
    end
  end
end
