require 'spec_helper'

describe "area_protegidas/show" do
  before(:each) do
    @area_protegida = assign(:area_protegida, stub_model(AreaProtegida,
      :AREA_NOMBRE => "Area Nombre",
      :AREA_TIPO => "Area Tipo",
      :AREA_OBJETIVOS => "Area Objetivos",
      :BIO_ID => 1,
      :GEO_ID => 2,
      :CLIMA_ID => 3,
      :CAT_ID => 4
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Area Nombre/)
    rendered.should match(/Area Tipo/)
    rendered.should match(/Area Objetivos/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/4/)
  end
end
