require 'spec_helper'

describe "amenazas/index" do
  before(:each) do
    assign(:amenazas, [
      stub_model(Amenaza,
        :INV_ID => 1,
        :DEF_ID => 2,
        :AREA_ID => 3,
        :AMENAZA_CONTRAVENTOR_TIPO => "Amenaza Contraventor Tipo",
        :AMENAZA_TIPO => "Amenaza Tipo"
      ),
      stub_model(Amenaza,
        :INV_ID => 1,
        :DEF_ID => 2,
        :AREA_ID => 3,
        :AMENAZA_CONTRAVENTOR_TIPO => "Amenaza Contraventor Tipo",
        :AMENAZA_TIPO => "Amenaza Tipo"
      )
    ])
  end

  it "renders a list of amenazas" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Amenaza Contraventor Tipo".to_s, :count => 2
    assert_select "tr>td", :text => "Amenaza Tipo".to_s, :count => 2
  end
end
