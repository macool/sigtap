require 'spec_helper'

describe "amenazas/edit" do
  before(:each) do
    @amenaza = assign(:amenaza, stub_model(Amenaza,
      :INV_ID => 1,
      :DEF_ID => 1,
      :AREA_ID => 1,
      :AMENAZA_CONTRAVENTOR_TIPO => "MyString",
      :AMENAZA_TIPO => "MyString"
    ))
  end

  it "renders the edit amenaza form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => amenazas_path(@amenaza), :method => "post" do
      assert_select "input#amenaza_INV_ID", :name => "amenaza[INV_ID]"
      assert_select "input#amenaza_DEF_ID", :name => "amenaza[DEF_ID]"
      assert_select "input#amenaza_AREA_ID", :name => "amenaza[AREA_ID]"
      assert_select "input#amenaza_AMENAZA_CONTRAVENTOR_TIPO", :name => "amenaza[AMENAZA_CONTRAVENTOR_TIPO]"
      assert_select "input#amenaza_AMENAZA_TIPO", :name => "amenaza[AMENAZA_TIPO]"
    end
  end
end
