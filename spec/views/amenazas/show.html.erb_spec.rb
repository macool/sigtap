require 'spec_helper'

describe "amenazas/show" do
  before(:each) do
    @amenaza = assign(:amenaza, stub_model(Amenaza,
      :INV_ID => 1,
      :DEF_ID => 2,
      :AREA_ID => 3,
      :AMENAZA_CONTRAVENTOR_TIPO => "Amenaza Contraventor Tipo",
      :AMENAZA_TIPO => "Amenaza Tipo"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/Amenaza Contraventor Tipo/)
    rendered.should match(/Amenaza Tipo/)
  end
end
