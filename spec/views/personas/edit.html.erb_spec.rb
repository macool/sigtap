require 'spec_helper'

describe "personas/edit" do
  before(:each) do
    @persona = assign(:persona, stub_model(Persona,
      :TUR_ID => 1,
      :CON_ID => 1,
      :AREA_ID => 1,
      :ADMIN_ID => 1,
      :PERSONA_NOMBRE => "MyString",
      :PERSONA_APELLIDOS => "MyString",
      :PERSONA_MAIL => "MyString",
      :PERSONA_TELEFONO => "MyString",
      :PERSONA_DIRECCION => "MyString",
      :PERSONA_ROL => "MyString"
    ))
  end

  it "renders the edit persona form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => personas_path(@persona), :method => "post" do
      assert_select "input#persona_TUR_ID", :name => "persona[TUR_ID]"
      assert_select "input#persona_CON_ID", :name => "persona[CON_ID]"
      assert_select "input#persona_AREA_ID", :name => "persona[AREA_ID]"
      assert_select "input#persona_ADMIN_ID", :name => "persona[ADMIN_ID]"
      assert_select "input#persona_PERSONA_NOMBRE", :name => "persona[PERSONA_NOMBRE]"
      assert_select "input#persona_PERSONA_APELLIDOS", :name => "persona[PERSONA_APELLIDOS]"
      assert_select "input#persona_PERSONA_MAIL", :name => "persona[PERSONA_MAIL]"
      assert_select "input#persona_PERSONA_TELEFONO", :name => "persona[PERSONA_TELEFONO]"
      assert_select "input#persona_PERSONA_DIRECCION", :name => "persona[PERSONA_DIRECCION]"
      assert_select "input#persona_PERSONA_ROL", :name => "persona[PERSONA_ROL]"
    end
  end
end
