require 'spec_helper'

describe "personas/show" do
  before(:each) do
    @persona = assign(:persona, stub_model(Persona,
      :TUR_ID => 1,
      :CON_ID => 2,
      :AREA_ID => 3,
      :ADMIN_ID => 4,
      :PERSONA_NOMBRE => "Persona Nombre",
      :PERSONA_APELLIDOS => "Persona Apellidos",
      :PERSONA_MAIL => "Persona Mail",
      :PERSONA_TELEFONO => "Persona Telefono",
      :PERSONA_DIRECCION => "Persona Direccion",
      :PERSONA_ROL => "Persona Rol"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/4/)
    rendered.should match(/Persona Nombre/)
    rendered.should match(/Persona Apellidos/)
    rendered.should match(/Persona Mail/)
    rendered.should match(/Persona Telefono/)
    rendered.should match(/Persona Direccion/)
    rendered.should match(/Persona Rol/)
  end
end
