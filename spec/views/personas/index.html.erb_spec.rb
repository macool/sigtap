require 'spec_helper'

describe "personas/index" do
  before(:each) do
    assign(:personas, [
      stub_model(Persona,
        :TUR_ID => 1,
        :CON_ID => 2,
        :AREA_ID => 3,
        :ADMIN_ID => 4,
        :PERSONA_NOMBRE => "Persona Nombre",
        :PERSONA_APELLIDOS => "Persona Apellidos",
        :PERSONA_MAIL => "Persona Mail",
        :PERSONA_TELEFONO => "Persona Telefono",
        :PERSONA_DIRECCION => "Persona Direccion",
        :PERSONA_ROL => "Persona Rol"
      ),
      stub_model(Persona,
        :TUR_ID => 1,
        :CON_ID => 2,
        :AREA_ID => 3,
        :ADMIN_ID => 4,
        :PERSONA_NOMBRE => "Persona Nombre",
        :PERSONA_APELLIDOS => "Persona Apellidos",
        :PERSONA_MAIL => "Persona Mail",
        :PERSONA_TELEFONO => "Persona Telefono",
        :PERSONA_DIRECCION => "Persona Direccion",
        :PERSONA_ROL => "Persona Rol"
      )
    ])
  end

  it "renders a list of personas" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => "Persona Nombre".to_s, :count => 2
    assert_select "tr>td", :text => "Persona Apellidos".to_s, :count => 2
    assert_select "tr>td", :text => "Persona Mail".to_s, :count => 2
    assert_select "tr>td", :text => "Persona Telefono".to_s, :count => 2
    assert_select "tr>td", :text => "Persona Direccion".to_s, :count => 2
    assert_select "tr>td", :text => "Persona Rol".to_s, :count => 2
  end
end
