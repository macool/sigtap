require 'spec_helper'

describe "amenaza_especies/index" do
  before(:each) do
    assign(:amenaza_especies, [
      stub_model(AmenazaEspecie,
        :EXT_ILE_ID => 1,
        :DEF_ID => 2,
        :AME_ESP_NOMBRE_CIENTIFICO => "Ame Esp Nombre Cientifico",
        :AME_ESP_CATEGORIA => "Ame Esp Categoria",
        :AME_ESP_NUMERO => 3,
        :AME_ESP_VOLUMEN => "Ame Esp Volumen"
      ),
      stub_model(AmenazaEspecie,
        :EXT_ILE_ID => 1,
        :DEF_ID => 2,
        :AME_ESP_NOMBRE_CIENTIFICO => "Ame Esp Nombre Cientifico",
        :AME_ESP_CATEGORIA => "Ame Esp Categoria",
        :AME_ESP_NUMERO => 3,
        :AME_ESP_VOLUMEN => "Ame Esp Volumen"
      )
    ])
  end

  it "renders a list of amenaza_especies" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Ame Esp Nombre Cientifico".to_s, :count => 2
    assert_select "tr>td", :text => "Ame Esp Categoria".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Ame Esp Volumen".to_s, :count => 2
  end
end
