require 'spec_helper'

describe "amenaza_especies/show" do
  before(:each) do
    @amenaza_especie = assign(:amenaza_especie, stub_model(AmenazaEspecie,
      :EXT_ILE_ID => 1,
      :DEF_ID => 2,
      :AME_ESP_NOMBRE_CIENTIFICO => "Ame Esp Nombre Cientifico",
      :AME_ESP_CATEGORIA => "Ame Esp Categoria",
      :AME_ESP_NUMERO => 3,
      :AME_ESP_VOLUMEN => "Ame Esp Volumen"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/Ame Esp Nombre Cientifico/)
    rendered.should match(/Ame Esp Categoria/)
    rendered.should match(/3/)
    rendered.should match(/Ame Esp Volumen/)
  end
end
