require 'spec_helper'

describe "amenaza_especies/edit" do
  before(:each) do
    @amenaza_especie = assign(:amenaza_especie, stub_model(AmenazaEspecie,
      :EXT_ILE_ID => 1,
      :DEF_ID => 1,
      :AME_ESP_NOMBRE_CIENTIFICO => "MyString",
      :AME_ESP_CATEGORIA => "MyString",
      :AME_ESP_NUMERO => 1,
      :AME_ESP_VOLUMEN => "MyString"
    ))
  end

  it "renders the edit amenaza_especie form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => amenaza_especies_path(@amenaza_especie), :method => "post" do
      assert_select "input#amenaza_especie_EXT_ILE_ID", :name => "amenaza_especie[EXT_ILE_ID]"
      assert_select "input#amenaza_especie_DEF_ID", :name => "amenaza_especie[DEF_ID]"
      assert_select "input#amenaza_especie_AME_ESP_NOMBRE_CIENTIFICO", :name => "amenaza_especie[AME_ESP_NOMBRE_CIENTIFICO]"
      assert_select "input#amenaza_especie_AME_ESP_CATEGORIA", :name => "amenaza_especie[AME_ESP_CATEGORIA]"
      assert_select "input#amenaza_especie_AME_ESP_NUMERO", :name => "amenaza_especie[AME_ESP_NUMERO]"
      assert_select "input#amenaza_especie_AME_ESP_VOLUMEN", :name => "amenaza_especie[AME_ESP_VOLUMEN]"
    end
  end
end
