require 'spec_helper'

describe "conflicto_socioambientals/index" do
  before(:each) do
    assign(:conflicto_socioambientals, [
      stub_model(ConflictoSocioambiental,
        :AREA_ID => 1,
        :CON_TIPO => "Con Tipo",
        :CON_ESTADO => "Con Estado",
        :CON_INVOLUCRADO => "Con Involucrado"
      ),
      stub_model(ConflictoSocioambiental,
        :AREA_ID => 1,
        :CON_TIPO => "Con Tipo",
        :CON_ESTADO => "Con Estado",
        :CON_INVOLUCRADO => "Con Involucrado"
      )
    ])
  end

  it "renders a list of conflicto_socioambientals" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Con Tipo".to_s, :count => 2
    assert_select "tr>td", :text => "Con Estado".to_s, :count => 2
    assert_select "tr>td", :text => "Con Involucrado".to_s, :count => 2
  end
end
