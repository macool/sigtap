require 'spec_helper'

describe "conflicto_socioambientals/new" do
  before(:each) do
    assign(:conflicto_socioambiental, stub_model(ConflictoSocioambiental,
      :AREA_ID => 1,
      :CON_TIPO => "MyString",
      :CON_ESTADO => "MyString",
      :CON_INVOLUCRADO => "MyString"
    ).as_new_record)
  end

  it "renders new conflicto_socioambiental form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => conflicto_socioambientals_path, :method => "post" do
      assert_select "input#conflicto_socioambiental_AREA_ID", :name => "conflicto_socioambiental[AREA_ID]"
      assert_select "input#conflicto_socioambiental_CON_TIPO", :name => "conflicto_socioambiental[CON_TIPO]"
      assert_select "input#conflicto_socioambiental_CON_ESTADO", :name => "conflicto_socioambiental[CON_ESTADO]"
      assert_select "input#conflicto_socioambiental_CON_INVOLUCRADO", :name => "conflicto_socioambiental[CON_INVOLUCRADO]"
    end
  end
end
