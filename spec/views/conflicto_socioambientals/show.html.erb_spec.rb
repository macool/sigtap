require 'spec_helper'

describe "conflicto_socioambientals/show" do
  before(:each) do
    @conflicto_socioambiental = assign(:conflicto_socioambiental, stub_model(ConflictoSocioambiental,
      :AREA_ID => 1,
      :CON_TIPO => "Con Tipo",
      :CON_ESTADO => "Con Estado",
      :CON_INVOLUCRADO => "Con Involucrado"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Con Tipo/)
    rendered.should match(/Con Estado/)
    rendered.should match(/Con Involucrado/)
  end
end
