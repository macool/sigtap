require 'spec_helper'

describe "biodiversidad_especies/index" do
  before(:each) do
    assign(:biodiversidad_especies, [
      stub_model(BiodiversidadEspecie,
        :BIO_ID => 1,
        :BIO_ESP_TIPO => "Bio Esp Tipo",
        :BIO_ESP_NUM_FAMILIAS => 2,
        :BIO_ESP_NUM_ESPECIES => 3,
        :BIO_ESP_REPRESENTATIVA => "Bio Esp Representativa",
        :BIO_ESP_NOMBRE => "Bio Esp Nombre"
      ),
      stub_model(BiodiversidadEspecie,
        :BIO_ID => 1,
        :BIO_ESP_TIPO => "Bio Esp Tipo",
        :BIO_ESP_NUM_FAMILIAS => 2,
        :BIO_ESP_NUM_ESPECIES => 3,
        :BIO_ESP_REPRESENTATIVA => "Bio Esp Representativa",
        :BIO_ESP_NOMBRE => "Bio Esp Nombre"
      )
    ])
  end

  it "renders a list of biodiversidad_especies" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Bio Esp Tipo".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Bio Esp Representativa".to_s, :count => 2
    assert_select "tr>td", :text => "Bio Esp Nombre".to_s, :count => 2
  end
end
