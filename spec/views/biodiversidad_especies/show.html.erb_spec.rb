require 'spec_helper'

describe "biodiversidad_especies/show" do
  before(:each) do
    @biodiversidad_especie = assign(:biodiversidad_especie, stub_model(BiodiversidadEspecie,
      :BIO_ID => 1,
      :BIO_ESP_TIPO => "Bio Esp Tipo",
      :BIO_ESP_NUM_FAMILIAS => 2,
      :BIO_ESP_NUM_ESPECIES => 3,
      :BIO_ESP_REPRESENTATIVA => "Bio Esp Representativa",
      :BIO_ESP_NOMBRE => "Bio Esp Nombre"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Bio Esp Tipo/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/Bio Esp Representativa/)
    rendered.should match(/Bio Esp Nombre/)
  end
end
