require 'spec_helper'

describe "biodiversidad_especies/edit" do
  before(:each) do
    @biodiversidad_especie = assign(:biodiversidad_especie, stub_model(BiodiversidadEspecie,
      :BIO_ID => 1,
      :BIO_ESP_TIPO => "MyString",
      :BIO_ESP_NUM_FAMILIAS => 1,
      :BIO_ESP_NUM_ESPECIES => 1,
      :BIO_ESP_REPRESENTATIVA => "MyString",
      :BIO_ESP_NOMBRE => "MyString"
    ))
  end

  it "renders the edit biodiversidad_especie form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => biodiversidad_especies_path(@biodiversidad_especie), :method => "post" do
      assert_select "input#biodiversidad_especie_BIO_ID", :name => "biodiversidad_especie[BIO_ID]"
      assert_select "input#biodiversidad_especie_BIO_ESP_TIPO", :name => "biodiversidad_especie[BIO_ESP_TIPO]"
      assert_select "input#biodiversidad_especie_BIO_ESP_NUM_FAMILIAS", :name => "biodiversidad_especie[BIO_ESP_NUM_FAMILIAS]"
      assert_select "input#biodiversidad_especie_BIO_ESP_NUM_ESPECIES", :name => "biodiversidad_especie[BIO_ESP_NUM_ESPECIES]"
      assert_select "input#biodiversidad_especie_BIO_ESP_REPRESENTATIVA", :name => "biodiversidad_especie[BIO_ESP_REPRESENTATIVA]"
      assert_select "input#biodiversidad_especie_BIO_ESP_NOMBRE", :name => "biodiversidad_especie[BIO_ESP_NOMBRE]"
    end
  end
end
