require 'spec_helper'

describe "otra_infraestructuras/edit" do
  before(:each) do
    @otra_infraestructura = assign(:otra_infraestructura, stub_model(OtraInfraestructura,
      :AMENAZA_ID => 1,
      :OTR_INF_AREA_COBERTURA => 1.5,
      :OTR_INF_TIPO => "MyString"
    ))
  end

  it "renders the edit otra_infraestructura form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => otra_infraestructuras_path(@otra_infraestructura), :method => "post" do
      assert_select "input#otra_infraestructura_AMENAZA_ID", :name => "otra_infraestructura[AMENAZA_ID]"
      assert_select "input#otra_infraestructura_OTR_INF_AREA_COBERTURA", :name => "otra_infraestructura[OTR_INF_AREA_COBERTURA]"
      assert_select "input#otra_infraestructura_OTR_INF_TIPO", :name => "otra_infraestructura[OTR_INF_TIPO]"
    end
  end
end
