require 'spec_helper'

describe "otra_infraestructuras/show" do
  before(:each) do
    @otra_infraestructura = assign(:otra_infraestructura, stub_model(OtraInfraestructura,
      :AMENAZA_ID => 1,
      :OTR_INF_AREA_COBERTURA => 1.5,
      :OTR_INF_TIPO => "Otr Inf Tipo"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/1.5/)
    rendered.should match(/Otr Inf Tipo/)
  end
end
