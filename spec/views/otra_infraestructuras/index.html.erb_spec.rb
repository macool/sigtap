require 'spec_helper'

describe "otra_infraestructuras/index" do
  before(:each) do
    assign(:otra_infraestructuras, [
      stub_model(OtraInfraestructura,
        :AMENAZA_ID => 1,
        :OTR_INF_AREA_COBERTURA => 1.5,
        :OTR_INF_TIPO => "Otr Inf Tipo"
      ),
      stub_model(OtraInfraestructura,
        :AMENAZA_ID => 1,
        :OTR_INF_AREA_COBERTURA => 1.5,
        :OTR_INF_TIPO => "Otr Inf Tipo"
      )
    ])
  end

  it "renders a list of otra_infraestructuras" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Otr Inf Tipo".to_s, :count => 2
  end
end
