require 'spec_helper'

describe "especie_introducidas/show" do
  before(:each) do
    @especie_introducida = assign(:especie_introducida, stub_model(EspecieIntroducida,
      :AMENAZA_ID => 1,
      :ESP_INT_TIPO => "Esp Int Tipo",
      :ESP_INT_ESPECIE => "Esp Int Especie",
      :ESP_INT_EXT_PLANTACION => 1.5,
      :ESP_INT_EXT_POBLACION => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Esp Int Tipo/)
    rendered.should match(/Esp Int Especie/)
    rendered.should match(/1.5/)
    rendered.should match(/2/)
  end
end
