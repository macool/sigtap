require 'spec_helper'

describe "especie_introducidas/edit" do
  before(:each) do
    @especie_introducida = assign(:especie_introducida, stub_model(EspecieIntroducida,
      :AMENAZA_ID => 1,
      :ESP_INT_TIPO => "MyString",
      :ESP_INT_ESPECIE => "MyString",
      :ESP_INT_EXT_PLANTACION => 1.5,
      :ESP_INT_EXT_POBLACION => 1
    ))
  end

  it "renders the edit especie_introducida form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => especie_introducidas_path(@especie_introducida), :method => "post" do
      assert_select "input#especie_introducida_AMENAZA_ID", :name => "especie_introducida[AMENAZA_ID]"
      assert_select "input#especie_introducida_ESP_INT_TIPO", :name => "especie_introducida[ESP_INT_TIPO]"
      assert_select "input#especie_introducida_ESP_INT_ESPECIE", :name => "especie_introducida[ESP_INT_ESPECIE]"
      assert_select "input#especie_introducida_ESP_INT_EXT_PLANTACION", :name => "especie_introducida[ESP_INT_EXT_PLANTACION]"
      assert_select "input#especie_introducida_ESP_INT_EXT_POBLACION", :name => "especie_introducida[ESP_INT_EXT_POBLACION]"
    end
  end
end
