require 'spec_helper'

describe "especie_introducidas/index" do
  before(:each) do
    assign(:especie_introducidas, [
      stub_model(EspecieIntroducida,
        :AMENAZA_ID => 1,
        :ESP_INT_TIPO => "Esp Int Tipo",
        :ESP_INT_ESPECIE => "Esp Int Especie",
        :ESP_INT_EXT_PLANTACION => 1.5,
        :ESP_INT_EXT_POBLACION => 2
      ),
      stub_model(EspecieIntroducida,
        :AMENAZA_ID => 1,
        :ESP_INT_TIPO => "Esp Int Tipo",
        :ESP_INT_ESPECIE => "Esp Int Especie",
        :ESP_INT_EXT_PLANTACION => 1.5,
        :ESP_INT_EXT_POBLACION => 2
      )
    ])
  end

  it "renders a list of especie_introducidas" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Esp Int Tipo".to_s, :count => 2
    assert_select "tr>td", :text => "Esp Int Especie".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
