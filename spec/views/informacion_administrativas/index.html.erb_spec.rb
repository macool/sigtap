require 'spec_helper'

describe "informacion_administrativas/index" do
  before(:each) do
    assign(:informacion_administrativas, [
      stub_model(InformacionAdministrativa,
        :AREA_ID => 1,
        :PERSON_ID => 2,
        :ADMIN_PLAN_MANEJO => "Admin Plan Manejo",
        :ADMIN_GUARDA_N => 3,
        :ADMIN_GUARDA_O => 4,
        :ADMIN_TECNICO_N => 5,
        :ADMIN_TECNICO_O => 6,
        :ADMIN_ADMIN_N => 7,
        :ADMIN_ADMIN_O => 8
      ),
      stub_model(InformacionAdministrativa,
        :AREA_ID => 1,
        :PERSON_ID => 2,
        :ADMIN_PLAN_MANEJO => "Admin Plan Manejo",
        :ADMIN_GUARDA_N => 3,
        :ADMIN_GUARDA_O => 4,
        :ADMIN_TECNICO_N => 5,
        :ADMIN_TECNICO_O => 6,
        :ADMIN_ADMIN_N => 7,
        :ADMIN_ADMIN_O => 8
      )
    ])
  end

  it "renders a list of informacion_administrativas" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Admin Plan Manejo".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => 7.to_s, :count => 2
    assert_select "tr>td", :text => 8.to_s, :count => 2
  end
end
