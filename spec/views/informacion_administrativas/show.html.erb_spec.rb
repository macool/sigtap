require 'spec_helper'

describe "informacion_administrativas/show" do
  before(:each) do
    @informacion_administrativa = assign(:informacion_administrativa, stub_model(InformacionAdministrativa,
      :AREA_ID => 1,
      :PERSON_ID => 2,
      :ADMIN_PLAN_MANEJO => "Admin Plan Manejo",
      :ADMIN_GUARDA_N => 3,
      :ADMIN_GUARDA_O => 4,
      :ADMIN_TECNICO_N => 5,
      :ADMIN_TECNICO_O => 6,
      :ADMIN_ADMIN_N => 7,
      :ADMIN_ADMIN_O => 8
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/Admin Plan Manejo/)
    rendered.should match(/3/)
    rendered.should match(/4/)
    rendered.should match(/5/)
    rendered.should match(/6/)
    rendered.should match(/7/)
    rendered.should match(/8/)
  end
end
