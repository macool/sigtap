require 'spec_helper'

describe "informacion_administrativas/new" do
  before(:each) do
    assign(:informacion_administrativa, stub_model(InformacionAdministrativa,
      :AREA_ID => 1,
      :PERSON_ID => 1,
      :ADMIN_PLAN_MANEJO => "MyString",
      :ADMIN_GUARDA_N => 1,
      :ADMIN_GUARDA_O => 1,
      :ADMIN_TECNICO_N => 1,
      :ADMIN_TECNICO_O => 1,
      :ADMIN_ADMIN_N => 1,
      :ADMIN_ADMIN_O => 1
    ).as_new_record)
  end

  it "renders new informacion_administrativa form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => informacion_administrativas_path, :method => "post" do
      assert_select "input#informacion_administrativa_AREA_ID", :name => "informacion_administrativa[AREA_ID]"
      assert_select "input#informacion_administrativa_PERSON_ID", :name => "informacion_administrativa[PERSON_ID]"
      assert_select "input#informacion_administrativa_ADMIN_PLAN_MANEJO", :name => "informacion_administrativa[ADMIN_PLAN_MANEJO]"
      assert_select "input#informacion_administrativa_ADMIN_GUARDA_N", :name => "informacion_administrativa[ADMIN_GUARDA_N]"
      assert_select "input#informacion_administrativa_ADMIN_GUARDA_O", :name => "informacion_administrativa[ADMIN_GUARDA_O]"
      assert_select "input#informacion_administrativa_ADMIN_TECNICO_N", :name => "informacion_administrativa[ADMIN_TECNICO_N]"
      assert_select "input#informacion_administrativa_ADMIN_TECNICO_O", :name => "informacion_administrativa[ADMIN_TECNICO_O]"
      assert_select "input#informacion_administrativa_ADMIN_ADMIN_N", :name => "informacion_administrativa[ADMIN_ADMIN_N]"
      assert_select "input#informacion_administrativa_ADMIN_ADMIN_O", :name => "informacion_administrativa[ADMIN_ADMIN_O]"
    end
  end
end
