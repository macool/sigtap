require 'spec_helper'

describe "categorizacions/show" do
  before(:each) do
    @categorizacion = assign(:categorizacion, stub_model(Categorizacion,
      :AREA_ID => 1,
      :CAT_TIPO => "Cat Tipo",
      :CAT_INT_DETALLES => "MyText",
      :CAT_NAC_TIPO => "Cat Nac Tipo",
      :CAT_NAC_DETALLES => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Cat Tipo/)
    rendered.should match(/MyText/)
    rendered.should match(/Cat Nac Tipo/)
    rendered.should match(/MyText/)
  end
end
