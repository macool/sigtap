require 'spec_helper'

describe "categorizacions/new" do
  before(:each) do
    assign(:categorizacion, stub_model(Categorizacion,
      :AREA_ID => 1,
      :CAT_TIPO => "MyString",
      :CAT_INT_DETALLES => "MyText",
      :CAT_NAC_TIPO => "MyString",
      :CAT_NAC_DETALLES => "MyText"
    ).as_new_record)
  end

  it "renders new categorizacion form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => categorizacions_path, :method => "post" do
      assert_select "input#categorizacion_AREA_ID", :name => "categorizacion[AREA_ID]"
      assert_select "input#categorizacion_CAT_TIPO", :name => "categorizacion[CAT_TIPO]"
      assert_select "textarea#categorizacion_CAT_INT_DETALLES", :name => "categorizacion[CAT_INT_DETALLES]"
      assert_select "input#categorizacion_CAT_NAC_TIPO", :name => "categorizacion[CAT_NAC_TIPO]"
      assert_select "textarea#categorizacion_CAT_NAC_DETALLES", :name => "categorizacion[CAT_NAC_DETALLES]"
    end
  end
end
