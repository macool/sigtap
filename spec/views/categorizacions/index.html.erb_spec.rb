require 'spec_helper'

describe "categorizacions/index" do
  before(:each) do
    assign(:categorizacions, [
      stub_model(Categorizacion,
        :AREA_ID => 1,
        :CAT_TIPO => "Cat Tipo",
        :CAT_INT_DETALLES => "MyText",
        :CAT_NAC_TIPO => "Cat Nac Tipo",
        :CAT_NAC_DETALLES => "MyText"
      ),
      stub_model(Categorizacion,
        :AREA_ID => 1,
        :CAT_TIPO => "Cat Tipo",
        :CAT_INT_DETALLES => "MyText",
        :CAT_NAC_TIPO => "Cat Nac Tipo",
        :CAT_NAC_DETALLES => "MyText"
      )
    ])
  end

  it "renders a list of categorizacions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Cat Tipo".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Cat Nac Tipo".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
