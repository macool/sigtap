require 'spec_helper'

describe "infraestructura_vials/index" do
  before(:each) do
    assign(:infraestructura_vials, [
      stub_model(InfraestructuraVial,
        :AMENAZA_ID => 1,
        :INF_VIA_PUNTOOX => 1.5,
        :INF_VIA_PUNTOOY => 1.5,
        :INF_VIA_PUNTOFX => 1.5,
        :INF_VIA_PUNTOFY => 1.5,
        :INF_VIA_LONGITUD => 1.5,
        :INF_VIA_TIPO => "Inf Via Tipo"
      ),
      stub_model(InfraestructuraVial,
        :AMENAZA_ID => 1,
        :INF_VIA_PUNTOOX => 1.5,
        :INF_VIA_PUNTOOY => 1.5,
        :INF_VIA_PUNTOFX => 1.5,
        :INF_VIA_PUNTOFY => 1.5,
        :INF_VIA_LONGITUD => 1.5,
        :INF_VIA_TIPO => "Inf Via Tipo"
      )
    ])
  end

  it "renders a list of infraestructura_vials" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Inf Via Tipo".to_s, :count => 2
  end
end
