require 'spec_helper'

describe "infraestructura_vials/show" do
  before(:each) do
    @infraestructura_vial = assign(:infraestructura_vial, stub_model(InfraestructuraVial,
      :AMENAZA_ID => 1,
      :INF_VIA_PUNTOOX => 1.5,
      :INF_VIA_PUNTOOY => 1.5,
      :INF_VIA_PUNTOFX => 1.5,
      :INF_VIA_PUNTOFY => 1.5,
      :INF_VIA_LONGITUD => 1.5,
      :INF_VIA_TIPO => "Inf Via Tipo"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/Inf Via Tipo/)
  end
end
