require 'spec_helper'

describe "infraestructura_vials/edit" do
  before(:each) do
    @infraestructura_vial = assign(:infraestructura_vial, stub_model(InfraestructuraVial,
      :AMENAZA_ID => 1,
      :INF_VIA_PUNTOOX => 1.5,
      :INF_VIA_PUNTOOY => 1.5,
      :INF_VIA_PUNTOFX => 1.5,
      :INF_VIA_PUNTOFY => 1.5,
      :INF_VIA_LONGITUD => 1.5,
      :INF_VIA_TIPO => "MyString"
    ))
  end

  it "renders the edit infraestructura_vial form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => infraestructura_vials_path(@infraestructura_vial), :method => "post" do
      assert_select "input#infraestructura_vial_AMENAZA_ID", :name => "infraestructura_vial[AMENAZA_ID]"
      assert_select "input#infraestructura_vial_INF_VIA_PUNTOOX", :name => "infraestructura_vial[INF_VIA_PUNTOOX]"
      assert_select "input#infraestructura_vial_INF_VIA_PUNTOOY", :name => "infraestructura_vial[INF_VIA_PUNTOOY]"
      assert_select "input#infraestructura_vial_INF_VIA_PUNTOFX", :name => "infraestructura_vial[INF_VIA_PUNTOFX]"
      assert_select "input#infraestructura_vial_INF_VIA_PUNTOFY", :name => "infraestructura_vial[INF_VIA_PUNTOFY]"
      assert_select "input#infraestructura_vial_INF_VIA_LONGITUD", :name => "infraestructura_vial[INF_VIA_LONGITUD]"
      assert_select "input#infraestructura_vial_INF_VIA_TIPO", :name => "infraestructura_vial[INF_VIA_TIPO]"
    end
  end
end
