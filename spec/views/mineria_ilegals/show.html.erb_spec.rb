require 'spec_helper'

describe "mineria_ilegals/show" do
  before(:each) do
    @mineria_ilegal = assign(:mineria_ilegal, stub_model(MineriaIlegal,
      :AMENAZA_ID => 1,
      :MIN_ILE_AREA => 1.5,
      :MIN_ILE_NUM_PERSONAS => 2,
      :MIN_ILE_TIPO => "Min Ile Tipo",
      :MIN_ILE_PRACTICA => "Min Ile Practica"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/1.5/)
    rendered.should match(/2/)
    rendered.should match(/Min Ile Tipo/)
    rendered.should match(/Min Ile Practica/)
  end
end
