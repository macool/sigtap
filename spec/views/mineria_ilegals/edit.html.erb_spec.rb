require 'spec_helper'

describe "mineria_ilegals/edit" do
  before(:each) do
    @mineria_ilegal = assign(:mineria_ilegal, stub_model(MineriaIlegal,
      :AMENAZA_ID => 1,
      :MIN_ILE_AREA => 1.5,
      :MIN_ILE_NUM_PERSONAS => 1,
      :MIN_ILE_TIPO => "MyString",
      :MIN_ILE_PRACTICA => "MyString"
    ))
  end

  it "renders the edit mineria_ilegal form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => mineria_ilegals_path(@mineria_ilegal), :method => "post" do
      assert_select "input#mineria_ilegal_AMENAZA_ID", :name => "mineria_ilegal[AMENAZA_ID]"
      assert_select "input#mineria_ilegal_MIN_ILE_AREA", :name => "mineria_ilegal[MIN_ILE_AREA]"
      assert_select "input#mineria_ilegal_MIN_ILE_NUM_PERSONAS", :name => "mineria_ilegal[MIN_ILE_NUM_PERSONAS]"
      assert_select "input#mineria_ilegal_MIN_ILE_TIPO", :name => "mineria_ilegal[MIN_ILE_TIPO]"
      assert_select "input#mineria_ilegal_MIN_ILE_PRACTICA", :name => "mineria_ilegal[MIN_ILE_PRACTICA]"
    end
  end
end
