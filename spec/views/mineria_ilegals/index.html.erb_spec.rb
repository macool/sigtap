require 'spec_helper'

describe "mineria_ilegals/index" do
  before(:each) do
    assign(:mineria_ilegals, [
      stub_model(MineriaIlegal,
        :AMENAZA_ID => 1,
        :MIN_ILE_AREA => 1.5,
        :MIN_ILE_NUM_PERSONAS => 2,
        :MIN_ILE_TIPO => "Min Ile Tipo",
        :MIN_ILE_PRACTICA => "Min Ile Practica"
      ),
      stub_model(MineriaIlegal,
        :AMENAZA_ID => 1,
        :MIN_ILE_AREA => 1.5,
        :MIN_ILE_NUM_PERSONAS => 2,
        :MIN_ILE_TIPO => "Min Ile Tipo",
        :MIN_ILE_PRACTICA => "Min Ile Practica"
      )
    ])
  end

  it "renders a list of mineria_ilegals" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Min Ile Tipo".to_s, :count => 2
    assert_select "tr>td", :text => "Min Ile Practica".to_s, :count => 2
  end
end
