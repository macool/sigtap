require 'spec_helper'

describe "invasions/new" do
  before(:each) do
    assign(:invasion, stub_model(Invasion,
      :INV_TIPO => "MyString",
      :INV_EXTENSION => 1.5,
      :INV_NUM_PERSONAS => 1
    ).as_new_record)
  end

  it "renders new invasion form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => invasions_path, :method => "post" do
      assert_select "input#invasion_INV_TIPO", :name => "invasion[INV_TIPO]"
      assert_select "input#invasion_INV_EXTENSION", :name => "invasion[INV_EXTENSION]"
      assert_select "input#invasion_INV_NUM_PERSONAS", :name => "invasion[INV_NUM_PERSONAS]"
    end
  end
end
