require 'spec_helper'

describe "invasions/index" do
  before(:each) do
    assign(:invasions, [
      stub_model(Invasion,
        :INV_TIPO => "Inv Tipo",
        :INV_EXTENSION => 1.5,
        :INV_NUM_PERSONAS => 1
      ),
      stub_model(Invasion,
        :INV_TIPO => "Inv Tipo",
        :INV_EXTENSION => 1.5,
        :INV_NUM_PERSONAS => 1
      )
    ])
  end

  it "renders a list of invasions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Inv Tipo".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
