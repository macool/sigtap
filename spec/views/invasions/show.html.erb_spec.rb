require 'spec_helper'

describe "invasions/show" do
  before(:each) do
    @invasion = assign(:invasion, stub_model(Invasion,
      :INV_TIPO => "Inv Tipo",
      :INV_EXTENSION => 1.5,
      :INV_NUM_PERSONAS => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Inv Tipo/)
    rendered.should match(/1.5/)
    rendered.should match(/1/)
  end
end
