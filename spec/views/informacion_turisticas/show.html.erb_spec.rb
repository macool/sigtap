require 'spec_helper'

describe "informacion_turisticas/show" do
  before(:each) do
    @informacion_turistica = assign(:informacion_turistica, stub_model(InformacionTuristica,
      :AREA_ID => 1,
      :INFO_TUR_SITIOS_REP => "Info Tur Sitios Rep",
      :INFO_TUR_INFRAESTRUCTURA => "Info Tur Infraestructura",
      :INFO_TUR_SITIO_ACCESO => "Info Tur Sitio Acceso",
      :INFO_TUR_NUM_ANUAL_VISITANTES => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Info Tur Sitios Rep/)
    rendered.should match(/Info Tur Infraestructura/)
    rendered.should match(/Info Tur Sitio Acceso/)
    rendered.should match(/2/)
  end
end
