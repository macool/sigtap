require 'spec_helper'

describe "informacion_turisticas/new" do
  before(:each) do
    assign(:informacion_turistica, stub_model(InformacionTuristica,
      :AREA_ID => 1,
      :INFO_TUR_SITIOS_REP => "MyString",
      :INFO_TUR_INFRAESTRUCTURA => "MyString",
      :INFO_TUR_SITIO_ACCESO => "MyString",
      :INFO_TUR_NUM_ANUAL_VISITANTES => 1
    ).as_new_record)
  end

  it "renders new informacion_turistica form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => informacion_turisticas_path, :method => "post" do
      assert_select "input#informacion_turistica_AREA_ID", :name => "informacion_turistica[AREA_ID]"
      assert_select "input#informacion_turistica_INFO_TUR_SITIOS_REP", :name => "informacion_turistica[INFO_TUR_SITIOS_REP]"
      assert_select "input#informacion_turistica_INFO_TUR_INFRAESTRUCTURA", :name => "informacion_turistica[INFO_TUR_INFRAESTRUCTURA]"
      assert_select "input#informacion_turistica_INFO_TUR_SITIO_ACCESO", :name => "informacion_turistica[INFO_TUR_SITIO_ACCESO]"
      assert_select "input#informacion_turistica_INFO_TUR_NUM_ANUAL_VISITANTES", :name => "informacion_turistica[INFO_TUR_NUM_ANUAL_VISITANTES]"
    end
  end
end
