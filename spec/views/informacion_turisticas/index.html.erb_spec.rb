require 'spec_helper'

describe "informacion_turisticas/index" do
  before(:each) do
    assign(:informacion_turisticas, [
      stub_model(InformacionTuristica,
        :AREA_ID => 1,
        :INFO_TUR_SITIOS_REP => "Info Tur Sitios Rep",
        :INFO_TUR_INFRAESTRUCTURA => "Info Tur Infraestructura",
        :INFO_TUR_SITIO_ACCESO => "Info Tur Sitio Acceso",
        :INFO_TUR_NUM_ANUAL_VISITANTES => 2
      ),
      stub_model(InformacionTuristica,
        :AREA_ID => 1,
        :INFO_TUR_SITIOS_REP => "Info Tur Sitios Rep",
        :INFO_TUR_INFRAESTRUCTURA => "Info Tur Infraestructura",
        :INFO_TUR_SITIO_ACCESO => "Info Tur Sitio Acceso",
        :INFO_TUR_NUM_ANUAL_VISITANTES => 2
      )
    ])
  end

  it "renders a list of informacion_turisticas" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Info Tur Sitios Rep".to_s, :count => 2
    assert_select "tr>td", :text => "Info Tur Infraestructura".to_s, :count => 2
    assert_select "tr>td", :text => "Info Tur Sitio Acceso".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
