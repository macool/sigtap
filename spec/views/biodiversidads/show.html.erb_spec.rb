require 'spec_helper'

describe "biodiversidads/show" do
  before(:each) do
    @biodiversidad = assign(:biodiversidad, stub_model(Biodiversidad,
      :AREA_ID => 1,
      :BIO_ECOSISTEMAS => "Bio Ecosistemas",
      :BIO_FORMAS_VEGETALES => "Bio Formas Vegetales",
      :BIO_ZONAS_VIDA => "Bio Zonas Vida"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Bio Ecosistemas/)
    rendered.should match(/Bio Formas Vegetales/)
    rendered.should match(/Bio Zonas Vida/)
  end
end
