require 'spec_helper'

describe "biodiversidads/index" do
  before(:each) do
    assign(:biodiversidads, [
      stub_model(Biodiversidad,
        :AREA_ID => 1,
        :BIO_ECOSISTEMAS => "Bio Ecosistemas",
        :BIO_FORMAS_VEGETALES => "Bio Formas Vegetales",
        :BIO_ZONAS_VIDA => "Bio Zonas Vida"
      ),
      stub_model(Biodiversidad,
        :AREA_ID => 1,
        :BIO_ECOSISTEMAS => "Bio Ecosistemas",
        :BIO_FORMAS_VEGETALES => "Bio Formas Vegetales",
        :BIO_ZONAS_VIDA => "Bio Zonas Vida"
      )
    ])
  end

  it "renders a list of biodiversidads" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Bio Ecosistemas".to_s, :count => 2
    assert_select "tr>td", :text => "Bio Formas Vegetales".to_s, :count => 2
    assert_select "tr>td", :text => "Bio Zonas Vida".to_s, :count => 2
  end
end
