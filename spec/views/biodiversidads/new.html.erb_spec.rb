require 'spec_helper'

describe "biodiversidads/new" do
  before(:each) do
    assign(:biodiversidad, stub_model(Biodiversidad,
      :AREA_ID => 1,
      :BIO_ECOSISTEMAS => "MyString",
      :BIO_FORMAS_VEGETALES => "MyString",
      :BIO_ZONAS_VIDA => "MyString"
    ).as_new_record)
  end

  it "renders new biodiversidad form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => biodiversidads_path, :method => "post" do
      assert_select "input#biodiversidad_AREA_ID", :name => "biodiversidad[AREA_ID]"
      assert_select "input#biodiversidad_BIO_ECOSISTEMAS", :name => "biodiversidad[BIO_ECOSISTEMAS]"
      assert_select "input#biodiversidad_BIO_FORMAS_VEGETALES", :name => "biodiversidad[BIO_FORMAS_VEGETALES]"
      assert_select "input#biodiversidad_BIO_ZONAS_VIDA", :name => "biodiversidad[BIO_ZONAS_VIDA]"
    end
  end
end
