require 'spec_helper'

describe "deforestacions/show" do
  before(:each) do
    @deforestacion = assign(:deforestacion, stub_model(Deforestacion,
      :AMENAZA_ID => 1,
      :DEF_AREA => "Def Area"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Def Area/)
  end
end
