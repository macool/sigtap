require 'spec_helper'

describe "deforestacions/index" do
  before(:each) do
    assign(:deforestacions, [
      stub_model(Deforestacion,
        :AMENAZA_ID => 1,
        :DEF_AREA => "Def Area"
      ),
      stub_model(Deforestacion,
        :AMENAZA_ID => 1,
        :DEF_AREA => "Def Area"
      )
    ])
  end

  it "renders a list of deforestacions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Def Area".to_s, :count => 2
  end
end
