require 'spec_helper'

describe "deforestacions/edit" do
  before(:each) do
    @deforestacion = assign(:deforestacion, stub_model(Deforestacion,
      :AMENAZA_ID => 1,
      :DEF_AREA => "MyString"
    ))
  end

  it "renders the edit deforestacion form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => deforestacions_path(@deforestacion), :method => "post" do
      assert_select "input#deforestacion_AMENAZA_ID", :name => "deforestacion[AMENAZA_ID]"
      assert_select "input#deforestacion_DEF_AREA", :name => "deforestacion[DEF_AREA]"
    end
  end
end
