require 'spec_helper'

describe "deforestacions/new" do
  before(:each) do
    assign(:deforestacion, stub_model(Deforestacion,
      :AMENAZA_ID => 1,
      :DEF_AREA => "MyString"
    ).as_new_record)
  end

  it "renders new deforestacion form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => deforestacions_path, :method => "post" do
      assert_select "input#deforestacion_AMENAZA_ID", :name => "deforestacion[AMENAZA_ID]"
      assert_select "input#deforestacion_DEF_AREA", :name => "deforestacion[DEF_AREA]"
    end
  end
end
