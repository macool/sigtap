require 'spec_helper'

describe "comunidads/edit" do
  before(:each) do
    @comunidad = assign(:comunidad, stub_model(Comunidad,
      :AREA_ID => "MyString",
      :COM_NOMBRE => "MyString",
      :COM_GRUPO_ETNICO_PREDOMINANTE => "MyString",
      :COM_TIPO_ORGA_SOCIAL => "MyString",
      :COM_LIDER_COMUNITARIO => "MyString",
      :COM_SUPERFICIE_APROX => 1.5,
      :COM_LOCALIZACION => "MyString",
      :COM_NUM_FAMILIAS => 1,
      :COM_NUM_INDIVIDUOS => 1,
      :COM_ACTIVIDADES => "MyString",
      :COM_SOCIO_BOSQUE => "MyString",
      :COM_HEC_SOCIOBOSQUE => 1.5,
      :COM_HEC_SOCIOPARAMO => 1.5
    ))
  end

  it "renders the edit comunidad form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => comunidads_path(@comunidad), :method => "post" do
      assert_select "input#comunidad_AREA_ID", :name => "comunidad[AREA_ID]"
      assert_select "input#comunidad_COM_NOMBRE", :name => "comunidad[COM_NOMBRE]"
      assert_select "input#comunidad_COM_GRUPO_ETNICO_PREDOMINANTE", :name => "comunidad[COM_GRUPO_ETNICO_PREDOMINANTE]"
      assert_select "input#comunidad_COM_TIPO_ORGA_SOCIAL", :name => "comunidad[COM_TIPO_ORGA_SOCIAL]"
      assert_select "input#comunidad_COM_LIDER_COMUNITARIO", :name => "comunidad[COM_LIDER_COMUNITARIO]"
      assert_select "input#comunidad_COM_SUPERFICIE_APROX", :name => "comunidad[COM_SUPERFICIE_APROX]"
      assert_select "input#comunidad_COM_LOCALIZACION", :name => "comunidad[COM_LOCALIZACION]"
      assert_select "input#comunidad_COM_NUM_FAMILIAS", :name => "comunidad[COM_NUM_FAMILIAS]"
      assert_select "input#comunidad_COM_NUM_INDIVIDUOS", :name => "comunidad[COM_NUM_INDIVIDUOS]"
      assert_select "input#comunidad_COM_ACTIVIDADES", :name => "comunidad[COM_ACTIVIDADES]"
      assert_select "input#comunidad_COM_SOCIO_BOSQUE", :name => "comunidad[COM_SOCIO_BOSQUE]"
      assert_select "input#comunidad_COM_HEC_SOCIOBOSQUE", :name => "comunidad[COM_HEC_SOCIOBOSQUE]"
      assert_select "input#comunidad_COM_HEC_SOCIOPARAMO", :name => "comunidad[COM_HEC_SOCIOPARAMO]"
    end
  end
end
