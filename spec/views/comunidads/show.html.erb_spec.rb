require 'spec_helper'

describe "comunidads/show" do
  before(:each) do
    @comunidad = assign(:comunidad, stub_model(Comunidad,
      :AREA_ID => "Area",
      :COM_NOMBRE => "Com Nombre",
      :COM_GRUPO_ETNICO_PREDOMINANTE => "Com Grupo Etnico Predominante",
      :COM_TIPO_ORGA_SOCIAL => "Com Tipo Orga Social",
      :COM_LIDER_COMUNITARIO => "Com Lider Comunitario",
      :COM_SUPERFICIE_APROX => 1.5,
      :COM_LOCALIZACION => "Com Localizacion",
      :COM_NUM_FAMILIAS => 1,
      :COM_NUM_INDIVIDUOS => 2,
      :COM_ACTIVIDADES => "Com Actividades",
      :COM_SOCIO_BOSQUE => "Com Socio Bosque",
      :COM_HEC_SOCIOBOSQUE => 1.5,
      :COM_HEC_SOCIOPARAMO => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Area/)
    rendered.should match(/Com Nombre/)
    rendered.should match(/Com Grupo Etnico Predominante/)
    rendered.should match(/Com Tipo Orga Social/)
    rendered.should match(/Com Lider Comunitario/)
    rendered.should match(/1.5/)
    rendered.should match(/Com Localizacion/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/Com Actividades/)
    rendered.should match(/Com Socio Bosque/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
  end
end
