require 'spec_helper'

describe "comunidads/index" do
  before(:each) do
    assign(:comunidads, [
      stub_model(Comunidad,
        :AREA_ID => "Area",
        :COM_NOMBRE => "Com Nombre",
        :COM_GRUPO_ETNICO_PREDOMINANTE => "Com Grupo Etnico Predominante",
        :COM_TIPO_ORGA_SOCIAL => "Com Tipo Orga Social",
        :COM_LIDER_COMUNITARIO => "Com Lider Comunitario",
        :COM_SUPERFICIE_APROX => 1.5,
        :COM_LOCALIZACION => "Com Localizacion",
        :COM_NUM_FAMILIAS => 1,
        :COM_NUM_INDIVIDUOS => 2,
        :COM_ACTIVIDADES => "Com Actividades",
        :COM_SOCIO_BOSQUE => "Com Socio Bosque",
        :COM_HEC_SOCIOBOSQUE => 1.5,
        :COM_HEC_SOCIOPARAMO => 1.5
      ),
      stub_model(Comunidad,
        :AREA_ID => "Area",
        :COM_NOMBRE => "Com Nombre",
        :COM_GRUPO_ETNICO_PREDOMINANTE => "Com Grupo Etnico Predominante",
        :COM_TIPO_ORGA_SOCIAL => "Com Tipo Orga Social",
        :COM_LIDER_COMUNITARIO => "Com Lider Comunitario",
        :COM_SUPERFICIE_APROX => 1.5,
        :COM_LOCALIZACION => "Com Localizacion",
        :COM_NUM_FAMILIAS => 1,
        :COM_NUM_INDIVIDUOS => 2,
        :COM_ACTIVIDADES => "Com Actividades",
        :COM_SOCIO_BOSQUE => "Com Socio Bosque",
        :COM_HEC_SOCIOBOSQUE => 1.5,
        :COM_HEC_SOCIOPARAMO => 1.5
      )
    ])
  end

  it "renders a list of comunidads" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Area".to_s, :count => 2
    assert_select "tr>td", :text => "Com Nombre".to_s, :count => 2
    assert_select "tr>td", :text => "Com Grupo Etnico Predominante".to_s, :count => 2
    assert_select "tr>td", :text => "Com Tipo Orga Social".to_s, :count => 2
    assert_select "tr>td", :text => "Com Lider Comunitario".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "Com Localizacion".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Com Actividades".to_s, :count => 2
    assert_select "tr>td", :text => "Com Socio Bosque".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
