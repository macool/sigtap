require 'spec_helper'

describe "informacion_geograficas/index" do
  before(:each) do
    assign(:informacion_geograficas, [
      stub_model(InformacionGeografica,
        :AMENAZA_ID => 1,
        :TUR_ID => 2,
        :AREA_ID => 3,
        :GEO_REGION => "Geo Region",
        :GEO_PROVINCIAS => "Geo Provincias",
        :GEO_CANTONES => "Geo Cantones",
        :GEO_PARROQUIAS => "Geo Parroquias",
        :GEO_DATUM => "Geo Datum",
        :GEO_COORDENADAS_UTM_N_MIN => 1.5,
        :GEO_COORDENADAS_UTM_E_MIN => 1.5,
        :GEO_COORDENADAS_UTM_N_MAX => 1.5,
        :GEO_COORDENADAS_UTM_E_MAX => 1.5,
        :GEO_DESDE => 4,
        :GEO_HASTA => 5,
        :GEO_LIM_NORTE => "Geo Lim Norte",
        :GEO_LIM_SUR => "Geo Lim Sur",
        :GEO_LIM_ESTE => "Geo Lim Este",
        :GEO_LIM_OESTE => "Geo Lim Oeste"
      ),
      stub_model(InformacionGeografica,
        :AMENAZA_ID => 1,
        :TUR_ID => 2,
        :AREA_ID => 3,
        :GEO_REGION => "Geo Region",
        :GEO_PROVINCIAS => "Geo Provincias",
        :GEO_CANTONES => "Geo Cantones",
        :GEO_PARROQUIAS => "Geo Parroquias",
        :GEO_DATUM => "Geo Datum",
        :GEO_COORDENADAS_UTM_N_MIN => 1.5,
        :GEO_COORDENADAS_UTM_E_MIN => 1.5,
        :GEO_COORDENADAS_UTM_N_MAX => 1.5,
        :GEO_COORDENADAS_UTM_E_MAX => 1.5,
        :GEO_DESDE => 4,
        :GEO_HASTA => 5,
        :GEO_LIM_NORTE => "Geo Lim Norte",
        :GEO_LIM_SUR => "Geo Lim Sur",
        :GEO_LIM_ESTE => "Geo Lim Este",
        :GEO_LIM_OESTE => "Geo Lim Oeste"
      )
    ])
  end

  it "renders a list of informacion_geograficas" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Geo Region".to_s, :count => 2
    assert_select "tr>td", :text => "Geo Provincias".to_s, :count => 2
    assert_select "tr>td", :text => "Geo Cantones".to_s, :count => 2
    assert_select "tr>td", :text => "Geo Parroquias".to_s, :count => 2
    assert_select "tr>td", :text => "Geo Datum".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => "Geo Lim Norte".to_s, :count => 2
    assert_select "tr>td", :text => "Geo Lim Sur".to_s, :count => 2
    assert_select "tr>td", :text => "Geo Lim Este".to_s, :count => 2
    assert_select "tr>td", :text => "Geo Lim Oeste".to_s, :count => 2
  end
end
