require 'spec_helper'

describe "informacion_geograficas/show" do
  before(:each) do
    @informacion_geografica = assign(:informacion_geografica, stub_model(InformacionGeografica,
      :AMENAZA_ID => 1,
      :TUR_ID => 2,
      :AREA_ID => 3,
      :GEO_REGION => "Geo Region",
      :GEO_PROVINCIAS => "Geo Provincias",
      :GEO_CANTONES => "Geo Cantones",
      :GEO_PARROQUIAS => "Geo Parroquias",
      :GEO_DATUM => "Geo Datum",
      :GEO_COORDENADAS_UTM_N_MIN => 1.5,
      :GEO_COORDENADAS_UTM_E_MIN => 1.5,
      :GEO_COORDENADAS_UTM_N_MAX => 1.5,
      :GEO_COORDENADAS_UTM_E_MAX => 1.5,
      :GEO_DESDE => 4,
      :GEO_HASTA => 5,
      :GEO_LIM_NORTE => "Geo Lim Norte",
      :GEO_LIM_SUR => "Geo Lim Sur",
      :GEO_LIM_ESTE => "Geo Lim Este",
      :GEO_LIM_OESTE => "Geo Lim Oeste"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/Geo Region/)
    rendered.should match(/Geo Provincias/)
    rendered.should match(/Geo Cantones/)
    rendered.should match(/Geo Parroquias/)
    rendered.should match(/Geo Datum/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/4/)
    rendered.should match(/5/)
    rendered.should match(/Geo Lim Norte/)
    rendered.should match(/Geo Lim Sur/)
    rendered.should match(/Geo Lim Este/)
    rendered.should match(/Geo Lim Oeste/)
  end
end
