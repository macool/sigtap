require 'spec_helper'

describe "informacion_geograficas/edit" do
  before(:each) do
    @informacion_geografica = assign(:informacion_geografica, stub_model(InformacionGeografica,
      :AMENAZA_ID => 1,
      :TUR_ID => 1,
      :AREA_ID => 1,
      :GEO_REGION => "MyString",
      :GEO_PROVINCIAS => "MyString",
      :GEO_CANTONES => "MyString",
      :GEO_PARROQUIAS => "MyString",
      :GEO_DATUM => "MyString",
      :GEO_COORDENADAS_UTM_N_MIN => 1.5,
      :GEO_COORDENADAS_UTM_E_MIN => 1.5,
      :GEO_COORDENADAS_UTM_N_MAX => 1.5,
      :GEO_COORDENADAS_UTM_E_MAX => 1.5,
      :GEO_DESDE => 1,
      :GEO_HASTA => 1,
      :GEO_LIM_NORTE => "MyString",
      :GEO_LIM_SUR => "MyString",
      :GEO_LIM_ESTE => "MyString",
      :GEO_LIM_OESTE => "MyString"
    ))
  end

  it "renders the edit informacion_geografica form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => informacion_geograficas_path(@informacion_geografica), :method => "post" do
      assert_select "input#informacion_geografica_AMENAZA_ID", :name => "informacion_geografica[AMENAZA_ID]"
      assert_select "input#informacion_geografica_TUR_ID", :name => "informacion_geografica[TUR_ID]"
      assert_select "input#informacion_geografica_AREA_ID", :name => "informacion_geografica[AREA_ID]"
      assert_select "input#informacion_geografica_GEO_REGION", :name => "informacion_geografica[GEO_REGION]"
      assert_select "input#informacion_geografica_GEO_PROVINCIAS", :name => "informacion_geografica[GEO_PROVINCIAS]"
      assert_select "input#informacion_geografica_GEO_CANTONES", :name => "informacion_geografica[GEO_CANTONES]"
      assert_select "input#informacion_geografica_GEO_PARROQUIAS", :name => "informacion_geografica[GEO_PARROQUIAS]"
      assert_select "input#informacion_geografica_GEO_DATUM", :name => "informacion_geografica[GEO_DATUM]"
      assert_select "input#informacion_geografica_GEO_COORDENADAS_UTM_N_MIN", :name => "informacion_geografica[GEO_COORDENADAS_UTM_N_MIN]"
      assert_select "input#informacion_geografica_GEO_COORDENADAS_UTM_E_MIN", :name => "informacion_geografica[GEO_COORDENADAS_UTM_E_MIN]"
      assert_select "input#informacion_geografica_GEO_COORDENADAS_UTM_N_MAX", :name => "informacion_geografica[GEO_COORDENADAS_UTM_N_MAX]"
      assert_select "input#informacion_geografica_GEO_COORDENADAS_UTM_E_MAX", :name => "informacion_geografica[GEO_COORDENADAS_UTM_E_MAX]"
      assert_select "input#informacion_geografica_GEO_DESDE", :name => "informacion_geografica[GEO_DESDE]"
      assert_select "input#informacion_geografica_GEO_HASTA", :name => "informacion_geografica[GEO_HASTA]"
      assert_select "input#informacion_geografica_GEO_LIM_NORTE", :name => "informacion_geografica[GEO_LIM_NORTE]"
      assert_select "input#informacion_geografica_GEO_LIM_SUR", :name => "informacion_geografica[GEO_LIM_SUR]"
      assert_select "input#informacion_geografica_GEO_LIM_ESTE", :name => "informacion_geografica[GEO_LIM_ESTE]"
      assert_select "input#informacion_geografica_GEO_LIM_OESTE", :name => "informacion_geografica[GEO_LIM_OESTE]"
    end
  end
end
