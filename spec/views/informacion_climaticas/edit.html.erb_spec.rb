require 'spec_helper'

describe "informacion_climaticas/edit" do
  before(:each) do
    @informacion_climatica = assign(:informacion_climatica, stub_model(InformacionClimatica,
      :AREA_ID => "MyString",
      :CLIMA_PRECIP_DESDE => 1.5,
      :CLIMA_PRECIP_HASTA => 1.5,
      :CLIMA_TEMP_DESDE => 1.5,
      :CLIMA_TEMP_HASTA => 1.5,
      :CLIMA_HUME_DESDE => 1.5,
      :CLIMA_HUME_HASTA => 1.5
    ))
  end

  it "renders the edit informacion_climatica form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => informacion_climaticas_path(@informacion_climatica), :method => "post" do
      assert_select "input#informacion_climatica_AREA_ID", :name => "informacion_climatica[AREA_ID]"
      assert_select "input#informacion_climatica_CLIMA_PRECIP_DESDE", :name => "informacion_climatica[CLIMA_PRECIP_DESDE]"
      assert_select "input#informacion_climatica_CLIMA_PRECIP_HASTA", :name => "informacion_climatica[CLIMA_PRECIP_HASTA]"
      assert_select "input#informacion_climatica_CLIMA_TEMP_DESDE", :name => "informacion_climatica[CLIMA_TEMP_DESDE]"
      assert_select "input#informacion_climatica_CLIMA_TEMP_HASTA", :name => "informacion_climatica[CLIMA_TEMP_HASTA]"
      assert_select "input#informacion_climatica_CLIMA_HUME_DESDE", :name => "informacion_climatica[CLIMA_HUME_DESDE]"
      assert_select "input#informacion_climatica_CLIMA_HUME_HASTA", :name => "informacion_climatica[CLIMA_HUME_HASTA]"
    end
  end
end
