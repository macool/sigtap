require 'spec_helper'

describe "informacion_climaticas/index" do
  before(:each) do
    assign(:informacion_climaticas, [
      stub_model(InformacionClimatica,
        :AREA_ID => "Area",
        :CLIMA_PRECIP_DESDE => 1.5,
        :CLIMA_PRECIP_HASTA => 1.5,
        :CLIMA_TEMP_DESDE => 1.5,
        :CLIMA_TEMP_HASTA => 1.5,
        :CLIMA_HUME_DESDE => 1.5,
        :CLIMA_HUME_HASTA => 1.5
      ),
      stub_model(InformacionClimatica,
        :AREA_ID => "Area",
        :CLIMA_PRECIP_DESDE => 1.5,
        :CLIMA_PRECIP_HASTA => 1.5,
        :CLIMA_TEMP_DESDE => 1.5,
        :CLIMA_TEMP_HASTA => 1.5,
        :CLIMA_HUME_DESDE => 1.5,
        :CLIMA_HUME_HASTA => 1.5
      )
    ])
  end

  it "renders a list of informacion_climaticas" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Area".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
