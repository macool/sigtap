require 'spec_helper'

describe "informacion_climaticas/show" do
  before(:each) do
    @informacion_climatica = assign(:informacion_climatica, stub_model(InformacionClimatica,
      :AREA_ID => "Area",
      :CLIMA_PRECIP_DESDE => 1.5,
      :CLIMA_PRECIP_HASTA => 1.5,
      :CLIMA_TEMP_DESDE => 1.5,
      :CLIMA_TEMP_HASTA => 1.5,
      :CLIMA_HUME_DESDE => 1.5,
      :CLIMA_HUME_HASTA => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Area/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
  end
end
