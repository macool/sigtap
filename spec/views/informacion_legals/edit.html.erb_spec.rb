require 'spec_helper'

describe "informacion_legals/edit" do
  before(:each) do
    @informacion_legal = assign(:informacion_legal, stub_model(InformacionLegal,
      :AREA_ID => 1,
      :LEGAL_TIPO => "MyString",
      :LEGAL_NUMERO => 1
    ))
  end

  it "renders the edit informacion_legal form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => informacion_legals_path(@informacion_legal), :method => "post" do
      assert_select "input#informacion_legal_AREA_ID", :name => "informacion_legal[AREA_ID]"
      assert_select "input#informacion_legal_LEGAL_TIPO", :name => "informacion_legal[LEGAL_TIPO]"
      assert_select "input#informacion_legal_LEGAL_NUMERO", :name => "informacion_legal[LEGAL_NUMERO]"
    end
  end
end
