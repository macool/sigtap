require 'spec_helper'

describe "informacion_legals/index" do
  before(:each) do
    assign(:informacion_legals, [
      stub_model(InformacionLegal,
        :AREA_ID => 1,
        :LEGAL_TIPO => "Legal Tipo",
        :LEGAL_NUMERO => 2
      ),
      stub_model(InformacionLegal,
        :AREA_ID => 1,
        :LEGAL_TIPO => "Legal Tipo",
        :LEGAL_NUMERO => 2
      )
    ])
  end

  it "renders a list of informacion_legals" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Legal Tipo".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
