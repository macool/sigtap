require 'spec_helper'

describe "informacion_legals/show" do
  before(:each) do
    @informacion_legal = assign(:informacion_legal, stub_model(InformacionLegal,
      :AREA_ID => 1,
      :LEGAL_TIPO => "Legal Tipo",
      :LEGAL_NUMERO => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Legal Tipo/)
    rendered.should match(/2/)
  end
end
