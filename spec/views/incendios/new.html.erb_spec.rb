require 'spec_helper'

describe "incendios/new" do
  before(:each) do
    assign(:incendio, stub_model(Incendio,
      :AMENAZA_ID => 1,
      :INC_AREA => 1.5,
      :INC_ECOSISTEMA => "MyText",
      :INC_ORIGEN => "MyString"
    ).as_new_record)
  end

  it "renders new incendio form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => incendios_path, :method => "post" do
      assert_select "input#incendio_AMENAZA_ID", :name => "incendio[AMENAZA_ID]"
      assert_select "input#incendio_INC_AREA", :name => "incendio[INC_AREA]"
      assert_select "textarea#incendio_INC_ECOSISTEMA", :name => "incendio[INC_ECOSISTEMA]"
      assert_select "input#incendio_INC_ORIGEN", :name => "incendio[INC_ORIGEN]"
    end
  end
end
