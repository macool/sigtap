require 'spec_helper'

describe "incendios/edit" do
  before(:each) do
    @incendio = assign(:incendio, stub_model(Incendio,
      :AMENAZA_ID => 1,
      :INC_AREA => 1.5,
      :INC_ECOSISTEMA => "MyText",
      :INC_ORIGEN => "MyString"
    ))
  end

  it "renders the edit incendio form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => incendios_path(@incendio), :method => "post" do
      assert_select "input#incendio_AMENAZA_ID", :name => "incendio[AMENAZA_ID]"
      assert_select "input#incendio_INC_AREA", :name => "incendio[INC_AREA]"
      assert_select "textarea#incendio_INC_ECOSISTEMA", :name => "incendio[INC_ECOSISTEMA]"
      assert_select "input#incendio_INC_ORIGEN", :name => "incendio[INC_ORIGEN]"
    end
  end
end
