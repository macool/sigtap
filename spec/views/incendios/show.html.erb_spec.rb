require 'spec_helper'

describe "incendios/show" do
  before(:each) do
    @incendio = assign(:incendio, stub_model(Incendio,
      :AMENAZA_ID => 1,
      :INC_AREA => 1.5,
      :INC_ECOSISTEMA => "MyText",
      :INC_ORIGEN => "Inc Origen"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/1.5/)
    rendered.should match(/MyText/)
    rendered.should match(/Inc Origen/)
  end
end
