require 'spec_helper'

describe "incendios/index" do
  before(:each) do
    assign(:incendios, [
      stub_model(Incendio,
        :AMENAZA_ID => 1,
        :INC_AREA => 1.5,
        :INC_ECOSISTEMA => "MyText",
        :INC_ORIGEN => "Inc Origen"
      ),
      stub_model(Incendio,
        :AMENAZA_ID => 1,
        :INC_AREA => 1.5,
        :INC_ECOSISTEMA => "MyText",
        :INC_ORIGEN => "Inc Origen"
      )
    ])
  end

  it "renders a list of incendios" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Inc Origen".to_s, :count => 2
  end
end
