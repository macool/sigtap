require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe ActividadAgropecuariaController do

  # This should return the minimal set of attributes required to create a valid
  # ActividadAgropecuarium. As you add validations to ActividadAgropecuarium, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "AMENAZA_ID" => "1" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # ActividadAgropecuariaController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all actividad_agropecuaria as @actividad_agropecuaria" do
      actividad_agropecuarium = ActividadAgropecuarium.create! valid_attributes
      get :index, {}, valid_session
      assigns(:actividad_agropecuaria).should eq([actividad_agropecuarium])
    end
  end

  describe "GET show" do
    it "assigns the requested actividad_agropecuarium as @actividad_agropecuarium" do
      actividad_agropecuarium = ActividadAgropecuarium.create! valid_attributes
      get :show, {:id => actividad_agropecuarium.to_param}, valid_session
      assigns(:actividad_agropecuarium).should eq(actividad_agropecuarium)
    end
  end

  describe "GET new" do
    it "assigns a new actividad_agropecuarium as @actividad_agropecuarium" do
      get :new, {}, valid_session
      assigns(:actividad_agropecuarium).should be_a_new(ActividadAgropecuarium)
    end
  end

  describe "GET edit" do
    it "assigns the requested actividad_agropecuarium as @actividad_agropecuarium" do
      actividad_agropecuarium = ActividadAgropecuarium.create! valid_attributes
      get :edit, {:id => actividad_agropecuarium.to_param}, valid_session
      assigns(:actividad_agropecuarium).should eq(actividad_agropecuarium)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new ActividadAgropecuarium" do
        expect {
          post :create, {:actividad_agropecuarium => valid_attributes}, valid_session
        }.to change(ActividadAgropecuarium, :count).by(1)
      end

      it "assigns a newly created actividad_agropecuarium as @actividad_agropecuarium" do
        post :create, {:actividad_agropecuarium => valid_attributes}, valid_session
        assigns(:actividad_agropecuarium).should be_a(ActividadAgropecuarium)
        assigns(:actividad_agropecuarium).should be_persisted
      end

      it "redirects to the created actividad_agropecuarium" do
        post :create, {:actividad_agropecuarium => valid_attributes}, valid_session
        response.should redirect_to(ActividadAgropecuarium.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved actividad_agropecuarium as @actividad_agropecuarium" do
        # Trigger the behavior that occurs when invalid params are submitted
        ActividadAgropecuarium.any_instance.stub(:save).and_return(false)
        post :create, {:actividad_agropecuarium => { "AMENAZA_ID" => "invalid value" }}, valid_session
        assigns(:actividad_agropecuarium).should be_a_new(ActividadAgropecuarium)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        ActividadAgropecuarium.any_instance.stub(:save).and_return(false)
        post :create, {:actividad_agropecuarium => { "AMENAZA_ID" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested actividad_agropecuarium" do
        actividad_agropecuarium = ActividadAgropecuarium.create! valid_attributes
        # Assuming there are no other actividad_agropecuaria in the database, this
        # specifies that the ActividadAgropecuarium created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        ActividadAgropecuarium.any_instance.should_receive(:update_attributes).with({ "AMENAZA_ID" => "1" })
        put :update, {:id => actividad_agropecuarium.to_param, :actividad_agropecuarium => { "AMENAZA_ID" => "1" }}, valid_session
      end

      it "assigns the requested actividad_agropecuarium as @actividad_agropecuarium" do
        actividad_agropecuarium = ActividadAgropecuarium.create! valid_attributes
        put :update, {:id => actividad_agropecuarium.to_param, :actividad_agropecuarium => valid_attributes}, valid_session
        assigns(:actividad_agropecuarium).should eq(actividad_agropecuarium)
      end

      it "redirects to the actividad_agropecuarium" do
        actividad_agropecuarium = ActividadAgropecuarium.create! valid_attributes
        put :update, {:id => actividad_agropecuarium.to_param, :actividad_agropecuarium => valid_attributes}, valid_session
        response.should redirect_to(actividad_agropecuarium)
      end
    end

    describe "with invalid params" do
      it "assigns the actividad_agropecuarium as @actividad_agropecuarium" do
        actividad_agropecuarium = ActividadAgropecuarium.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        ActividadAgropecuarium.any_instance.stub(:save).and_return(false)
        put :update, {:id => actividad_agropecuarium.to_param, :actividad_agropecuarium => { "AMENAZA_ID" => "invalid value" }}, valid_session
        assigns(:actividad_agropecuarium).should eq(actividad_agropecuarium)
      end

      it "re-renders the 'edit' template" do
        actividad_agropecuarium = ActividadAgropecuarium.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        ActividadAgropecuarium.any_instance.stub(:save).and_return(false)
        put :update, {:id => actividad_agropecuarium.to_param, :actividad_agropecuarium => { "AMENAZA_ID" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested actividad_agropecuarium" do
      actividad_agropecuarium = ActividadAgropecuarium.create! valid_attributes
      expect {
        delete :destroy, {:id => actividad_agropecuarium.to_param}, valid_session
      }.to change(ActividadAgropecuarium, :count).by(-1)
    end

    it "redirects to the actividad_agropecuaria list" do
      actividad_agropecuarium = ActividadAgropecuarium.create! valid_attributes
      delete :destroy, {:id => actividad_agropecuarium.to_param}, valid_session
      response.should redirect_to(actividad_agropecuaria_url)
    end
  end

end
