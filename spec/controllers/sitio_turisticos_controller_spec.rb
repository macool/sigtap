require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe SitioTuristicosController do

  # This should return the minimal set of attributes required to create a valid
  # SitioTuristico. As you add validations to SitioTuristico, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "GEO_ID" => "1" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # SitioTuristicosController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all sitio_turisticos as @sitio_turisticos" do
      sitio_turistico = SitioTuristico.create! valid_attributes
      get :index, {}, valid_session
      assigns(:sitio_turisticos).should eq([sitio_turistico])
    end
  end

  describe "GET show" do
    it "assigns the requested sitio_turistico as @sitio_turistico" do
      sitio_turistico = SitioTuristico.create! valid_attributes
      get :show, {:id => sitio_turistico.to_param}, valid_session
      assigns(:sitio_turistico).should eq(sitio_turistico)
    end
  end

  describe "GET new" do
    it "assigns a new sitio_turistico as @sitio_turistico" do
      get :new, {}, valid_session
      assigns(:sitio_turistico).should be_a_new(SitioTuristico)
    end
  end

  describe "GET edit" do
    it "assigns the requested sitio_turistico as @sitio_turistico" do
      sitio_turistico = SitioTuristico.create! valid_attributes
      get :edit, {:id => sitio_turistico.to_param}, valid_session
      assigns(:sitio_turistico).should eq(sitio_turistico)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new SitioTuristico" do
        expect {
          post :create, {:sitio_turistico => valid_attributes}, valid_session
        }.to change(SitioTuristico, :count).by(1)
      end

      it "assigns a newly created sitio_turistico as @sitio_turistico" do
        post :create, {:sitio_turistico => valid_attributes}, valid_session
        assigns(:sitio_turistico).should be_a(SitioTuristico)
        assigns(:sitio_turistico).should be_persisted
      end

      it "redirects to the created sitio_turistico" do
        post :create, {:sitio_turistico => valid_attributes}, valid_session
        response.should redirect_to(SitioTuristico.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved sitio_turistico as @sitio_turistico" do
        # Trigger the behavior that occurs when invalid params are submitted
        SitioTuristico.any_instance.stub(:save).and_return(false)
        post :create, {:sitio_turistico => { "GEO_ID" => "invalid value" }}, valid_session
        assigns(:sitio_turistico).should be_a_new(SitioTuristico)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        SitioTuristico.any_instance.stub(:save).and_return(false)
        post :create, {:sitio_turistico => { "GEO_ID" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested sitio_turistico" do
        sitio_turistico = SitioTuristico.create! valid_attributes
        # Assuming there are no other sitio_turisticos in the database, this
        # specifies that the SitioTuristico created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        SitioTuristico.any_instance.should_receive(:update_attributes).with({ "GEO_ID" => "1" })
        put :update, {:id => sitio_turistico.to_param, :sitio_turistico => { "GEO_ID" => "1" }}, valid_session
      end

      it "assigns the requested sitio_turistico as @sitio_turistico" do
        sitio_turistico = SitioTuristico.create! valid_attributes
        put :update, {:id => sitio_turistico.to_param, :sitio_turistico => valid_attributes}, valid_session
        assigns(:sitio_turistico).should eq(sitio_turistico)
      end

      it "redirects to the sitio_turistico" do
        sitio_turistico = SitioTuristico.create! valid_attributes
        put :update, {:id => sitio_turistico.to_param, :sitio_turistico => valid_attributes}, valid_session
        response.should redirect_to(sitio_turistico)
      end
    end

    describe "with invalid params" do
      it "assigns the sitio_turistico as @sitio_turistico" do
        sitio_turistico = SitioTuristico.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        SitioTuristico.any_instance.stub(:save).and_return(false)
        put :update, {:id => sitio_turistico.to_param, :sitio_turistico => { "GEO_ID" => "invalid value" }}, valid_session
        assigns(:sitio_turistico).should eq(sitio_turistico)
      end

      it "re-renders the 'edit' template" do
        sitio_turistico = SitioTuristico.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        SitioTuristico.any_instance.stub(:save).and_return(false)
        put :update, {:id => sitio_turistico.to_param, :sitio_turistico => { "GEO_ID" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested sitio_turistico" do
      sitio_turistico = SitioTuristico.create! valid_attributes
      expect {
        delete :destroy, {:id => sitio_turistico.to_param}, valid_session
      }.to change(SitioTuristico, :count).by(-1)
    end

    it "redirects to the sitio_turisticos list" do
      sitio_turistico = SitioTuristico.create! valid_attributes
      delete :destroy, {:id => sitio_turistico.to_param}, valid_session
      response.should redirect_to(sitio_turisticos_url)
    end
  end

end
