require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe AmenazaEspeciesController do

  # This should return the minimal set of attributes required to create a valid
  # AmenazaEspecie. As you add validations to AmenazaEspecie, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "EXT_ILE_ID" => "1" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # AmenazaEspeciesController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all amenaza_especies as @amenaza_especies" do
      amenaza_especie = AmenazaEspecie.create! valid_attributes
      get :index, {}, valid_session
      assigns(:amenaza_especies).should eq([amenaza_especie])
    end
  end

  describe "GET show" do
    it "assigns the requested amenaza_especie as @amenaza_especie" do
      amenaza_especie = AmenazaEspecie.create! valid_attributes
      get :show, {:id => amenaza_especie.to_param}, valid_session
      assigns(:amenaza_especie).should eq(amenaza_especie)
    end
  end

  describe "GET new" do
    it "assigns a new amenaza_especie as @amenaza_especie" do
      get :new, {}, valid_session
      assigns(:amenaza_especie).should be_a_new(AmenazaEspecie)
    end
  end

  describe "GET edit" do
    it "assigns the requested amenaza_especie as @amenaza_especie" do
      amenaza_especie = AmenazaEspecie.create! valid_attributes
      get :edit, {:id => amenaza_especie.to_param}, valid_session
      assigns(:amenaza_especie).should eq(amenaza_especie)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new AmenazaEspecie" do
        expect {
          post :create, {:amenaza_especie => valid_attributes}, valid_session
        }.to change(AmenazaEspecie, :count).by(1)
      end

      it "assigns a newly created amenaza_especie as @amenaza_especie" do
        post :create, {:amenaza_especie => valid_attributes}, valid_session
        assigns(:amenaza_especie).should be_a(AmenazaEspecie)
        assigns(:amenaza_especie).should be_persisted
      end

      it "redirects to the created amenaza_especie" do
        post :create, {:amenaza_especie => valid_attributes}, valid_session
        response.should redirect_to(AmenazaEspecie.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved amenaza_especie as @amenaza_especie" do
        # Trigger the behavior that occurs when invalid params are submitted
        AmenazaEspecie.any_instance.stub(:save).and_return(false)
        post :create, {:amenaza_especie => { "EXT_ILE_ID" => "invalid value" }}, valid_session
        assigns(:amenaza_especie).should be_a_new(AmenazaEspecie)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        AmenazaEspecie.any_instance.stub(:save).and_return(false)
        post :create, {:amenaza_especie => { "EXT_ILE_ID" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested amenaza_especie" do
        amenaza_especie = AmenazaEspecie.create! valid_attributes
        # Assuming there are no other amenaza_especies in the database, this
        # specifies that the AmenazaEspecie created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        AmenazaEspecie.any_instance.should_receive(:update_attributes).with({ "EXT_ILE_ID" => "1" })
        put :update, {:id => amenaza_especie.to_param, :amenaza_especie => { "EXT_ILE_ID" => "1" }}, valid_session
      end

      it "assigns the requested amenaza_especie as @amenaza_especie" do
        amenaza_especie = AmenazaEspecie.create! valid_attributes
        put :update, {:id => amenaza_especie.to_param, :amenaza_especie => valid_attributes}, valid_session
        assigns(:amenaza_especie).should eq(amenaza_especie)
      end

      it "redirects to the amenaza_especie" do
        amenaza_especie = AmenazaEspecie.create! valid_attributes
        put :update, {:id => amenaza_especie.to_param, :amenaza_especie => valid_attributes}, valid_session
        response.should redirect_to(amenaza_especie)
      end
    end

    describe "with invalid params" do
      it "assigns the amenaza_especie as @amenaza_especie" do
        amenaza_especie = AmenazaEspecie.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        AmenazaEspecie.any_instance.stub(:save).and_return(false)
        put :update, {:id => amenaza_especie.to_param, :amenaza_especie => { "EXT_ILE_ID" => "invalid value" }}, valid_session
        assigns(:amenaza_especie).should eq(amenaza_especie)
      end

      it "re-renders the 'edit' template" do
        amenaza_especie = AmenazaEspecie.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        AmenazaEspecie.any_instance.stub(:save).and_return(false)
        put :update, {:id => amenaza_especie.to_param, :amenaza_especie => { "EXT_ILE_ID" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested amenaza_especie" do
      amenaza_especie = AmenazaEspecie.create! valid_attributes
      expect {
        delete :destroy, {:id => amenaza_especie.to_param}, valid_session
      }.to change(AmenazaEspecie, :count).by(-1)
    end

    it "redirects to the amenaza_especies list" do
      amenaza_especie = AmenazaEspecie.create! valid_attributes
      delete :destroy, {:id => amenaza_especie.to_param}, valid_session
      response.should redirect_to(amenaza_especies_url)
    end
  end

end
