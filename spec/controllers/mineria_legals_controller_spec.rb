require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe MineriaLegalsController do

  # This should return the minimal set of attributes required to create a valid
  # MineriaLegal. As you add validations to MineriaLegal, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    { "AMENAZA_ID" => "1" }
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # MineriaLegalsController. Be sure to keep this updated too.
  def valid_session
    {}
  end

  describe "GET index" do
    it "assigns all mineria_legals as @mineria_legals" do
      mineria_legal = MineriaLegal.create! valid_attributes
      get :index, {}, valid_session
      assigns(:mineria_legals).should eq([mineria_legal])
    end
  end

  describe "GET show" do
    it "assigns the requested mineria_legal as @mineria_legal" do
      mineria_legal = MineriaLegal.create! valid_attributes
      get :show, {:id => mineria_legal.to_param}, valid_session
      assigns(:mineria_legal).should eq(mineria_legal)
    end
  end

  describe "GET new" do
    it "assigns a new mineria_legal as @mineria_legal" do
      get :new, {}, valid_session
      assigns(:mineria_legal).should be_a_new(MineriaLegal)
    end
  end

  describe "GET edit" do
    it "assigns the requested mineria_legal as @mineria_legal" do
      mineria_legal = MineriaLegal.create! valid_attributes
      get :edit, {:id => mineria_legal.to_param}, valid_session
      assigns(:mineria_legal).should eq(mineria_legal)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new MineriaLegal" do
        expect {
          post :create, {:mineria_legal => valid_attributes}, valid_session
        }.to change(MineriaLegal, :count).by(1)
      end

      it "assigns a newly created mineria_legal as @mineria_legal" do
        post :create, {:mineria_legal => valid_attributes}, valid_session
        assigns(:mineria_legal).should be_a(MineriaLegal)
        assigns(:mineria_legal).should be_persisted
      end

      it "redirects to the created mineria_legal" do
        post :create, {:mineria_legal => valid_attributes}, valid_session
        response.should redirect_to(MineriaLegal.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved mineria_legal as @mineria_legal" do
        # Trigger the behavior that occurs when invalid params are submitted
        MineriaLegal.any_instance.stub(:save).and_return(false)
        post :create, {:mineria_legal => { "AMENAZA_ID" => "invalid value" }}, valid_session
        assigns(:mineria_legal).should be_a_new(MineriaLegal)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        MineriaLegal.any_instance.stub(:save).and_return(false)
        post :create, {:mineria_legal => { "AMENAZA_ID" => "invalid value" }}, valid_session
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested mineria_legal" do
        mineria_legal = MineriaLegal.create! valid_attributes
        # Assuming there are no other mineria_legals in the database, this
        # specifies that the MineriaLegal created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        MineriaLegal.any_instance.should_receive(:update_attributes).with({ "AMENAZA_ID" => "1" })
        put :update, {:id => mineria_legal.to_param, :mineria_legal => { "AMENAZA_ID" => "1" }}, valid_session
      end

      it "assigns the requested mineria_legal as @mineria_legal" do
        mineria_legal = MineriaLegal.create! valid_attributes
        put :update, {:id => mineria_legal.to_param, :mineria_legal => valid_attributes}, valid_session
        assigns(:mineria_legal).should eq(mineria_legal)
      end

      it "redirects to the mineria_legal" do
        mineria_legal = MineriaLegal.create! valid_attributes
        put :update, {:id => mineria_legal.to_param, :mineria_legal => valid_attributes}, valid_session
        response.should redirect_to(mineria_legal)
      end
    end

    describe "with invalid params" do
      it "assigns the mineria_legal as @mineria_legal" do
        mineria_legal = MineriaLegal.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        MineriaLegal.any_instance.stub(:save).and_return(false)
        put :update, {:id => mineria_legal.to_param, :mineria_legal => { "AMENAZA_ID" => "invalid value" }}, valid_session
        assigns(:mineria_legal).should eq(mineria_legal)
      end

      it "re-renders the 'edit' template" do
        mineria_legal = MineriaLegal.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        MineriaLegal.any_instance.stub(:save).and_return(false)
        put :update, {:id => mineria_legal.to_param, :mineria_legal => { "AMENAZA_ID" => "invalid value" }}, valid_session
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested mineria_legal" do
      mineria_legal = MineriaLegal.create! valid_attributes
      expect {
        delete :destroy, {:id => mineria_legal.to_param}, valid_session
      }.to change(MineriaLegal, :count).by(-1)
    end

    it "redirects to the mineria_legals list" do
      mineria_legal = MineriaLegal.create! valid_attributes
      delete :destroy, {:id => mineria_legal.to_param}, valid_session
      response.should redirect_to(mineria_legals_url)
    end
  end

end
