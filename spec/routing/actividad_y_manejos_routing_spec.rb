require "spec_helper"

describe ActividadYManejosController do
  describe "routing" do

    it "routes to #index" do
      get("/actividad_y_manejos").should route_to("actividad_y_manejos#index")
    end

    it "routes to #new" do
      get("/actividad_y_manejos/new").should route_to("actividad_y_manejos#new")
    end

    it "routes to #show" do
      get("/actividad_y_manejos/1").should route_to("actividad_y_manejos#show", :id => "1")
    end

    it "routes to #edit" do
      get("/actividad_y_manejos/1/edit").should route_to("actividad_y_manejos#edit", :id => "1")
    end

    it "routes to #create" do
      post("/actividad_y_manejos").should route_to("actividad_y_manejos#create")
    end

    it "routes to #update" do
      put("/actividad_y_manejos/1").should route_to("actividad_y_manejos#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/actividad_y_manejos/1").should route_to("actividad_y_manejos#destroy", :id => "1")
    end

  end
end
