require "spec_helper"

describe IncendiosController do
  describe "routing" do

    it "routes to #index" do
      get("/incendios").should route_to("incendios#index")
    end

    it "routes to #new" do
      get("/incendios/new").should route_to("incendios#new")
    end

    it "routes to #show" do
      get("/incendios/1").should route_to("incendios#show", :id => "1")
    end

    it "routes to #edit" do
      get("/incendios/1/edit").should route_to("incendios#edit", :id => "1")
    end

    it "routes to #create" do
      post("/incendios").should route_to("incendios#create")
    end

    it "routes to #update" do
      put("/incendios/1").should route_to("incendios#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/incendios/1").should route_to("incendios#destroy", :id => "1")
    end

  end
end
