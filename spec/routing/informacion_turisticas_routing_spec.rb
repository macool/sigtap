require "spec_helper"

describe InformacionTuristicasController do
  describe "routing" do

    it "routes to #index" do
      get("/informacion_turisticas").should route_to("informacion_turisticas#index")
    end

    it "routes to #new" do
      get("/informacion_turisticas/new").should route_to("informacion_turisticas#new")
    end

    it "routes to #show" do
      get("/informacion_turisticas/1").should route_to("informacion_turisticas#show", :id => "1")
    end

    it "routes to #edit" do
      get("/informacion_turisticas/1/edit").should route_to("informacion_turisticas#edit", :id => "1")
    end

    it "routes to #create" do
      post("/informacion_turisticas").should route_to("informacion_turisticas#create")
    end

    it "routes to #update" do
      put("/informacion_turisticas/1").should route_to("informacion_turisticas#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/informacion_turisticas/1").should route_to("informacion_turisticas#destroy", :id => "1")
    end

  end
end
