require "spec_helper"

describe InformacionHidrograficasController do
  describe "routing" do

    it "routes to #index" do
      get("/informacion_hidrograficas").should route_to("informacion_hidrograficas#index")
    end

    it "routes to #new" do
      get("/informacion_hidrograficas/new").should route_to("informacion_hidrograficas#new")
    end

    it "routes to #show" do
      get("/informacion_hidrograficas/1").should route_to("informacion_hidrograficas#show", :id => "1")
    end

    it "routes to #edit" do
      get("/informacion_hidrograficas/1/edit").should route_to("informacion_hidrograficas#edit", :id => "1")
    end

    it "routes to #create" do
      post("/informacion_hidrograficas").should route_to("informacion_hidrograficas#create")
    end

    it "routes to #update" do
      put("/informacion_hidrograficas/1").should route_to("informacion_hidrograficas#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/informacion_hidrograficas/1").should route_to("informacion_hidrograficas#destroy", :id => "1")
    end

  end
end
