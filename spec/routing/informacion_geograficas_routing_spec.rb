require "spec_helper"

describe InformacionGeograficasController do
  describe "routing" do

    it "routes to #index" do
      get("/informacion_geograficas").should route_to("informacion_geograficas#index")
    end

    it "routes to #new" do
      get("/informacion_geograficas/new").should route_to("informacion_geograficas#new")
    end

    it "routes to #show" do
      get("/informacion_geograficas/1").should route_to("informacion_geograficas#show", :id => "1")
    end

    it "routes to #edit" do
      get("/informacion_geograficas/1/edit").should route_to("informacion_geograficas#edit", :id => "1")
    end

    it "routes to #create" do
      post("/informacion_geograficas").should route_to("informacion_geograficas#create")
    end

    it "routes to #update" do
      put("/informacion_geograficas/1").should route_to("informacion_geograficas#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/informacion_geograficas/1").should route_to("informacion_geograficas#destroy", :id => "1")
    end

  end
end
