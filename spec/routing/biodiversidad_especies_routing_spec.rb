require "spec_helper"

describe BiodiversidadEspeciesController do
  describe "routing" do

    it "routes to #index" do
      get("/biodiversidad_especies").should route_to("biodiversidad_especies#index")
    end

    it "routes to #new" do
      get("/biodiversidad_especies/new").should route_to("biodiversidad_especies#new")
    end

    it "routes to #show" do
      get("/biodiversidad_especies/1").should route_to("biodiversidad_especies#show", :id => "1")
    end

    it "routes to #edit" do
      get("/biodiversidad_especies/1/edit").should route_to("biodiversidad_especies#edit", :id => "1")
    end

    it "routes to #create" do
      post("/biodiversidad_especies").should route_to("biodiversidad_especies#create")
    end

    it "routes to #update" do
      put("/biodiversidad_especies/1").should route_to("biodiversidad_especies#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/biodiversidad_especies/1").should route_to("biodiversidad_especies#destroy", :id => "1")
    end

  end
end
