require "spec_helper"

describe OtraInfraestructurasController do
  describe "routing" do

    it "routes to #index" do
      get("/otra_infraestructuras").should route_to("otra_infraestructuras#index")
    end

    it "routes to #new" do
      get("/otra_infraestructuras/new").should route_to("otra_infraestructuras#new")
    end

    it "routes to #show" do
      get("/otra_infraestructuras/1").should route_to("otra_infraestructuras#show", :id => "1")
    end

    it "routes to #edit" do
      get("/otra_infraestructuras/1/edit").should route_to("otra_infraestructuras#edit", :id => "1")
    end

    it "routes to #create" do
      post("/otra_infraestructuras").should route_to("otra_infraestructuras#create")
    end

    it "routes to #update" do
      put("/otra_infraestructuras/1").should route_to("otra_infraestructuras#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/otra_infraestructuras/1").should route_to("otra_infraestructuras#destroy", :id => "1")
    end

  end
end
