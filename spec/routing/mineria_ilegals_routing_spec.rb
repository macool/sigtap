require "spec_helper"

describe MineriaIlegalsController do
  describe "routing" do

    it "routes to #index" do
      get("/mineria_ilegals").should route_to("mineria_ilegals#index")
    end

    it "routes to #new" do
      get("/mineria_ilegals/new").should route_to("mineria_ilegals#new")
    end

    it "routes to #show" do
      get("/mineria_ilegals/1").should route_to("mineria_ilegals#show", :id => "1")
    end

    it "routes to #edit" do
      get("/mineria_ilegals/1/edit").should route_to("mineria_ilegals#edit", :id => "1")
    end

    it "routes to #create" do
      post("/mineria_ilegals").should route_to("mineria_ilegals#create")
    end

    it "routes to #update" do
      put("/mineria_ilegals/1").should route_to("mineria_ilegals#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/mineria_ilegals/1").should route_to("mineria_ilegals#destroy", :id => "1")
    end

  end
end
