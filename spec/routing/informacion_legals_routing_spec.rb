require "spec_helper"

describe InformacionLegalsController do
  describe "routing" do

    it "routes to #index" do
      get("/informacion_legals").should route_to("informacion_legals#index")
    end

    it "routes to #new" do
      get("/informacion_legals/new").should route_to("informacion_legals#new")
    end

    it "routes to #show" do
      get("/informacion_legals/1").should route_to("informacion_legals#show", :id => "1")
    end

    it "routes to #edit" do
      get("/informacion_legals/1/edit").should route_to("informacion_legals#edit", :id => "1")
    end

    it "routes to #create" do
      post("/informacion_legals").should route_to("informacion_legals#create")
    end

    it "routes to #update" do
      put("/informacion_legals/1").should route_to("informacion_legals#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/informacion_legals/1").should route_to("informacion_legals#destroy", :id => "1")
    end

  end
end
