require "spec_helper"

describe InvasionsController do
  describe "routing" do

    it "routes to #index" do
      get("/invasions").should route_to("invasions#index")
    end

    it "routes to #new" do
      get("/invasions/new").should route_to("invasions#new")
    end

    it "routes to #show" do
      get("/invasions/1").should route_to("invasions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/invasions/1/edit").should route_to("invasions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/invasions").should route_to("invasions#create")
    end

    it "routes to #update" do
      put("/invasions/1").should route_to("invasions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/invasions/1").should route_to("invasions#destroy", :id => "1")
    end

  end
end
