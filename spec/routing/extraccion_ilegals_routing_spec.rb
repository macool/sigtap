require "spec_helper"

describe ExtraccionIlegalsController do
  describe "routing" do

    it "routes to #index" do
      get("/extraccion_ilegals").should route_to("extraccion_ilegals#index")
    end

    it "routes to #new" do
      get("/extraccion_ilegals/new").should route_to("extraccion_ilegals#new")
    end

    it "routes to #show" do
      get("/extraccion_ilegals/1").should route_to("extraccion_ilegals#show", :id => "1")
    end

    it "routes to #edit" do
      get("/extraccion_ilegals/1/edit").should route_to("extraccion_ilegals#edit", :id => "1")
    end

    it "routes to #create" do
      post("/extraccion_ilegals").should route_to("extraccion_ilegals#create")
    end

    it "routes to #update" do
      put("/extraccion_ilegals/1").should route_to("extraccion_ilegals#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/extraccion_ilegals/1").should route_to("extraccion_ilegals#destroy", :id => "1")
    end

  end
end
