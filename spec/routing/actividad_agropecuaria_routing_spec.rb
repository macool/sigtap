require "spec_helper"

describe ActividadAgropecuariaController do
  describe "routing" do

    it "routes to #index" do
      get("/actividad_agropecuaria").should route_to("actividad_agropecuaria#index")
    end

    it "routes to #new" do
      get("/actividad_agropecuaria/new").should route_to("actividad_agropecuaria#new")
    end

    it "routes to #show" do
      get("/actividad_agropecuaria/1").should route_to("actividad_agropecuaria#show", :id => "1")
    end

    it "routes to #edit" do
      get("/actividad_agropecuaria/1/edit").should route_to("actividad_agropecuaria#edit", :id => "1")
    end

    it "routes to #create" do
      post("/actividad_agropecuaria").should route_to("actividad_agropecuaria#create")
    end

    it "routes to #update" do
      put("/actividad_agropecuaria/1").should route_to("actividad_agropecuaria#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/actividad_agropecuaria/1").should route_to("actividad_agropecuaria#destroy", :id => "1")
    end

  end
end
