require "spec_helper"

describe MineriaLegalsController do
  describe "routing" do

    it "routes to #index" do
      get("/mineria_legals").should route_to("mineria_legals#index")
    end

    it "routes to #new" do
      get("/mineria_legals/new").should route_to("mineria_legals#new")
    end

    it "routes to #show" do
      get("/mineria_legals/1").should route_to("mineria_legals#show", :id => "1")
    end

    it "routes to #edit" do
      get("/mineria_legals/1/edit").should route_to("mineria_legals#edit", :id => "1")
    end

    it "routes to #create" do
      post("/mineria_legals").should route_to("mineria_legals#create")
    end

    it "routes to #update" do
      put("/mineria_legals/1").should route_to("mineria_legals#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/mineria_legals/1").should route_to("mineria_legals#destroy", :id => "1")
    end

  end
end
