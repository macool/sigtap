require "spec_helper"

describe AmenazasController do
  describe "routing" do

    it "routes to #index" do
      get("/amenazas").should route_to("amenazas#index")
    end

    it "routes to #new" do
      get("/amenazas/new").should route_to("amenazas#new")
    end

    it "routes to #show" do
      get("/amenazas/1").should route_to("amenazas#show", :id => "1")
    end

    it "routes to #edit" do
      get("/amenazas/1/edit").should route_to("amenazas#edit", :id => "1")
    end

    it "routes to #create" do
      post("/amenazas").should route_to("amenazas#create")
    end

    it "routes to #update" do
      put("/amenazas/1").should route_to("amenazas#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/amenazas/1").should route_to("amenazas#destroy", :id => "1")
    end

  end
end
