require "spec_helper"

describe ComunidadsController do
  describe "routing" do

    it "routes to #index" do
      get("/comunidads").should route_to("comunidads#index")
    end

    it "routes to #new" do
      get("/comunidads/new").should route_to("comunidads#new")
    end

    it "routes to #show" do
      get("/comunidads/1").should route_to("comunidads#show", :id => "1")
    end

    it "routes to #edit" do
      get("/comunidads/1/edit").should route_to("comunidads#edit", :id => "1")
    end

    it "routes to #create" do
      post("/comunidads").should route_to("comunidads#create")
    end

    it "routes to #update" do
      put("/comunidads/1").should route_to("comunidads#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/comunidads/1").should route_to("comunidads#destroy", :id => "1")
    end

  end
end
