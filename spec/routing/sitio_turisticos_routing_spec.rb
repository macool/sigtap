require "spec_helper"

describe SitioTuristicosController do
  describe "routing" do

    it "routes to #index" do
      get("/sitio_turisticos").should route_to("sitio_turisticos#index")
    end

    it "routes to #new" do
      get("/sitio_turisticos/new").should route_to("sitio_turisticos#new")
    end

    it "routes to #show" do
      get("/sitio_turisticos/1").should route_to("sitio_turisticos#show", :id => "1")
    end

    it "routes to #edit" do
      get("/sitio_turisticos/1/edit").should route_to("sitio_turisticos#edit", :id => "1")
    end

    it "routes to #create" do
      post("/sitio_turisticos").should route_to("sitio_turisticos#create")
    end

    it "routes to #update" do
      put("/sitio_turisticos/1").should route_to("sitio_turisticos#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/sitio_turisticos/1").should route_to("sitio_turisticos#destroy", :id => "1")
    end

  end
end
