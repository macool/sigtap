require "spec_helper"

describe InformacionClimaticasController do
  describe "routing" do

    it "routes to #index" do
      get("/informacion_climaticas").should route_to("informacion_climaticas#index")
    end

    it "routes to #new" do
      get("/informacion_climaticas/new").should route_to("informacion_climaticas#new")
    end

    it "routes to #show" do
      get("/informacion_climaticas/1").should route_to("informacion_climaticas#show", :id => "1")
    end

    it "routes to #edit" do
      get("/informacion_climaticas/1/edit").should route_to("informacion_climaticas#edit", :id => "1")
    end

    it "routes to #create" do
      post("/informacion_climaticas").should route_to("informacion_climaticas#create")
    end

    it "routes to #update" do
      put("/informacion_climaticas/1").should route_to("informacion_climaticas#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/informacion_climaticas/1").should route_to("informacion_climaticas#destroy", :id => "1")
    end

  end
end
