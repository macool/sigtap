require "spec_helper"

describe BiodiversidadsController do
  describe "routing" do

    it "routes to #index" do
      get("/biodiversidads").should route_to("biodiversidads#index")
    end

    it "routes to #new" do
      get("/biodiversidads/new").should route_to("biodiversidads#new")
    end

    it "routes to #show" do
      get("/biodiversidads/1").should route_to("biodiversidads#show", :id => "1")
    end

    it "routes to #edit" do
      get("/biodiversidads/1/edit").should route_to("biodiversidads#edit", :id => "1")
    end

    it "routes to #create" do
      post("/biodiversidads").should route_to("biodiversidads#create")
    end

    it "routes to #update" do
      put("/biodiversidads/1").should route_to("biodiversidads#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/biodiversidads/1").should route_to("biodiversidads#destroy", :id => "1")
    end

  end
end
