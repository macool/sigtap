require "spec_helper"

describe ConflictoSocioambientalsController do
  describe "routing" do

    it "routes to #index" do
      get("/conflicto_socioambientals").should route_to("conflicto_socioambientals#index")
    end

    it "routes to #new" do
      get("/conflicto_socioambientals/new").should route_to("conflicto_socioambientals#new")
    end

    it "routes to #show" do
      get("/conflicto_socioambientals/1").should route_to("conflicto_socioambientals#show", :id => "1")
    end

    it "routes to #edit" do
      get("/conflicto_socioambientals/1/edit").should route_to("conflicto_socioambientals#edit", :id => "1")
    end

    it "routes to #create" do
      post("/conflicto_socioambientals").should route_to("conflicto_socioambientals#create")
    end

    it "routes to #update" do
      put("/conflicto_socioambientals/1").should route_to("conflicto_socioambientals#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/conflicto_socioambientals/1").should route_to("conflicto_socioambientals#destroy", :id => "1")
    end

  end
end
