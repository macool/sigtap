require "spec_helper"

describe DeforestacionsController do
  describe "routing" do

    it "routes to #index" do
      get("/deforestacions").should route_to("deforestacions#index")
    end

    it "routes to #new" do
      get("/deforestacions/new").should route_to("deforestacions#new")
    end

    it "routes to #show" do
      get("/deforestacions/1").should route_to("deforestacions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/deforestacions/1/edit").should route_to("deforestacions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/deforestacions").should route_to("deforestacions#create")
    end

    it "routes to #update" do
      put("/deforestacions/1").should route_to("deforestacions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/deforestacions/1").should route_to("deforestacions#destroy", :id => "1")
    end

  end
end
