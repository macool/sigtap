require "spec_helper"

describe ActividadHidrocarburiferasController do
  describe "routing" do

    it "routes to #index" do
      get("/actividad_hidrocarburiferas").should route_to("actividad_hidrocarburiferas#index")
    end

    it "routes to #new" do
      get("/actividad_hidrocarburiferas/new").should route_to("actividad_hidrocarburiferas#new")
    end

    it "routes to #show" do
      get("/actividad_hidrocarburiferas/1").should route_to("actividad_hidrocarburiferas#show", :id => "1")
    end

    it "routes to #edit" do
      get("/actividad_hidrocarburiferas/1/edit").should route_to("actividad_hidrocarburiferas#edit", :id => "1")
    end

    it "routes to #create" do
      post("/actividad_hidrocarburiferas").should route_to("actividad_hidrocarburiferas#create")
    end

    it "routes to #update" do
      put("/actividad_hidrocarburiferas/1").should route_to("actividad_hidrocarburiferas#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/actividad_hidrocarburiferas/1").should route_to("actividad_hidrocarburiferas#destroy", :id => "1")
    end

  end
end
