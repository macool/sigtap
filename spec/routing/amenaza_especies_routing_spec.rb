require "spec_helper"

describe AmenazaEspeciesController do
  describe "routing" do

    it "routes to #index" do
      get("/amenaza_especies").should route_to("amenaza_especies#index")
    end

    it "routes to #new" do
      get("/amenaza_especies/new").should route_to("amenaza_especies#new")
    end

    it "routes to #show" do
      get("/amenaza_especies/1").should route_to("amenaza_especies#show", :id => "1")
    end

    it "routes to #edit" do
      get("/amenaza_especies/1/edit").should route_to("amenaza_especies#edit", :id => "1")
    end

    it "routes to #create" do
      post("/amenaza_especies").should route_to("amenaza_especies#create")
    end

    it "routes to #update" do
      put("/amenaza_especies/1").should route_to("amenaza_especies#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/amenaza_especies/1").should route_to("amenaza_especies#destroy", :id => "1")
    end

  end
end
