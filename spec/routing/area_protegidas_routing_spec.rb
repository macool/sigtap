require "spec_helper"

describe AreaProtegidasController do
  describe "routing" do

    it "routes to #index" do
      get("/area_protegidas").should route_to("area_protegidas#index")
    end

    it "routes to #new" do
      get("/area_protegidas/new").should route_to("area_protegidas#new")
    end

    it "routes to #show" do
      get("/area_protegidas/1").should route_to("area_protegidas#show", :id => "1")
    end

    it "routes to #edit" do
      get("/area_protegidas/1/edit").should route_to("area_protegidas#edit", :id => "1")
    end

    it "routes to #create" do
      post("/area_protegidas").should route_to("area_protegidas#create")
    end

    it "routes to #update" do
      put("/area_protegidas/1").should route_to("area_protegidas#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/area_protegidas/1").should route_to("area_protegidas#destroy", :id => "1")
    end

  end
end
