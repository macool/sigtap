require "spec_helper"

describe CategorizacionsController do
  describe "routing" do

    it "routes to #index" do
      get("/categorizacions").should route_to("categorizacions#index")
    end

    it "routes to #new" do
      get("/categorizacions/new").should route_to("categorizacions#new")
    end

    it "routes to #show" do
      get("/categorizacions/1").should route_to("categorizacions#show", :id => "1")
    end

    it "routes to #edit" do
      get("/categorizacions/1/edit").should route_to("categorizacions#edit", :id => "1")
    end

    it "routes to #create" do
      post("/categorizacions").should route_to("categorizacions#create")
    end

    it "routes to #update" do
      put("/categorizacions/1").should route_to("categorizacions#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/categorizacions/1").should route_to("categorizacions#destroy", :id => "1")
    end

  end
end
