require "spec_helper"

describe EspecieIntroducidasController do
  describe "routing" do

    it "routes to #index" do
      get("/especie_introducidas").should route_to("especie_introducidas#index")
    end

    it "routes to #new" do
      get("/especie_introducidas/new").should route_to("especie_introducidas#new")
    end

    it "routes to #show" do
      get("/especie_introducidas/1").should route_to("especie_introducidas#show", :id => "1")
    end

    it "routes to #edit" do
      get("/especie_introducidas/1/edit").should route_to("especie_introducidas#edit", :id => "1")
    end

    it "routes to #create" do
      post("/especie_introducidas").should route_to("especie_introducidas#create")
    end

    it "routes to #update" do
      put("/especie_introducidas/1").should route_to("especie_introducidas#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/especie_introducidas/1").should route_to("especie_introducidas#destroy", :id => "1")
    end

  end
end
