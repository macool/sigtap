require "spec_helper"

describe InfraestructuraVialsController do
  describe "routing" do

    it "routes to #index" do
      get("/infraestructura_vials").should route_to("infraestructura_vials#index")
    end

    it "routes to #new" do
      get("/infraestructura_vials/new").should route_to("infraestructura_vials#new")
    end

    it "routes to #show" do
      get("/infraestructura_vials/1").should route_to("infraestructura_vials#show", :id => "1")
    end

    it "routes to #edit" do
      get("/infraestructura_vials/1/edit").should route_to("infraestructura_vials#edit", :id => "1")
    end

    it "routes to #create" do
      post("/infraestructura_vials").should route_to("infraestructura_vials#create")
    end

    it "routes to #update" do
      put("/infraestructura_vials/1").should route_to("infraestructura_vials#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/infraestructura_vials/1").should route_to("infraestructura_vials#destroy", :id => "1")
    end

  end
end
