class AdminConstraint
  def matches?( request )
    return false unless request.session[:user_id]
    user = User.find_by_ID request.session[:user_id]
    user && user.is_admin?
  end
end