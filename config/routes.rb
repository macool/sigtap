require 'sidekiq/web'
require 'admin_constraint'

Sigtap::Application.routes.draw do

  namespace :admin do
    resources :area_protegidas, path: "areas_protegidas" do

      member do
        get :downloadable
      end

      resources :informacion_legals, path: "informacion_legal"
      resources :informacion_administrativas, path: "informacion_administrativa"
      resources :informacion_geograficas, path: "informacion_geografica"
      resources :categorizacions, path: "categorizaciones"
      resources :informacion_climaticas, path: "informacion_climatica"
      resources :informacion_turisticas, path: "informacion_turistica"
      resources :amenazas, path: "amenaza"
      resources :biodiversidads, path: "biodiversidad" do
        resources :biodiversidad_especies
      end
      resources :informacion_hidrograficas, path: "informaciones_hidrograficas"
      resources :deforestacions, path: "deforestaciones"
      resources :extraccion_ilegals, path: "extracciones_ilegales"
      resources :amenaza_especies
      resources :incendios
      resources :actividad_agropecuaria, path: "actividad_agropecuaria"
      resources :mineria_ilegals, path: "mineria_ilegal"
      resources :mineria_legals, path: "mineria_legales"
      resources :infraestructura_vials, path: "infraestructuras_viales"
      resources :otra_infraestructuras, path: "otras_infraestructuras"
      resources :especie_introducidas, path: "especies_introducidas"
      resources :comunidads, path: "comunidades"
      resources :actividad_hidrocarburiferas, path: "actividades_hidrocarburiferas"
      resources :conflicto_socioambientals, path: "conflictos_socioambientales"
      resources :invasions, path: "invasiones"
      resources :sitio_turisticos, path: "sitios_turisticos"
    end
  end

  # resources :personas
  resources :areas_protegidas do
    collection do
      get :maps
      get 'show/:id' => 'areas_protegidas#show'
    end
  end
  resources :sessions, path: :login
  get 'login' => 'sessions#index', :as => :login
  get 'logout' => 'sessions#destroy', :as => :logout

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'areas_protegidas#maps'

  mount Sidekiq::Web, at: '/sidekiq_web', :constraints => AdminConstraint.new

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
  match '*a', :to => 'application#routing_error'
end
