webpage = "http://sigtap.shiriculapo.com/"

PDFKit.configure do |config|
      config.wkhtmltopdf = '/usr/local/bin/wkhtmltopdf'
      config.default_options = {
        :print_media_type => false,
        :footer_center => 'http://sigtap.shiriculapo.com',
        :footer_font_size => 9
      }
    end