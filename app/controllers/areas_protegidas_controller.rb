# encoding: utf-8
class AreasProtegidasController < ApplicationController
  
  layout "map", :only => :maps

  def show
    respond_to do |format|
      format.js {
        @area_protegida = AreaProtegida.find params[:id]
      }
      format.json {
        unless area_protegida = AreaProtegida.find_by_slug_name(params[:id])
          render :json => nil
          return
        end
        render :json => {
            "id" => area_protegida.AREA_ID,
            "Nombre" => area_protegida.AREA_NOMBRE,
            "Tipo" => area_protegida.AREA_TIPO,
            "Región Natural" => area_protegida.region_natural,
            "Provincias" => area_protegida.provincia,
            "Cantones" => area_protegida.canton
        }
      }
    end
  end
  
  def maps
  end
  
end
