class ActividadYManejosController < ApplicationController
  # GET /actividad_y_manejos
  # GET /actividad_y_manejos.json
  def index
    @actividad_y_manejos = ActividadYManejo.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @actividad_y_manejos }
    end
  end

  # GET /actividad_y_manejos/1
  # GET /actividad_y_manejos/1.json
  def show
    @actividad_y_manejo = ActividadYManejo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @actividad_y_manejo }
    end
  end

  # GET /actividad_y_manejos/new
  # GET /actividad_y_manejos/new.json
  def new
    @actividad_y_manejo = ActividadYManejo.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @actividad_y_manejo }
    end
  end

  # GET /actividad_y_manejos/1/edit
  def edit
    @actividad_y_manejo = ActividadYManejo.find(params[:id])
  end

  # POST /actividad_y_manejos
  # POST /actividad_y_manejos.json
  def create
    @actividad_y_manejo = ActividadYManejo.new(params[:actividad_y_manejo])

    respond_to do |format|
      if @actividad_y_manejo.save
        format.html { redirect_to @actividad_y_manejo, notice: 'Actividad y manejo was successfully created.' }
        format.json { render json: @actividad_y_manejo, status: :created, location: @actividad_y_manejo }
      else
        format.html { render action: "new" }
        format.json { render json: @actividad_y_manejo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /actividad_y_manejos/1
  # PUT /actividad_y_manejos/1.json
  def update
    @actividad_y_manejo = ActividadYManejo.find(params[:id])

    respond_to do |format|
      if @actividad_y_manejo.update_attributes(params[:actividad_y_manejo])
        format.html { redirect_to @actividad_y_manejo, notice: 'Actividad y manejo was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @actividad_y_manejo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /actividad_y_manejos/1
  # DELETE /actividad_y_manejos/1.json
  def destroy
    @actividad_y_manejo = ActividadYManejo.find(params[:id])
    @actividad_y_manejo.destroy

    respond_to do |format|
      format.html { redirect_to actividad_y_manejos_url }
      format.json { head :no_content }
    end
  end
end
