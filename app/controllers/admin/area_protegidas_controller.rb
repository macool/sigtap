# encoding: utf-8
class Admin::AreaProtegidasController < AdminController

  before_filter :set_tipo_area_protegida, :only => [:new, :edit, :create, :update]

  # GET /area_protegidas
  # GET /area_protegidas.json
  def index
    @area_protegidas = AreaProtegida.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @area_protegidas }
    end
  end

  # GET /area_protegidas/1
  # GET /area_protegidas/1.json
  def show
    respond_to do |format|
      format.html {
        unless @area_protegida = AreaProtegida.find_by_AREA_ID(params[:id], :include => [:informacion_climatica, :biodiversidad, :conflictos_socioambientales, :informaciones_administrativas, :categorizaciones, :comunidades, :sitios_turisticos, :informaciones_hidrograficas, :informaciones_legales, :informaciones_turisticas, :informaciones_geograficas])
          redirect_to admin_area_protegidas_path
        end
      }
      format.json { render json: @area_protegida }
      format.js {
        @area_protegida = AreaProtegida.find(params[:id])
      }
      
    end
  end

  # GET /area_protegidas/new
  # GET /area_protegidas/new.json
  def new
    @area_protegida = AreaProtegida.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @area_protegida }
    end
  end

  # GET /area_protegidas/1/edit
  def edit
    @area_protegida = AreaProtegida.find(params[:id])
  end

  # POST /area_protegidas
  # POST /area_protegidas.json
  def create
    params[:area_protegida][:AREA_NOMBRE].strip!
    @area_protegida = AreaProtegida.new(params[:area_protegida])

    respond_to do |format|
      if @area_protegida.save
        format.html { redirect_to admin_area_protegida_path(@area_protegida), notice: 'Área protegida creada.' }
        format.json { render json: @area_protegida, status: :created, location: admin_area_protegida_path(@area_protegida) }
        format.js {
          redirect_to action: :show
        }
      else
        format.html { render action: "new" }
        format.json { render json: @area_protegida.errors, status: :unprocessable_entity }
        format.js {
          redirect_to action: :new
        }
      end
    end
  end

  # PUT /area_protegidas/1
  # PUT /area_protegidas/1.json
  def update
    params[:area_protegida][:AREA_NOMBRE].strip!
    @area_protegida = AreaProtegida.find(params[:area_protegida][:id])

    respond_to do |format|
      if @area_protegida.update_attributes(params[:area_protegida])
        format.html { redirect_to admin_area_protegida_path(@area_protegida), notice: 'Área protegida actualizada' }
        format.json { head :no_content }
        format.js {
          render :show
        }
      else
        format.html { render action: "edit" }
        format.json { render json: @area_protegida.errors, status: :unprocessable_entity }
        format.js {
          render action: :edit
        }
      end
    end
  end

  # DELETE /area_protegidas/1
  # DELETE /area_protegidas/1.json
  def destroy
    @area_protegida = AreaProtegida.find(params[:id])
    @area_protegida.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegidas_path }
      format.json { head :no_content }
    end
  end

  def downloadable
    @area_protegida = AreaProtegida.find_by_AREA_ID(params[:id], :include => [:informacion_climatica, :biodiversidad, :conflictos_socioambientales, :informaciones_administrativas, :categorizaciones, :comunidades, :sitios_turisticos, :informaciones_hidrograficas, :informaciones_legales, :informaciones_turisticas, :informaciones_geograficas])
    
    filename = "/pdfs/#{SecureRandom.hex}.pdf"
    rails_root = Rails.root.to_s
    file_path = "#{rails_root}/public#{filename}"

    PdfRemoverWorker.perform_in(10.minutes, file_path)

    html = render_to_string("downloadable")

    kit = PDFKit.new( html, :print_media_type => false )
    kit.stylesheets << "#{rails_root}/public/pdf.css"

    kit.to_file( file_path )
    # render :file => filename, :content_type => "application/pdf"
    # redirect_to filename
    send_file file_path
  end

  private
    def set_tipo_area_protegida
      @tipos_area_protegida = [
        "Sistema Nacional de Áreas Protegidas",
        "Áreas de Vegetación y Bosques Protectores"
      ]
    end
end
