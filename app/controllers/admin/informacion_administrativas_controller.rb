# encoding: utf-8
class Admin::InformacionAdministrativasController < AdminController
  before_filter :find_area_protegida
  # GET /informacion_administrativas
  # GET /informacion_administrativas.json
  def index
    @informacion_administrativas = @area_protegida.informaciones_administrativas.all
  end

  # GET /informacion_administrativas/1
  # GET /informacion_administrativas/1.json
  def show
    @informacion_administrativa = @area_protegida.informaciones_administrativas.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @informacion_administrativa }
    end
  end

  # GET /informacion_administrativas/new
  # GET /informacion_administrativas/new.json
  def new
    @informacion_administrativa = @area_protegida.informaciones_administrativas.build
    render :edit
  end

  # GET /informacion_administrativas/1/edit
  def edit
    @informacion_administrativa = @area_protegida.informaciones_administrativas.find(params[:id])
  end

  # POST /informacion_administrativas
  # POST /informacion_administrativas.json
  def create
    @informacion_administrativa = @area_protegida.informaciones_administrativas.new(params[:informacion_administrativa])

    respond_to do |format|
      if @informacion_administrativa.save
        format.html { redirect_to admin_area_protegida_informacion_administrativa_path(@area_protegida, @informacion_administrativa), notice: 'Información Administrativa creada' }
        format.json { render json: @informacion_administrativa, status: :created }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "new" }
        format.json { render json: @informacion_administrativa.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # PUT /informacion_administrativas/1
  # PUT /informacion_administrativas/1.json
  def update
    @informacion_administrativa = @area_protegida.informaciones_administrativas.find(params[:id])

    respond_to do |format|
      if @informacion_administrativa.update_attributes(params[:informacion_administrativa])
        format.html { redirect_to admin_area_protegida_informacion_administrativas_path(@area_protegida), notice: 'Información Administrativa actualizada.' }
        format.json { head :no_content }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "edit" }
        format.json { render json: @informacion_administrativa.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # DELETE /informacion_administrativas/1
  # DELETE /informacion_administrativas/1.json
  def destroy
    @informacion_administrativa = @area_protegida.informaciones_administrativas.find(params[:id])
    @informacion_administrativa.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_informacion_administrativas_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

end
