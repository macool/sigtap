# encoding: utf-8
class Admin::InvasionsController < AdminController

  before_filter :find_area_protegida, :set_div_for_js

  def index
    # @invasions = Invasion.all
    @amenazas = @area_protegida.amenazas_invasiones.all
  end

  # GET /invasions/1
  # GET /invasions/1.json
  def show
    redirect_to action: :index
    # @invasion = Invasion.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @invasion }
    # end
  end

  # GET /invasions/new
  # GET /invasions/new.json
  def new
    # @invasion = Invasion.new

    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { render json: @invasion }
    # end
    @amenaza = @area_protegida.amenazas.new(:AMENAZA_TIPO => "Invasiones")
    @amenaza.invasiones.build

    render 'admin/amenazas/new'
  end

  # GET /invasions/1/edit
  def edit
    # @invasion = Invasion.find(params[:id])
    @amenaza = @area_protegida.amenazas.find params[:id]
    render 'admin/amenazas/new'
  end

  # POST /invasions
  # POST /invasions.json
  def create
    redirect_to action: :index
    # @invasion = Invasion.new(params[:invasion])

    # respond_to do |format|
    #   if @invasion.save
    #     format.html { redirect_to @invasion, notice: 'Invasion was successfully created.' }
    #     format.json { render json: @invasion, status: :created, location: @invasion }
    #   else
    #     format.html { render action: "new" }
    #     format.json { render json: @invasion.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PUT /invasions/1
  # PUT /invasions/1.json
  def update
    redirect_to action: :index
    # @invasion = Invasion.find(params[:id])

    # respond_to do |format|
    #   if @invasion.update_attributes(params[:invasion])
    #     format.html { redirect_to @invasion, notice: 'Invasion was successfully updated.' }
    #     format.json { head :no_content }
    #   else
    #     format.html { render action: "edit" }
    #     format.json { render json: @invasion.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /invasions/1
  # DELETE /invasions/1.json
  def destroy
    @invasion = Invasion.find(params[:id])
    @invasion.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_invasions_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

  private

    def set_div_for_js
      @div_name = "#invasiones"
    end
end
