# encoding: utf-8
class Admin::OtraInfraestructurasController < AdminController
  before_filter :find_area_protegida, :set_div_for_js
  # GET /otra_infraestructuras
  # GET /otra_infraestructuras.json
  def index
    # @otra_infraestructuras = OtraInfraestructura.all
    @amenazas = @area_protegida.amenazas_otras_infraestructuras.all
  end

  # GET /otra_infraestructuras/1
  # GET /otra_infraestructuras/1.json
  def show
    redirect_to action: :index
    # @otra_infraestructura = OtraInfraestructura.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @otra_infraestructura }
    # end
  end

  # GET /otra_infraestructuras/new
  # GET /otra_infraestructuras/new.json
  def new
    # @otra_infraestructura = OtraInfraestructura.new

    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { render json: @otra_infraestructura }
    # end
    @amenaza = @area_protegida.amenazas.new(:AMENAZA_TIPO => "Obras de infraestructura")
    @amenaza.otras_infraestructuras.build

    render 'admin/amenazas/new'
  end

  # GET /otra_infraestructuras/1/edit
  def edit
    # @otra_infraestructura = OtraInfraestructura.find(params[:id])
    @amenaza = @area_protegida.amenazas.find params[:id]
    render 'admin/amenazas/new'
  end

  # POST /otra_infraestructuras
  # POST /otra_infraestructuras.json
  def create
    redirect_to action: :index
    # @otra_infraestructura = OtraInfraestructura.new(params[:otra_infraestructura])

    # respond_to do |format|
    #   if @otra_infraestructura.save
    #     format.html { redirect_to @otra_infraestructura, notice: 'Otra infraestructura was successfully created.' }
    #     format.json { render json: @otra_infraestructura, status: :created, location: @otra_infraestructura }
    #   else
    #     format.html { render action: "new" }
    #     format.json { render json: @otra_infraestructura.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PUT /otra_infraestructuras/1
  # PUT /otra_infraestructuras/1.json
  def update
    redirect_to action: :index
    # @otra_infraestructura = OtraInfraestructura.find(params[:id])

    # respond_to do |format|
    #   if @otra_infraestructura.update_attributes(params[:otra_infraestructura])
    #     format.html { redirect_to @otra_infraestructura, notice: 'Otra infraestructura was successfully updated.' }
    #     format.json { head :no_content }
    #   else
    #     format.html { render action: "edit" }
    #     format.json { render json: @otra_infraestructura.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /otra_infraestructuras/1
  # DELETE /otra_infraestructuras/1.json
  def destroy
    @otra_infraestructura = OtraInfraestructura.find(params[:id])
    @otra_infraestructura.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_otra_infraestructuras_path(@area_protegida)   }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

  private

    def set_div_for_js
      @div_name = "#obras_de_infraestructura"
    end
end
