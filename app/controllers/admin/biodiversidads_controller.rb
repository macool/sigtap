# encoding: utf-8
class Admin::BiodiversidadsController < AdminController

  before_filter :find_area_protegida

  # GET /biodiversidads
  # GET /biodiversidads.json
  def index
    @biodiversidad = @area_protegida.biodiversidad
  end

  # GET /biodiversidads/1
  # GET /biodiversidads/1.json
  def show
    redirect_to action: :index
    # @biodiversidad = @area_protegida.biodiversidad

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @biodiversidad }
    # end
  end

  # GET /biodiversidads/new
  # GET /biodiversidads/new.json
  def new
    @biodiversidad = @area_protegida.build_biodiversidad
    render :edit
  end

  # GET /biodiversidads/1/edit
  def edit
    @biodiversidad = @area_protegida.biodiversidad
  end

  # POST /biodiversidads
  # POST /biodiversidads.json
  def create
    @biodiversidad = @area_protegida.build_biodiversidad(params[:biodiversidad])

    respond_to do |format|
      if @biodiversidad.save
        format.html { redirect_to admin_area_protegida_biodiversidads_path(@area_protegida), notice: 'Biodiversidad creada.' }
        format.json { render json: @biodiversidad, status: :created, location: @biodiversidad }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "new" }
        format.json { render json: @biodiversidad.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # PUT /biodiversidads/1
  # PUT /biodiversidads/1.json
  def update
    @biodiversidad = @area_protegida.biodiversidad

    respond_to do |format|
      if @biodiversidad.update_attributes(params[:biodiversidad])
        format.html { redirect_to admin_area_protegida_biodiversidads_path(@area_protegida), notice: 'Biodiversidad actualizada.' }
        format.json { head :no_content }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "edit" }
        format.json { render json: @biodiversidad.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # DELETE /biodiversidads/1
  # DELETE /biodiversidads/1.json
  def destroy
    @biodiversidad = @area_protegida.biodiversidad
    @biodiversidad.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_biodiversidads_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        @biodiversidad = nil
        render :index
      }
    end
  end

end
