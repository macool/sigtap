# encoding: utf-8
class Admin::InformacionGeograficasController < AdminController
  before_filter :find_area_protegida
  # GET /informacion_geograficas
  # GET /informacion_geograficas.json
  def index
    @informacion_geograficas = @area_protegida.informaciones_geograficas.all
  end

  # GET /informacion_geograficas/1
  # GET /informacion_geograficas/1.json
  def show
    redirect_to action: :index
    # @informacion_geografica = @area_protegida.informaciones_geograficas.find params[:id]

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @informacion_geografica }
    # end
  end

  # GET /informacion_geograficas/new
  # GET /informacion_geograficas/new.json
  def new
    @informacion_geografica = @area_protegida.informaciones_geograficas.build
  end

  # GET /informacion_geograficas/1/edit
  def edit
    @informacion_geografica = @area_protegida.informaciones_geograficas.find(params[:id])
  end

  # POST /informacion_geograficas
  # POST /informacion_geograficas.json
  def create
    @informacion_geografica = @area_protegida.informaciones_geograficas.new(params[:informacion_geografica])

    respond_to do |format|
      if @informacion_geografica.save
        format.html { redirect_to admin_area_protegida_informacion_geografica_path(@area_protegida, @informacion_geografica), notice: 'Información Geográfica creada' }
        format.json { render json: @informacion_geografica, status: :created }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "new" }
        format.json { render json: @informacion_geografica.errors, status: :unprocessable_entity }
        format.js {
          render :new
        }
      end
    end
  end

  # PUT /informacion_geograficas/1
  # PUT /informacion_geograficas/1.json
  def update
    @informacion_geografica = @area_protegida.informaciones_geograficas.find(params[:id])

    respond_to do |format|
      if @informacion_geografica.update_attributes(params[:informacion_geografica])
        format.html { redirect_to admin_area_protegida_informacion_geografica_path(@area_protegida, @informacion_geografica), notice: 'Informacion geografica actualizada' }
        format.json { head :no_content }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "edit" }
        format.json { render json: @informacion_geografica.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # DELETE /informacion_geograficas/1
  # DELETE /informacion_geograficas/1.json
  def destroy
    @informacion_geografica = @area_protegida.informaciones_geograficas.find(params[:id])
    @informacion_geografica.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_informacion_geograficas_url(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

end
