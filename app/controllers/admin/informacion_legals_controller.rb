# encoding: utf-8
class Admin::InformacionLegalsController < AdminController
  before_filter :find_area_protegida
  # GET /informacion_legals
  # GET /informacion_legals.json
  def index
    @informacion_legals = @area_protegida.informaciones_legales.all
  end

  # GET /informacion_legals/1
  # GET /informacion_legals/1.json
  def show
    redirect_to action: :index
    # @informacion_legal = @area_protegida.informaciones_legales.find params[:id]

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @informacion_legal }
    # end
  end

  # GET /informacion_legals/new
  # GET /informacion_legals/new.json
  def new
    @informacion_legal = @area_protegida.informaciones_legales.build

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @informacion_legal }
      format.js
    end
  end

  # GET /informacion_legals/1/edit
  def edit
    @informacion_legal = @area_protegida.informaciones_legales.find(params[:id])
  end

  # POST /informacion_legals
  # POST /informacion_legals.json
  def create
    @informacion_legal = @area_protegida.informaciones_legales.new(params[:informacion_legal])

    respond_to do |format|
      if @informacion_legal.save
        format.html { redirect_to admin_area_protegida_informacion_legals_path(@area_protegida), notice: 'Información Legal creada.' }
        format.json { render json: @informacion_legal, status: :created }
        format.js {
          redirect_to action: :index
        }
      else
        format.html { render action: "new" }
        format.json { render json: @informacion_legal.errors, status: :unprocessable_entity }
        format.js { render 'new' }
      end
    end
  end

  # PUT /informacion_legals/1
  # PUT /informacion_legals/1.json
  def update
    @informacion_legal = @area_protegida.informaciones_legales.find(params[:id])

    respond_to do |format|
      if @informacion_legal.update_attributes(params[:informacion_legal])
        format.html { redirect_to admin_area_protegida_informacion_legals_path(@area_protegida), notice: 'Información Legal actualizada.' }
        format.json { head :no_content }
        format.js {
          redirect_to action: :index
        }
      else
        format.html { render action: "edit" }
        format.json { render json: @informacion_legal.errors, status: :unprocessable_entity }
        format.js {
          render action: :edit
        }
      end
    end
  end

  # DELETE /informacion_legals/1
  # DELETE /informacion_legals/1.json
  def destroy
    @informacion_legal = @area_protegida.informaciones_legales.find(params[:id])
    @informacion_legal.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_informacion_legals_url(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

end
