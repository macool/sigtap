# encoding: utf-8
class Admin::ConflictoSocioambientalsController < AdminController
  
  before_filter :find_area_protegida

  def index
    @conflicto_socioambientals = @area_protegida.conflictos_socioambientales.all
  end

  # GET /conflicto_socioambientals/1
  # GET /conflicto_socioambientals/1.json
  def show
    @conflicto_socioambiental = @area_protegida.conflictos_socioambientales.find(params[:id])
  end

  # GET /conflicto_socioambientals/new
  # GET /conflicto_socioambientals/new.json
  def new
    @conflicto_socioambiental = @area_protegida.conflictos_socioambientales.new
    render :edit
  end

  # GET /conflicto_socioambientals/1/edit
  def edit
    @conflicto_socioambiental = @area_protegida.conflictos_socioambientales.find(params[:id])
  end

  # POST /conflicto_socioambientals
  # POST /conflicto_socioambientals.json
  def create
    @conflicto_socioambiental = @area_protegida.conflictos_socioambientales.new(params[:conflicto_socioambiental])

    respond_to do |format|
      if @conflicto_socioambiental.save
        format.html { redirect_to admin_area_protegida_conflicto_socioambientals_path(@area_protegida), notice: 'Conflicto Socioambiental creado.' }
        format.json { render json: @conflicto_socioambiental, status: :created, location: @conflicto_socioambiental }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "new" }
        format.json { render json: @conflicto_socioambiental.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # PUT /conflicto_socioambientals/1
  # PUT /conflicto_socioambientals/1.json
  def update
    @conflicto_socioambiental = @area_protegida.conflictos_socioambientales.find(params[:id])

    respond_to do |format|
      if @conflicto_socioambiental.update_attributes(params[:conflicto_socioambiental])
        format.html { redirect_to admin_area_protegida_conflicto_socioambientals_path(@area_protegida), notice: 'Conflicto Socioambiental actualizado.' }
        format.json { head :no_content }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "edit" }
        format.json { render json: @conflicto_socioambiental.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # DELETE /conflicto_socioambientals/1
  # DELETE /conflicto_socioambientals/1.json
  def destroy
    @conflicto_socioambiental = @area_protegida.conflictos_socioambientales.find(params[:id])
    @conflicto_socioambiental.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_conflicto_socioambientals_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end
end
