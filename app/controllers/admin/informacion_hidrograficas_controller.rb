# encoding: utf-8
class Admin::InformacionHidrograficasController < AdminController
  before_filter :find_area_protegida
  # GET /informacion_turisticas
  # GET /informacion_turisticas.json
  def index
    @informacion_hidrograficas = @area_protegida.informaciones_hidrograficas.all
  end

  # GET /informacion_hidrograficas/1
  # GET /informacion_hidrograficas/1.json
  def show
    redirect_to action: :index
    # @informacion_hidrografica = @area_protegida.informaciones_hidrograficas.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @informacion_hidrografica }
    # end
  end

  # GET /informacion_hidrograficas/new
  # GET /informacion_hidrograficas/new.json
  def new
    @informacion_hidrografica = @area_protegida.informaciones_hidrograficas.build
    render :edit
  end

  # GET /informacion_hidrograficas/1/edit
  def edit
    @informacion_hidrografica = @area_protegida.informaciones_hidrograficas.find(params[:id])
  end

  # POST /informacion_hidrograficas
  # POST /informacion_hidrograficas.json
  def create
    @informacion_hidrografica = @area_protegida.informaciones_hidrograficas.new(params[:informacion_hidrografica])

    respond_to do |format|
      if @informacion_hidrografica.save
        format.html { redirect_to admin_area_protegida_informacion_hidrografica_path(@area_protegida, @informacion_hidrografica), notice: 'Información hidrográfica creada'} 
        format.json { render json: @informacion_hidrografica, status: :created, location: @informacion_hidrografica }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "new" }
        format.json { render json: @informacion_hidrografica.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # PUT /informacion_hidrograficas/1
  # PUT /informacion_hidrograficas/1.json
  def update
    @informacion_hidrografica = @area_protegida.informaciones_hidrograficas.find(params[:id])

    respond_to do |format|
      if @informacion_hidrografica.update_attributes(params[:informacion_hidrografica])
        format.html { redirect_to admin_area_protegida_informacion_hidrograficas_path(@area_protegida), notice: 'Informacion hidrográfica actualizada.' }
        format.json { head :no_content }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "edit" }
        format.json { render json: @informacion_hidrografica.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # DELETE /informacion_hidrograficas/1
  # DELETE /informacion_hidrograficas/1.json
  def destroy
    @informacion_hidrografica = @area_protegida.informaciones_hidrograficas.find(params[:id])
    @informacion_hidrografica.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_informacion_hidrograficas_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end
end
