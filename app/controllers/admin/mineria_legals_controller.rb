# encoding: utf-8
class Admin::MineriaLegalsController < AdminController
  before_filter :find_area_protegida, :set_div_for_js
  # GET /mineria_legals
  # GET /mineria_legals.json
  def index
    # @mineria_legals = MineriaLegal.all
    @amenazas = @area_protegida.amenazas_minerias_legales.all
  end

  # GET /mineria_legals/1
  # GET /mineria_legals/1.json
  def show
    redirect_to action: :index
    # @mineria_legal = MineriaLegal.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @mineria_legal }
    # end
  end

  # GET /mineria_legals/new
  # GET /mineria_legals/new.json
  def new
    # @mineria_legal = MineriaLegal.new

    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { render json: @mineria_legal }
    # end
    @amenaza = @area_protegida.amenazas.new(:AMENAZA_TIPO => "Minería Legal en el límite del Área Protegida")
    @amenaza.minerias_legales.build

    render 'admin/amenazas/new'
  end

  # GET /mineria_legals/1/edit
  def edit
    # @mineria_legal = MineriaLegal.find(params[:id])
    @amenaza = @area_protegida.amenazas.find params[:id]
    render 'admin/amenazas/new'
  end

  # POST /mineria_legals
  # POST /mineria_legals.json
  def create
    redirect_to action: :index
    # @mineria_legal = MineriaLegal.new(params[:mineria_legal])

    # respond_to do |format|
    #   if @mineria_legal.save
    #     format.html { redirect_to @mineria_legal, notice: 'Mineria legal was successfully created.' }
    #     format.json { render json: @mineria_legal, status: :created, location: @mineria_legal }
    #   else
    #     format.html { render action: "new" }
    #     format.json { render json: @mineria_legal.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PUT /mineria_legals/1
  # PUT /mineria_legals/1.json
  def update
    redirect_to action: :index
    # @mineria_legal = MineriaLegal.find(params[:id])

    # respond_to do |format|
    #   if @mineria_legal.update_attributes(params[:mineria_legal])
    #     format.html { redirect_to @mineria_legal, notice: 'Mineria legal was successfully updated.' }
    #     format.json { head :no_content }
    #   else
    #     format.html { render action: "edit" }
    #     format.json { render json: @mineria_legal.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /mineria_legals/1
  # DELETE /mineria_legals/1.json
  def destroy
    @mineria_legal = MineriaLegal.find(params[:id])
    @mineria_legal.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_mineria_legals_path(@area_protegida)  }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

  private

    def set_div_for_js
      @div_name = "#minería_legal_en_el_límite_del_Área_protegida"
    end
end
