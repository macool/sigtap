# encoding: utf-8
class Admin::InfraestructuraVialsController < AdminController
  before_filter :find_area_protegida, :set_div_for_js
  # GET /infraestructura_vials
  # GET /infraestructura_vials.json
  def index
    # @infraestructura_vials = InfraestructuraVial.all
    @amenazas = @area_protegida.amenazas_infraestructuras_viales.all
  end

  # GET /infraestructura_vials/1
  # GET /infraestructura_vials/1.json
  def show
    redirect_to action: :index
    # @infraestructura_vial = InfraestructuraVial.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @infraestructura_vial }
    # end
  end

  # GET /infraestructura_vials/new
  # GET /infraestructura_vials/new.json
  def new
    # @infraestructura_vial = InfraestructuraVial.new

    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { render json: @infraestructura_vial }
    # end
    @amenaza = @area_protegida.amenazas.new(:AMENAZA_TIPO => "Infraestructura Vial")
    @amenaza.infraestructuras_viales.build

    render 'admin/amenazas/new'
  end

  # GET /infraestructura_vials/1/edit
  def edit
    # @infraestructura_vial = InfraestructuraVial.find(params[:id])
    @amenaza = @area_protegida.amenazas.find params[:id]
    render 'admin/amenazas/new'
  end

  # POST /infraestructura_vials
  # POST /infraestructura_vials.json
  def create
    redirect_to action: :index
    # @infraestructura_vial = InfraestructuraVial.new(params[:infraestructura_vial])

    # respond_to do |format|
    #   if @infraestructura_vial.save
    #     format.html { redirect_to @infraestructura_vial, notice: 'Infraestructura vial was successfully created.' }
    #     format.json { render json: @infraestructura_vial, status: :created, location: @infraestructura_vial }
    #   else
    #     format.html { render action: "new" }
    #     format.json { render json: @infraestructura_vial.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PUT /infraestructura_vials/1
  # PUT /infraestructura_vials/1.json
  def update
    redirect_to action: :index
    # @infraestructura_vial = InfraestructuraVial.find(params[:id])

    # respond_to do |format|
    #   if @infraestructura_vial.update_attributes(params[:infraestructura_vial])
    #     format.html { redirect_to @infraestructura_vial, notice: 'Infraestructura vial was successfully updated.' }
    #     format.json { head :no_content }
    #   else
    #     format.html { render action: "edit" }
    #     format.json { render json: @infraestructura_vial.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /infraestructura_vials/1
  # DELETE /infraestructura_vials/1.json
  def destroy
    @infraestructura_vial = InfraestructuraVial.find(params[:id])
    @infraestructura_vial.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_infraestructura_vials_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

  private

    def set_div_for_js
      @div_name = "#infraestructura_vial"
    end
end
