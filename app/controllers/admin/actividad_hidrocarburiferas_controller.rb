# encoding: utf-8
class Admin::ActividadHidrocarburiferasController < AdminController
  before_filter :find_area_protegida, :set_div_for_js
  # GET /actividad_hidrocarburiferas
  # GET /actividad_hidrocarburiferas.json
  def index
    # @actividad_hidrocarburiferas = ActividadHidrocarburifera.all
    @amenazas = @area_protegida.amenazas_actividades_hidrocarburiferas.all
  end

  # GET /actividad_hidrocarburiferas/1
  # GET /actividad_hidrocarburiferas/1.json
  def show
    redirect_to action: :index
    # @actividad_hidrocarburifera = ActividadHidrocarburifera.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @actividad_hidrocarburifera }
    # end
  end

  # GET /actividad_hidrocarburiferas/new
  # GET /actividad_hidrocarburiferas/new.json
  def new
    # @actividad_hidrocarburifera = ActividadHidrocarburifera.new

    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { render json: @actividad_hidrocarburifera }
    # end
    @amenaza = @area_protegida.amenazas.new(:AMENAZA_TIPO => "Actividades Hidrocarburíferas")
    @amenaza.actividades_hidrocarburiferas.build

    render 'admin/amenazas/new'
  end

  # GET /actividad_hidrocarburiferas/1/edit
  def edit
    # @actividad_hidrocarburifera = ActividadHidrocarburifera.find(params[:id])
    @amenaza = @area_protegida.amenazas.find params[:id]
    render 'admin/amenazas/new'
  end

  # POST /actividad_hidrocarburiferas
  # POST /actividad_hidrocarburiferas.json
  def create
    redirect_to action: :index
    # @actividad_hidrocarburifera = ActividadHidrocarburifera.new(params[:actividad_hidrocarburifera])

    # respond_to do |format|
    #   if @actividad_hidrocarburifera.save
    #     format.html { redirect_to @actividad_hidrocarburifera, notice: 'Actividad hidrocarburifera was successfully created.' }
    #     format.json { render json: @actividad_hidrocarburifera, status: :created, location: @actividad_hidrocarburifera }
    #   else
    #     format.html { render action: "new" }
    #     format.json { render json: @actividad_hidrocarburifera.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PUT /actividad_hidrocarburiferas/1
  # PUT /actividad_hidrocarburiferas/1.json
  def update
    redirect_to action: :index
    # @actividad_hidrocarburifera = ActividadHidrocarburifera.find(params[:id])

    # respond_to do |format|
    #   if @actividad_hidrocarburifera.update_attributes(params[:actividad_hidrocarburifera])
    #     format.html { redirect_to @actividad_hidrocarburifera, notice: 'Actividad hidrocarburifera was successfully updated.' }
    #     format.json { head :no_content }
    #   else
    #     format.html { render action: "edit" }
    #     format.json { render json: @actividad_hidrocarburifera.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /actividad_hidrocarburiferas/1
  # DELETE /actividad_hidrocarburiferas/1.json
  def destroy
    @actividad_hidrocarburifera = ActividadHidrocarburifera.find(params[:id])
    @actividad_hidrocarburifera.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_actividad_hidrocarburiferas_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

  private

    def set_div_for_js
      @div_name = "#actividades_hidrocarburíferas"
    end
end
