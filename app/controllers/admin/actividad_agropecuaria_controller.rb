# encoding: utf-8
class Admin::ActividadAgropecuariaController < AdminController

  before_filter :find_area_protegida, :set_div_for_js

  def index
    @amenazas = @area_protegida.amenazas_actividades_agropecuarias.all
  end

  def show
    redirect_to action: :index
    # @actividad_agropecuarium = ActividadAgropecuarium.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @actividad_agropecuarium }
    # end
  end

  # GET /actividad_agropecuaria/new
  # GET /actividad_agropecuaria/new.json
  def new
    @amenaza = @area_protegida.amenazas.new(:AMENAZA_TIPO => "Actividad Agropecuaria")
    @amenaza.actividades_agropecuarias.build
    # @amenaza.actividades_agropecuarias.first.actividades_y_manejos.build

    render 'admin/amenazas/new'
  end

  # GET /actividad_agropecuaria/1/edit
  def edit
    @amenaza = @area_protegida.amenazas.find(params[:id])
    render 'admin/amenazas/new'
  end

  # POST /actividad_agropecuaria
  # POST /actividad_agropecuaria.json
  def create
    redirect_to action: :index
    # @actividad_agropecuarium = ActividadAgropecuarium.new(params[:actividad_agropecuarium])

    # respond_to do |format|
    #   if @actividad_agropecuarium.save
    #     format.html { redirect_to @actividad_agropecuarium, notice: 'Actividad agropecuarium was successfully created.' }
    #     format.json { render json: @actividad_agropecuarium, status: :created, location: @actividad_agropecuarium }
    #   else
    #     format.html { render action: "new" }
    #     format.json { render json: @actividad_agropecuarium.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PUT /actividad_agropecuaria/1
  # PUT /actividad_agropecuaria/1.json
  def update
    redirect_to action: :index
    # @actividad_agropecuarium = ActividadAgropecuarium.find(params[:id])

    # respond_to do |format|
    #   if @actividad_agropecuarium.update_attributes(params[:actividad_agropecuarium])
    #     format.html { redirect_to @actividad_agropecuarium, notice: 'Actividad agropecuarium was successfully updated.' }
    #     format.json { head :no_content }
    #   else
    #     format.html { render action: "edit" }
    #     format.json { render json: @actividad_agropecuarium.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /actividad_agropecuaria/1
  # DELETE /actividad_agropecuaria/1.json
  def destroy
    @actividad_agropecuarium = ActividadAgropecuarium.find(params[:id])
    @actividad_agropecuarium.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_actividad_agropecuaria_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

  private

    def set_div_for_js
      @div_name = "#actividad_agropecuaria"
    end
end
