# encoding: utf-8
class Admin::BiodiversidadEspeciesController < AdminController
  # GET /biodiversidad_especies
  # GET /biodiversidad_especies.json

  before_filter :find_area_protegida, :find_biodiversidad
  before_filter :set_tipo_especie, :only => [:new, :edit, :update, :create]

  def index
    @biodiversidad_especies = @biodiversidad.biodiversidad_especies.order("BIO_ESP_TIPO, BIO_ESP_NOMBRE")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @biodiversidad_especies }
    end
  end

  # GET /biodiversidad_especies/1
  # GET /biodiversidad_especies/1.json
  def show
    redirect_to action: :index
    # @biodiversidad_especy = @biodiversidad.biodiversidad_especies.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @biodiversidad_especy }
    # end
  end

  # GET /biodiversidad_especies/new
  # GET /biodiversidad_especies/new.json
  def new
    @biodiversidad_especy = @biodiversidad.biodiversidad_especies.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @biodiversidad_especy }
    end
  end

  # GET /biodiversidad_especies/1/edit
  def edit
    @biodiversidad_especy = @biodiversidad.biodiversidad_especies.find(params[:id])
  end

  # POST /biodiversidad_especies
  # POST /biodiversidad_especies.json
  def create
    @biodiversidad_especy = @biodiversidad.biodiversidad_especies.new(params[:biodiversidad_especie])

    respond_to do |format|
      if @biodiversidad_especy.save
        format.html { redirect_to admin_area_protegida_biodiversidad_biodiversidad_especies_path(@area_protegida, @biodiversidad), notice: 'Especie de biodiversidad creada.' }
        format.json { render json: @biodiversidad_especy, status: :created, location: @biodiversidad_especy }
      else
        format.html { render action: "new" }
        format.json { render json: @biodiversidad_especy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /biodiversidad_especies/1
  # PUT /biodiversidad_especies/1.json
  def update
    @biodiversidad_especy = @biodiversidad.biodiversidad_especies.find(params[:id])

    respond_to do |format|
      if @biodiversidad_especy.update_attributes(params[:biodiversidad_especie])
        format.html { redirect_to admin_area_protegida_biodiversidad_biodiversidad_especies_path(@area_protegida, @biodiversidad), notice: 'Especie de biodiversidad actualizada.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @biodiversidad_especy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /biodiversidad_especies/1
  # DELETE /biodiversidad_especies/1.json
  def destroy
    @biodiversidad_especy = @biodiversidad.biodiversidad_especies.find(params[:id])
    @biodiversidad_especy.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_biodiversidad_biodiversidad_especies_path(@area_protegida, @biodiversidad) }
      format.json { head :no_content }
    end
  end

  private

    def find_biodiversidad
      @biodiversidad = Biodiversidad.find params[:biodiversidad_id]
    end

    def set_tipo_especie
      @tipos_especie = [
        "Fauna",
        "Flora"
      ]
    end

end
