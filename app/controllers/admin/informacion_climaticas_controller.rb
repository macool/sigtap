# encoding: utf-8
class Admin::InformacionClimaticasController < AdminController
  before_filter :find_area_protegida
  # GET /informacion_climaticas
  # GET /informacion_climaticas.json
  def index
    @informacion_climatica = @area_protegida.informacion_climatica
  end

  # GET /informacion_climaticas/1
  # GET /informacion_climaticas/1.json
  def show
    redirect_to action: :index
    # @informacion_climatica = @area_protegida.informacion_climatica
    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @informacion_climatica }
    # end
  end

  # GET /informacion_climaticas/new
  # GET /informacion_climaticas/new.json
  def new
    @informacion_climatica = @area_protegida.build_informacion_climatica
    render :edit
  end

  # GET /informacion_climaticas/1/edit
  def edit
    @informacion_climatica = @area_protegida.informacion_climatica
  end

  # POST /informacion_climaticas
  # POST /informacion_climaticas.json
  def create
    @informacion_climatica = @area_protegida.build_informacion_climatica(params[:informacion_climatica])

    respond_to do |format|
      if @informacion_climatica.save
        format.html { redirect_to admin_area_protegida_informacion_climatica_path(@area_protegida, @informacion_climatica), notice: 'Información climatica creada' }
        format.json { render json: @informacion_climatica, status: :created, location: @informacion_climatica }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "new" }
        format.json { render json: @informacion_climatica.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # PUT /informacion_climaticas/1
  # PUT /informacion_climaticas/1.json
  def update
    @informacion_climatica = @area_protegida.informacion_climatica

    respond_to do |format|
      if @informacion_climatica.update_attributes(params[:informacion_climatica])
        format.html { redirect_to admin_area_protegida_informacion_climatica_path(@area_protegida), notice: 'Informacion climatica actualizada' }
        format.json { head :no_content }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "edit" }
        format.json { render json: @informacion_climatica.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # DELETE /informacion_climaticas/1
  # DELETE /informacion_climaticas/1.json
  def destroy
    @informacion_climatica = @area_protegida.informacion_climatica
    @informacion_climatica.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_informacion_climatica_path }
      format.json { head :no_content }
      format.js {
        @informacion_climatica = nil
        render :index
      }
    end
  end
  
end
