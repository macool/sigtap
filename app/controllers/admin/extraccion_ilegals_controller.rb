# encoding: utf-8
class Admin::ExtraccionIlegalsController < AdminController
  before_filter :find_area_protegida, :set_div_for_js
  # GET /extraccion_ilegals
  # GET /extraccion_ilegals.json
  
  def index
    @amenazas = @area_protegida.amenazas_extracciones_ilegales.all
  end

  # GET /extraccion_ilegals/1
  # GET /extraccion_ilegals/1.json
  def show
    redirect_to action: :index
    # @extraccion_ilegal = ExtraccionIlegal.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @extraccion_ilegal }
    # end
  end

  # GET /extraccion_ilegals/new
  # GET /extraccion_ilegals/new.json
  def new
    # @amenaza = @area_protegida.amenazas.build
    # @extraccion_ilegal = @amenaza.extracciones_ilegales.build

    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { render json: @extraccion_ilegal }
    # end
    @amenaza = @area_protegida.amenazas.new(:AMENAZA_TIPO => "Extracción Ilegal")
    @amenaza.extracciones_ilegales.build

    render 'admin/amenazas/new'
  end

  # GET /extraccion_ilegals/1/edit
  def edit
    # @extraccion_ilegal = ExtraccionIlegal.find(params[:id])
    @amenaza = @area_protegida.amenazas.find params[:id]
    render 'admin/amenazas/new'
  end

  # POST /extraccion_ilegals
  # POST /extraccion_ilegals.json
  def create
    redirect_to action: :index
    # amenaza_attrs = params[:extraccion_ilegal].delete :amenaza
    # @amenaza = @area_protegida.amenazas.create(amenaza_attrs)    
    # @extraccion_ilegal = @amenaza.extracciones_ilegales.build(params[:extraccion_ilegal])



    # respond_to do |format|
    #   if @extraccion_ilegal.save
    #     format.html { redirect_to admin_area_protegida_amenaza_path(@area_protegida, @amenaza), notice: 'Amenaza creada'}
    #     format.json { render json: @extraccion_ilegal, status: :created, location: @extraccion_ilegal }
    #   else
    #     format.html { render action: "new" }
    #     format.json { render json: @extraccion_ilegal.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PUT /extraccion_ilegals/1
  # PUT /extraccion_ilegals/1.json
  def update
    redirect_to action: :index
    # @extraccion_ilegal = ExtraccionIlegal.find(params[:id])

    # respond_to do |format|
    #   if @extraccion_ilegal.update_attributes(params[:extraccion_ilegal])
    #     format.html { redirect_to @extraccion_ilegal, notice: 'Extraccion ilegal was successfully updated.' }
    #     format.json { head :no_content }
    #   else
    #     format.html { render action: "edit" }
    #     format.json { render json: @extraccion_ilegal.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /extraccion_ilegals/1
  # DELETE /extraccion_ilegals/1.json
  def destroy
    @extraccion_ilegal = ExtraccionIlegal.find(params[:id])
    @extraccion_ilegal.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_extraccion_ilegals_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

  private

    def set_div_for_js
      @div_name = "#extracción_ilegal"
    end
end
