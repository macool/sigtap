# encoding: utf-8
class Admin::MineriaIlegalsController < AdminController

  before_filter :find_area_protegida, :set_div_for_js
  
  def index
    @amenazas = @area_protegida.amenazas_minerias_ilegales.all
  end

  # GET /mineria_ilegals/1
  # GET /mineria_ilegals/1.json
  # def show
  #   redirect_to action: :index
  #   # @mineria_ilegal = MineriaIlegal.find(params[:id])

  #   # respond_to do |format|
  #   #   format.html # show.html.erb
  #   #   format.json { render json: @mineria_ilegal }
  #   # end
  # end

  # GET /mineria_ilegals/new
  # GET /mineria_ilegals/new.json
  def new
    @amenaza = @area_protegida.amenazas.new(:AMENAZA_TIPO => "Minería Ilegal")
    @amenaza.minerias_ilegales.build

    render 'admin/amenazas/new'
  end

  # GET /mineria_ilegals/1/edit
  def edit
    @amenaza = @area_protegida.amenazas.find params[:id]
    render 'admin/amenazas/new'
  end

  # POST /mineria_ilegals
  # POST /mineria_ilegals.json
  # def create
  #   redirect_to action: :index
  #   # @mineria_ilegal = MineriaIlegal.new(params[:mineria_ilegal])

  #   # respond_to do |format|
  #   #   if @mineria_ilegal.save
  #   #     format.html { redirect_to @mineria_ilegal, notice: 'Mineria ilegal was successfully created.' }
  #   #     format.json { render json: @mineria_ilegal, status: :created, location: @mineria_ilegal }
  #   #   else
  #   #     format.html { render action: "new" }
  #   #     format.json { render json: @mineria_ilegal.errors, status: :unprocessable_entity }
  #   #   end
  #   # end
  # end

  # PUT /mineria_ilegals/1
  # PUT /mineria_ilegals/1.json
  # def update
  #   @mineria_ilegal = MineriaIlegal.find(params[:id])

  #   respond_to do |format|
  #     if @mineria_ilegal.update_attributes(params[:mineria_ilegal])
  #       format.html { redirect_to @mineria_ilegal, notice: 'Mineria ilegal was successfully updated.' }
  #       format.json { head :no_content }
  #     else
  #       format.html { render action: "edit" }
  #       format.json { render json: @mineria_ilegal.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # DELETE /mineria_ilegals/1
  # DELETE /mineria_ilegals/1.json
  def destroy
    @mineria_ilegal = MineriaIlegal.find(params[:id])
    @mineria_ilegal.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_mineria_ilegals_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

  private

    def set_div_for_js
      @div_name = "#minería_ilegal"
    end
end
