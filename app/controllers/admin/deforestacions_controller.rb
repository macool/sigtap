# encoding: utf-8
class Admin::DeforestacionsController < AdminController

  before_filter :find_area_protegida, :set_div_for_js

  # GET /deforestacions
  # GET /deforestacions.json
  def index
    @amenazas = @area_protegida.amenazas_deforestaciones.all
  end

  # GET /deforestacions/1
  # GET /deforestacions/1.json
  def show
    redirect_to action: :index
    # @deforestacion = Deforestacion.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @deforestacion }
    # end
  end

  # GET /deforestacions/new
  # GET /deforestacions/new.json
  def new
    
    @amenaza = @area_protegida.amenazas.new(:AMENAZA_TIPO => "Deforestación")
    @amenaza.deforestaciones.build

    render 'admin/amenazas/new'
  end

  # GET /deforestacions/1/edit
  def edit
    @amenaza = @area_protegida.amenazas.find params[:id]
    render 'admin/amenazas/new'
  end

  # POST /deforestacions
  # POST /deforestacions.json
  def create
    redirect_to action: :index
    # @deforestacion = Deforestacion.new(params[:deforestacion])

    # respond_to do |format|
    #   if @deforestacion.save
    #     format.html { redirect_to @deforestacion, notice: 'Deforestacion was successfully created.' }
    #     format.json { render json: @deforestacion, status: :created, location: @deforestacion }
    #   else
    #     format.html { render action: "new" }
    #     format.json { render json: @deforestacion.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PUT /deforestacions/1
  # PUT /deforestacions/1.json
  def update
    redirect_to action: :index
    # @deforestacion = Deforestacion.find(params[:id])

    # respond_to do |format|
    #   if @deforestacion.update_attributes(params[:deforestacion])
    #     format.html { redirect_to @deforestacion, notice: 'Deforestacion was successfully updated.' }
    #     format.json { head :no_content }
    #   else
    #     format.html { render action: "edit" }
    #     format.json { render json: @deforestacion.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /deforestacions/1
  # DELETE /deforestacions/1.json
  def destroy
    @deforestacion = Deforestacion.find(params[:id])
    @deforestacion.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_deforestacions_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

  private

    def set_div_for_js
      @div_name = "#deforestación"
    end
end
