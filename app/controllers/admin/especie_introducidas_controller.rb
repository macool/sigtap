# encoding: utf-8
class Admin::EspecieIntroducidasController < AdminController
  before_filter :find_area_protegida, :set_div_for_js
  # GET /especie_introducidas
  # GET /especie_introducidas.json
  def index
    # @especie_introducidas = EspecieIntroducida.all
    @amenazas = @area_protegida.amenazas_especies_introducidas.all
  end

  # GET /especie_introducidas/1
  # GET /especie_introducidas/1.json
  def show
    redirect_to action: :index
    # @especie_introducida = EspecieIntroducida.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @especie_introducida }
    # end
  end

  # GET /especie_introducidas/new
  # GET /especie_introducidas/new.json
  def new
    # @especie_introducida = EspecieIntroducida.new

    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { render json: @especie_introducida }
    # end
    @amenaza = @area_protegida.amenazas.new(:AMENAZA_TIPO => "Especies introducidas")
    @amenaza.especies_introducidas.build

    render 'admin/amenazas/new'
  end

  # GET /especie_introducidas/1/edit
  def edit
    # @especie_introducida = EspecieIntroducida.find(params[:id])
    @amenaza = @area_protegida.amenazas.find params[:id]
    render 'admin/amenazas/new'
  end

  # POST /especie_introducidas
  # POST /especie_introducidas.json
  def create
    redirect_to action: :index
    # @especie_introducida = EspecieIntroducida.new(params[:especie_introducida])

    # respond_to do |format|
    #   if @especie_introducida.save
    #     format.html { redirect_to @especie_introducida, notice: 'Especie introducida was successfully created.' }
    #     format.json { render json: @especie_introducida, status: :created, location: @especie_introducida }
    #   else
    #     format.html { render action: "new" }
    #     format.json { render json: @especie_introducida.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PUT /especie_introducidas/1
  # PUT /especie_introducidas/1.json
  def update
    redirect_to action: :index
    # @especie_introducida = EspecieIntroducida.find(params[:id])

    # respond_to do |format|
    #   if @especie_introducida.update_attributes(params[:especie_introducida])
    #     format.html { redirect_to @especie_introducida, notice: 'Especie introducida was successfully updated.' }
    #     format.json { head :no_content }
    #   else
    #     format.html { render action: "edit" }
    #     format.json { render json: @especie_introducida.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /especie_introducidas/1
  # DELETE /especie_introducidas/1.json
  def destroy
    @especie_introducida = EspecieIntroducida.find(params[:id])
    @especie_introducida.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_especie_introducidas_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end
  
  private

    def set_div_for_js
      @div_name = "#especies_introducidas"
    end
end
