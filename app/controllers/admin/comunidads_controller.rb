# encoding: utf-8
class Admin::ComunidadsController < AdminController
  
  before_filter :find_area_protegida

  def index
    @comunidades = @area_protegida.comunidades.all
  end

  # GET /comunidads/1
  # GET /comunidads/1.json
  def show
    @comunidad = @area_protegida.comunidades.find(params[:id])
  end

  # GET /comunidads/new
  # GET /comunidads/new.json
  def new
    @comunidad = @area_protegida.comunidades.new
    render :edit
  end

  # GET /comunidads/1/edit
  def edit
    @comunidad = @area_protegida.comunidades.find(params[:id])
  end

  # POST /comunidads
  # POST /comunidads.json
  def create
    @comunidad = Comunidad.new(params[:comunidad])

    respond_to do |format|
      if @comunidad.save
        format.html { 
          redirect_to admin_area_protegida_comunidads_path(@area_protegida), notice: 'Comunidad creada.'
        }
        format.json { render json: @comunidad, status: :created, location: @comunidad }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "new" }
        format.json { render json: @comunidad.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # PUT /comunidads/1
  # PUT /comunidads/1.json
  def update
    @comunidad = Comunidad.find(params[:id])

    respond_to do |format|
      if @comunidad.update_attributes(params[:comunidad])
        format.html { redirect_to admin_area_protegida_comunidads_path(@area_protegida), notice: 'Comunidad actualizada.' }
        format.json { head :no_content }
        format.js {
        index
        render :index
      }
      else
        format.html { render action: "edit" }
        format.json { render json: @comunidad.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # DELETE /comunidads/1
  # DELETE /comunidads/1.json
  def destroy
    @comunidad = Comunidad.find(params[:id])
    @comunidad.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_comunidads_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end
end
