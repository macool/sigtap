# encoding: utf-8
class Admin::InformacionTuristicasController < AdminController
  before_filter :find_area_protegida
  # GET /informacion_turisticas
  # GET /informacion_turisticas.json
  def index
    @informacion_turisticas = @area_protegida.informaciones_turisticas.all
  end

  # GET /informacion_turisticas/1
  # GET /informacion_turisticas/1.json
  def show
    redirect_to action: :index
    # @informacion_turistica = @area_protegida.informaciones_turisticas.find params[:id]

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @informacion_turistica }
    # end
  end

  # GET /informacion_turisticas/new
  # GET /informacion_turisticas/new.json
  def new
    @informacion_turistica = @area_protegida.informaciones_turisticas.build
    render :edit
  end

  # GET /informacion_turisticas/1/edit
  def edit
    @informacion_turistica = @area_protegida.informaciones_turisticas.find(params[:id])
  end

  # POST /informacion_turisticas
  # POST /informacion_turisticas.json
  def create
    @informacion_turistica = @area_protegida.informaciones_turisticas.new(params[:informacion_turistica])

    respond_to do |format|
      if @informacion_turistica.save
        format.html { redirect_to admin_area_protegida_informacion_turistica_path(@area_protegida, @informacion_turistica), notice: 'Información turística creada'} 
        format.json { render json: @informacion_turistica, status: :created, location: @informacion_turistica }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "new" }
        format.json { render json: @informacion_turistica.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # PUT /informacion_turisticas/1
  # PUT /informacion_turisticas/1.json
  def update
    @informacion_turistica = @area_protegida.informaciones_turisticas.find(params[:id])

    respond_to do |format|
      if @informacion_turistica.update_attributes(params[:informacion_turistica])
        format.html { redirect_to admin_area_protegida_informacion_turisticas_path(@area_protegida), notice: 'Información turística actualizada' }
        format.json { head :no_content }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "edit" }
        format.json { render json: @informacion_turistica.errors, status: :unprocessable_entity }
        format.js {
          render :edit
        }
      end
    end
  end

  # DELETE /informacion_turisticas/1
  # DELETE /informacion_turisticas/1.json
  def destroy
    @informacion_turistica = @area_protegida.informaciones_turisticas.find(params[:id])
    @informacion_turistica.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_informacion_turisticas_url(@area_protegida)  }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

end
