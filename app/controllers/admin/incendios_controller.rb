# encoding: utf-8
class Admin::IncendiosController < AdminController
  before_filter :find_area_protegida, :set_div_for_js
  # GET /incendios
  # GET /incendios.json
  def index
    # @incendios = Incendio.all
    @amenazas = @area_protegida.amenazas_incendios.all
  end

  # GET /incendios/1
  # GET /incendios/1.json
  def show
    redirect_to action: :index
    # @incendio = Incendio.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @incendio }
    # end
  end

  # GET /incendios/new
  # GET /incendios/new.json
  def new
    # @incendio = Incendio.new

    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { render json: @incendio }
    # end
    @amenaza = @area_protegida.amenazas.new(:AMENAZA_TIPO => "Incendio")
    @amenaza.incendios.build

    render 'admin/amenazas/new'
  end

  # GET /incendios/1/edit
  def edit
    # @incendio = Incendio.find(params[:id])
    @amenaza = @area_protegida.amenazas.find params[:id]
    render 'admin/amenazas/new'
  end

  # POST /incendios
  # POST /incendios.json
  def create
    redirect_to action: :index
    # @incendio = Incendio.new(params[:incendio])

    # respond_to do |format|
    #   if @incendio.save
    #     format.html { redirect_to @incendio, notice: 'Incendio was successfully created.' }
    #     format.json { render json: @incendio, status: :created, location: @incendio }
    #   else
    #     format.html { render action: "new" }
    #     format.json { render json: @incendio.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PUT /incendios/1
  # PUT /incendios/1.json
  def update
    redirect_to action: :index
    # @incendio = Incendio.find(params[:id])

    # respond_to do |format|
    #   if @incendio.update_attributes(params[:incendio])
    #     format.html { redirect_to @incendio, notice: 'Incendio was successfully updated.' }
    #     format.json { head :no_content }
    #   else
    #     format.html { render action: "edit" }
    #     format.json { render json: @incendio.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # DELETE /incendios/1
  # DELETE /incendios/1.json
  def destroy
    @incendio = Incendio.find(params[:id])
    @incendio.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_incendios_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

  private

    def set_div_for_js
      @div_name = "#incendios"
    end
end
