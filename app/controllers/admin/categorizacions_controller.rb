# encoding: utf-8
class Admin::CategorizacionsController < AdminController

  before_filter :find_area_protegida
  before_filter :set_tipo_categorizacion, :only => [:new, :edit, :create, :update]

  # GET /categorizacions
  # GET /categorizacions.json
  def index
    @categorizacions = @area_protegida.categorizaciones.all
  end

  # GET /categorizacions/1
  # GET /categorizacions/1.json
  def show
    redirect_to action: :index
    # @categorizacion = @area_protegida.categorizaciones.find(params[:id])

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @categorizacion }
    # end
  end

  # GET /categorizacions/new
  # GET /categorizacions/new.json
  def new
    @categorizacion = @area_protegida.categorizaciones.new
  end

  # GET /categorizacions/1/edit
  def edit
    @categorizacion = @area_protegida.categorizaciones.find(params[:id])
    render :new
  end

  # POST /categorizacions
  # POST /categorizacions.json
  def create
    @categorizacion = @area_protegida.categorizaciones.new(params[:categorizacion])

    respond_to do |format|
      if @categorizacion.save
        format.html { redirect_to admin_area_protegida_categorizacions_path(@area_protegida), notice: 'Categorización creada.' }
        format.json { render json: @categorizacion, status: :created, location: @categorizacion }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "new" }
        format.json { render json: @categorizacion.errors, status: :unprocessable_entity }
        format.js {
          render :new
        }
      end
    end
  end

  # PUT /categorizacions/1
  # PUT /categorizacions/1.json
  def update
    # raise "#{params[:categorizacion][:detalles]}"

    @categorizacion = @area_protegida.categorizaciones.find(params[:id])

    respond_to do |format|
      if @categorizacion.update_attributes(params[:categorizacion])
        format.html { redirect_to admin_area_protegida_categorizacions_path(@area_protegida), notice: 'Categorización actualizada.' }
        format.json { head :no_content }
        format.js {
          index
          render :index
        }
      else
        format.html { render action: "edit" }
        format.json { render json: @categorizacion.errors, status: :unprocessable_entity }
        format.js {
          render :new
        }
      end
    end
  end

  # DELETE /categorizacions/1
  # DELETE /categorizacions/1.json
  def destroy
    @categorizacion = @area_protegida.categorizaciones.find(params[:id])
    @categorizacion.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_categorizacions_path(@area_protegida) }
      format.json { head :no_content }
      format.js {
        index
        render :index
      }
    end
  end

  private

    def set_tipo_categorizacion
      @tipos_categorizacion = [
        "Nacional",
        "Internacional"
      ]
    end

end
