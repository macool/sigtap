# encoding: utf-8
class Admin::AmenazasController < AdminController

  before_filter :find_area_protegida, :confirm_logged_in
  
  # GET /amenazas
  # GET /amenazas.json
  def index
    redirect_to admin_area_protegida_path(@area_protegida)
    # @amenazas = @area_protegida.amenazas.all

    # respond_to do |format|
    #   format.html # index.html.erb
    #   format.json { render json: @amenazas }
    # end
  end

  # GET /amenazas/1
  # GET /amenazas/1.json
  def show
    redirect_to admin_area_protegida_path(@area_protegida)
    # @amenaza = @area_protegida.amenazas.find params[:id]


    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @amenaza }
    # end
  end

  # GET /amenazas/new
  # GET /amenazas/new.json
  def new
    redirect_to admin_area_protegida_path(@area_protegida)
    # @amenaza = @area_protegida.amenazas.build


    # respond_to do |format|
    #   format.html # new.html.erb
    #   format.json { render json: @amenaza }
    # end
  end

  # GET /amenazas/1/edit
  def edit
    redirect_to admin_area_protegida_path(@area_protegida)
    # @amenaza = @area_protegida.amenazas.find(params[:id])
  end

  # POST /amenazas
  # POST /amenazas.json
  def create
    # redirect_to admin_area_protegida_path(@area_protegida)
    @amenaza = @area_protegida.amenazas.new(params[:amenaza])

    respond_to do |format|
      if @amenaza.save
        format.js { 
          case @amenaza.AMENAZA_TIPO
          when "Deforestación"
            redirect_to admin_area_protegida_deforestacions_path(@area_protegida)
          when "Extracción Ilegal"
            redirect_to admin_area_protegida_extraccion_ilegals_path(@area_protegida)
          when "Incendio"
            redirect_to admin_area_protegida_incendios_path(@area_protegida)
          when "Actividad Agropecuaria"
            redirect_to admin_area_protegida_actividad_agropecuaria_path(@area_protegida)
          when "Minería Ilegal"
            redirect_to admin_area_protegida_mineria_ilegals_path(@area_protegida)
            flash["notice"] = "Minería Ilegal creada."
          when "Minería Legal en el límite del Área Protegida"
            redirect_to admin_area_protegida_mineria_legals_path(@area_protegida)
          when "Infraestructura Vial"
            redirect_to admin_area_protegida_infraestructura_vials_path(@area_protegida)
            flash["notice"] = "Infraestructura vial creada."
          when "Obras de infraestructura"
            redirect_to admin_area_protegida_otra_infraestructuras_path(@area_protegida)
            flash["notice"] = "Otra infraestructura creada."
          when "Especies introducidas"
            redirect_to admin_area_protegida_especie_introducidas_path(@area_protegida)
            flash["notice"] = "Especie introducida creada."
          when "Actividades Hidrocarburíferas"
            redirect_to admin_area_protegida_actividad_hidrocarburiferas_path(@area_protegida)
            flash["notice"] = "Actividad hidrocarburífera creada."
          when "Invasiones"
            redirect_to admin_area_protegida_invasions_path(@area_protegida)
            flash["notice"] = "Invasión creada."
          end
        }
        format.json { render json: @amenaza, status: :created, location: @amenaza }
      else
        # format.html { render action: "new" }
        #format.json { render json: @amenaza.errors, status: :unprocessable_entity }
        
        format.js {
          case @amenaza.AMENAZA_TIPO
          when "Minería Legal en el límite del Área Protegida"
            @div_name = "#minería_legal_en_el_límite_del_Área_protegida"
            render :new
          when "Deforestación"
            @div_name = "#deforestación"
            render :new
          when "Extracción Ilegal"
            @div_name = "#extracción_ilegal"
            render :new
          when "Incendio"
            @div_name = "#incendios"
            render :new
          when "Actividad Agropecuaria"
            @div_name = "#actividad_agropecuaria"
            render :new
          when "Minería Ilegal"
            @div_name = "#minería_ilegal"
            render :new
          when "Infraestructura Vial"
            @div_name = "#infraestructura_vial"
            render :new
          when "Obras de infraestructura"
            @div_name = "#obras_de_infraestructura"
            render :new
          when "Especies introducidas"
            @div_name = "#especies_introducidas"
            render :new
          when "Actividades Hidrocarburíferas"
            @div_name = "#actividades_hidrocarburíferas"
            render :new
          when "Invasiones"
            @div_name = "#invasiones"
            render :new
          end
        }
        
      end
    end
  end

  # PUT /amenazas/1
  # PUT /amenazas/1.json
  def update
    # redirect_to admin_area_protegida_path(@area_protegida)
    @amenaza = @area_protegida.amenazas.find(params[:id])


    respond_to do |format|
      if @amenaza.update_attributes(params[:amenaza])
        format.js {
          case @amenaza.AMENAZA_TIPO
          when "Deforestación"
            redirect_to admin_area_protegida_deforestacions_path(@area_protegida)
          when "Extracción Ilegal"
            redirect_to admin_area_protegida_extraccion_ilegals_path(@area_protegida)
            flash["notice"] = 'Extracción ilegal actualizada.'
          when "Incendio"
            redirect_to admin_area_protegida_incendios_path(@area_protegida)
            flash["notice"] = 'Incendio actualizada.'
          when "Actividad Agropecuaria"
            redirect_to admin_area_protegida_actividad_agropecuaria_path(@area_protegida)
            flash["notice"] = "Actividad Agropecuaria actualizada."
          when "Minería Ilegal"
            redirect_to admin_area_protegida_mineria_ilegals_path(@area_protegida)
            flash["notice"] = "Minería Ilegal actualizada."
          when "Infraestructura Vial"
            redirect_to admin_area_protegida_infraestructura_vials_path(@area_protegida)
            flash["notice"] = "Infraestructura vial actualizada."
          when "Obras de infraestructura"
            redirect_to admin_area_protegida_otra_infraestructuras_path(@area_protegida)
            flash["notice"] = "Otra infraestructura actualizada."
          when "Minería Legal en el límite del Área Protegida"
            redirect_to admin_area_protegida_mineria_legals_path(@area_protegida)
            flash["notice"] = "Minería legal actualizada."
          when "Especies introducidas"
            redirect_to admin_area_protegida_especie_introducidas_path(@area_protegida)
            flash["notice"] = "Especie introducida actualizada."
          when "Actividades Hidrocarburíferas"
            redirect_to admin_area_protegida_actividad_hidrocarburiferas_path(@area_protegida)
            flash["notice"] = "Actividad hidrocarburífera actualizada."
          when "Invasiones"
            redirect_to admin_area_protegida_invasions_path(@area_protegida)
            flash["notice"] = "Invasión actualizada."
          end
        }
        format.json { head :no_content }
      else
        
        format.js {
          case @amenaza.AMENAZA_TIPO
          when "Minería Legal en el límite del Área Protegida"
            @div_name = "#minería_legal_en_el_límite_del_Área_protegida"
            render :new
          when "Deforestación"
            @div_name = "#deforestación"
            render :new
          when "Extracción Ilegal"
            @div_name = "#extracción_ilegal"
            render :new
          when "Incendio"
            @div_name = "#incendios"
            render :new
          when "Actividad Agropecuaria"
            @div_name = "#actividad_agropecuaria"
            render :new
          when "Minería Ilegal"
            @div_name = "#minería_ilegal"
            render :new
          when "Infraestructura Vial"
            @div_name = "#infraestructura_vial"
            render :new
          when "Obras de infraestructura"
            @div_name = "#obras_de_infraestructura"
            render :new
          when "Especies introducidas"
            @div_name = "#especies_introducidas"
            render :new
          when "Actividades Hidrocarburíferas"
            @div_name = "#actividades_hidrocarburíferas"
            render :new
          when "Invasiones"
            @div_name = "#invasiones"
            render :new
          end
        }
      end
    end
  end

  # DELETE /amenazas/1
  # DELETE /amenazas/1.json
  def destroy
    # redirect_to admin_area_protegida_path(@area_protegida)
    @amenaza = @area_protegida.amenazas.find(params[:id])
    @amenaza.destroy



    respond_to do |format|
      format.html { redirect_to admin_area_protegida_amenazas_url(@area_protegida) }
      format.json { head :no_content }
      format.js {
        case @amenaza.AMENAZA_TIPO
          when "Deforestación"
            redirect_to controller: :deforestacions, action: :index, status: 303
          when "Extracción Ilegal"
            redirect_to controller: :extraccion_ilegals, action: :index, status: 303
          when "Incendio"
            redirect_to controller: :incendios, action: :index, status: 303
          when "Actividad Agropecuaria"
            redirect_to controller: :actividad_agropecuaria, action: :index, status: 303
          when "Minería Ilegal"
            redirect_to controller: :mineria_ilegals, action: :index, status: 303
          when "Minería Legal en el límite del Área Protegida"
            redirect_to controller: :mineria_legals, action: :index, status: 303
          when "Infraestructura Vial"
            redirect_to controller: :infraestructura_vials, action: :index, status: 303
          when "Obras de infraestructura"
            redirect_to controller: :otra_infraestructuras, action: :index, status: 303
          when "Especies introducidas"
            redirect_to controller: :especie_introducidas, action: :index, status: 303
          when "Actividades Hidrocarburíferas"
            redirect_to controller: :actividad_hidrocarburiferas, action: :index, status: 303
          when "Invasiones"
            redirect_to controller: :invasions, action: :index, status: 303
          end
      }
    end
  end
end
