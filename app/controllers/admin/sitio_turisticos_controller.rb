# encoding: utf-8
class Admin::SitioTuristicosController < AdminController
  
  before_filter :find_area_protegida

  def index
    @sitio_turisticos = @area_protegida.sitios_turisticos.all
  end

  # GET /sitio_turisticos/1
  # GET /sitio_turisticos/1.json
  def show
    redirect_to action: :index
  end

  # GET /sitio_turisticos/new
  # GET /sitio_turisticos/new.json
  def new
    @sitio_turistico = @area_protegida.sitios_turisticos.new
    render :edit
  end

  # GET /sitio_turisticos/1/edit
  def edit
    @sitio_turistico = @area_protegida.sitios_turisticos.find(params[:id])
  end

  # POST /sitio_turisticos
  # POST /sitio_turisticos.json
  def create
    @sitio_turistico = @area_protegida.sitios_turisticos.new(params[:sitio_turistico])

    respond_to do |format|
      if @sitio_turistico.save
        format.html { 
          redirect_to admin_area_protegida_path(@area_protegida), notice: 'Sitio Turístico creado.'
        }
        format.json { render json: @sitio_turistico, status: :created, location: @sitio_turistico }
        format.js { index; render :index }
      else
        format.html { render action: "new" }
        format.json { render json: @sitio_turistico.errors, status: :unprocessable_entity }
        format.js { render :edit }
      end
    end
  end

  # PUT /sitio_turisticos/1
  # PUT /sitio_turisticos/1.json
  def update
    @sitio_turistico = SitioTuristico.find(params[:id])

    respond_to do |format|
      if @sitio_turistico.update_attributes(params[:sitio_turistico])
        format.html { 
          redirect_to admin_area_protegida_path(@area_protegida), notice: 'Sitio Turístico actualizado.'
        }
        format.json { head :no_content }
        format.js { index; render :index }
      else
        format.html { render action: "edit" }
        format.json { render json: @sitio_turistico.errors, status: :unprocessable_entity }
        format.js { render :edit }
      end
    end
  end

  # DELETE /sitio_turisticos/1
  # DELETE /sitio_turisticos/1.json
  def destroy
    @sitio_turistico = @area_protegida.sitios_turisticos.find(params[:id])
    @sitio_turistico.destroy

    respond_to do |format|
      format.html { redirect_to admin_area_protegida_sitio_turisticos_path(@area_protegida) }
      format.json { head :no_content }
      format.js { index; render :index }
    end
  end
end
