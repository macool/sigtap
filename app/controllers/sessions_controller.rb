# encoding: utf-8
class SessionsController < ApplicationController
  
  def index
    if current_user
      flash["alert-info"] = "Ya iniciaste sesión como #{current_user.nicename}"
      redirect_to root_path
      return
    end
  end
  
  def create
    if (user = User.find_by_user_login(params[:username])) and user.check_password(params[:password])
      session[:user_id] = user.id
      redirect_to root_path
      flash["alert-success"] = "Acabas de iniciar sesión como #{current_user.nicename}"
    else
      flash["alert-error"] = "Hubo un problema. Comprueba que escribiste bien tu nombre de usuario y contraseña"
      redirect_to login_path
    end
  end
  
  def destroy
    flash["alert-info"] = "Cerraste tu sesión"
    session[:user_id] = nil
    redirect_to root_path
  end
  
end