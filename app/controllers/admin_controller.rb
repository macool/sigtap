class AdminController < ApplicationController
  
  before_filter :confirm_logged_in_admin, :except => [:show, :downloadable]

  protected

    def find_area_protegida
      unless @area_protegida = AreaProtegida.find_by_AREA_ID(params[:area_protegida_id])
        redirect_to admin_area_protegidas_path
      end
    end

end