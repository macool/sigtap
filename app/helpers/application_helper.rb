# encoding: utf-8
module ApplicationHelper

  def make_tabs( hash, make_body = true, make_container = false )
    if hash.class == Array
      make_body = false
    end
    html_head = "<ul class='nav nav-tabs#{make_container ? " tabs_are_container" : ""}'>"
    html_body = make_body ? "<div class='tab-content'>" : ""
    hash.each_with_index do |(title, partial_name), i|
      html_head += content_tag(:li, :class => (i==0 ? "active" : "")) do
        if title == "Minería Legal en el límite del Área Protegida"
          link_to "Minería Legal", "##{title.split.join('_').downcase}", "data-toggle" => "tab"
        else
          link_to title, "##{title.split.join('_').downcase}", "data-toggle" => "tab"
        end
      end
      if make_body
        html_body += content_tag(:div, :class => "tab-pane#{i==0 ? ' active' : ''}#{make_container ? " tab_with_container" : ""}", :id => title.split.join("_").downcase) do
          render partial_name
        end
      end
    end
    html_head += "</ul>"
    html_body += make_body ? "</div>" : ""
    raw(html_head + html_body)
  end

  def js_helpers
    output = "window.Helpers.ButtonsHelper.init();"
    output += "window.Helpers.FormsHelper.init();"
    output += "window.Helpers.RemoteLinksHelper.finishedLoading();"
    output += "window.Helpers.InfoTitleHelper.init();"
    output += "window.Helpers.DatepickerHelper.init();"
    raw output
  end

  def js_content( selector, rendered_content )
    output = "$('#{selector}').html('#{rendered_content}');"
    output += js_helpers
    raw output
  end

  def form_helper_not_blank
    raw '<span class="help-inline">No puede quedar en blanco.</span>'
  end

  def form_helper_gt_0
    raw '<span class="help-inline">Debe ser mayor a cero.</span>'
  end

  def app_url
    return request.protocol + request.host_with_port
  end

end
