# encoding: utf-8
module InformacionHidrograficasHelper

  def humedales_form_table( selected=[], opciones=humedales_form_table_opciones )
    cb_class = "informacion_hidrografica_form_humedales_checkbox"
    html = ""
    opciones.each do |key, value|
      html += "<tr>"
      html += "<td><b>#{key}:</b></td>"
      html += "<td>"
      value.each do |v|
        html += "<div>"
        html += "<input type='checkbox' class='#{cb_class}'"
        if selected.include? v
          html += "checked='checked'"
        end
        html += "> "
        html += "<span>"
        html += "#{v}"
        html += "</span>"
        html += "</div>"
      end
      html += "</td>"
      html += "</tr>"
    end
    raw html
  end

  def humedales_form_table_opciones
    {
      "Marinos" => [
        "Costeros",
        "Costas Rocosas",
        "Arrecifes de Coral"
      ],
      "Estuarios" => [
        "Deltas",
        "Marismas de Marea",
        "Manglares"
      ],
      "Lacustres" => [
        "Lagos",
        "Lagunas"
      ],
      "Ribereños" => [
        "Ríos",
        "Arroyos"
      ],
      "Palustres" => [
        "Marismas",
        "Pantanos",
        "Ciénegas",
        "Turberas"
      ]
    }
  end

end
