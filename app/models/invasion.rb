class Invasion < ActiveRecord::Base
  attr_accessible :INV_EXTENSION,
                  :INV_NUM_PERSONAS,
                  :INV_TIPO,
                  :AMENAZA_ID

  self.table_name = "INVASIONES"
  self.primary_key = :INV_ID

  belongs_to :amenaza, :foreign_key => :AMENAZA_ID

  def AREA_ID
    self.area_protegida.AREA_ID
  end

  # validates :INV_EXTENSION, :presence => true
  # validates :INV_NUM_PERSONAS, :presence => true
  # validates :INV_TIPO, :presence => true
  validates :INV_EXTENSION, :numericality => {:greater_than => 0}, :allow_blank => true
  validates :INV_NUM_PERSONAS, :numericality => {:greater_than => 0}, :allow_blank => true



end
