# encoding: utf-8
class AreaProtegida < ActiveRecord::Base
  attr_accessible :AREA_FECHA_LEVANTAMIENTO,
                  :AREA_NOMBRE,
                  :AREA_OBJETIVOS,
                  :AREA_TIPO,
                  :BIO_ID,
                  :CAT_ID,
                  :CLIMA_ID,
                  :GEO_ID,
                  :responsable_registro,
                  :imagen_ap,
                  :remove_imagen_ap,
                  :imagen_ap_cache

  self.primary_key = :AREA_ID
  self.table_name = "AREAS_PROTEGIDAS"

  mount_uploader :imagen_ap, AreaProtegidaImagenUploader

  # relationships
    has_one :informacion_climatica, :foreign_key => :AREA_ID,
                                    :dependent => :destroy
    has_one :biodiversidad, :foreign_key => :AREA_ID,
                            :dependent => :destroy

    has_many :personas, :foreign_key => :AREA_ID,
                        :dependent => :destroy
    has_many :amenazas, :foreign_key => :AREA_ID,
                        :dependent => :destroy
    has_many :conflictos_socioambientales, :class_name => "ConflictoSocioambiental",
                                           :foreign_key => :AREA_ID,
                                           :dependent => :destroy
    has_many :informaciones_administrativas, :class_name => "InformacionAdministrativa",
                                             :foreign_key => :AREA_ID,
                                             :dependent => :destroy
    has_many :categorizaciones, :class_name => "Categorizacion",
                                :foreign_key => :AREA_ID,
                                :dependent => :destroy
    has_many :comunidades, :class_name => "Comunidad",
                           :foreign_key => :AREA_ID,
                           :dependent => :destroy
    has_many :sitios_turisticos, :class_name => "SitioTuristico",
                                 :foreign_key => :AREA_ID,
                                 :dependent => :destroy
    has_many :informaciones_hidrograficas, :class_name => "InformacionHidrografica",
                                           :foreign_key => :AREA_ID,
                                           :dependent => :destroy
    has_many :informaciones_legales, :class_name => "InformacionLegal",
                                     :foreign_key => :AREA_ID,
                                     :dependent => :destroy
    has_many :informaciones_turisticas, :class_name => "InformacionTuristica",
                                        :foreign_key => :AREA_ID,
                                        :dependent => :destroy
    has_many :informaciones_geograficas, :class_name => "InformacionGeografica",
                                        :foreign_key => :AREA_ID,
                                        :dependent => :destroy
  
  # instance methods
    def fecha_levantamiento_less_than_now
      errors.add(:AREA_FECHA_LEVANTAMIENTO, "No puede ser mayor a la fecha actual") if self.AREA_FECHA_LEVANTAMIENTO and self.AREA_FECHA_LEVANTAMIENTO > Time.now.to_date
    end
    def set_slug_name
      self.slug_name = self.AREA_NOMBRE.downcase.split(" ").join("_")  
    end

    def amenazas_deforestaciones
      self.amenazas.where(:AMENAZA_TIPO => "Deforestación").includes(:deforestaciones)
    end

    def amenazas_extracciones_ilegales
      self.amenazas.where(:AMENAZA_TIPO => "Extracción Ilegal").includes(:extracciones_ilegales)
    end

    def amenazas_incendios
      self.amenazas.where(:AMENAZA_TIPO => "Incendio").includes(:incendios)
    end
    
    def amenazas_actividades_agropecuarias
      self.amenazas.where(:AMENAZA_TIPO => "Actividad Agropecuaria").includes(:actividades_agropecuarias)
    end
    
    def amenazas_minerias_ilegales
      self.amenazas.where(:AMENAZA_TIPO => "Minería Ilegal").includes(:minerias_ilegales)
    end

    def amenazas_minerias_legales
      self.amenazas.where(:AMENAZA_TIPO => "Minería Legal en el límite del Área Protegida").includes(:minerias_legales)
    end

    def amenazas_infraestructuras_viales
      self.amenazas.where(:AMENAZA_TIPO => "Infraestructura Vial").includes(:infraestructuras_viales)
    end

    def amenazas_otras_infraestructuras
      self.amenazas.where(:AMENAZA_TIPO => "Obras de infraestructura").includes(:otras_infraestructuras)
    end

    def amenazas_especies_introducidas
      self.amenazas.where(:AMENAZA_TIPO => "Especies introducidas").includes(:especies_introducidas)
    end
    
    def amenazas_actividades_hidrocarburiferas
      self.amenazas.where(:AMENAZA_TIPO => "Actividades Hidrocarburíferas").includes(:actividades_hidrocarburiferas)
    end

    def amenazas_invasiones
      self.amenazas.where(:AMENAZA_TIPO => "Invasiones").includes(:invasiones)
    end

    # Imagen de SitioTuristico:
      def one_sitio_turistico
        @one_sitio_turistico ||= self.sitios_turisticos.where("imagen <> ''").first
      end
      def has_imagen?
        imagen_ap? or (one_sitio_turistico.imagen? if one_sitio_turistico)
      end
      def one_imagen_url
        imagen_ap_url or (one_sitio_turistico.imagen_url if one_sitio_turistico)
      end
      def one_imagen
        imagen_ap.blank? ? (one_sitio_turistico.imagen if one_sitio_turistico) : imagen_ap
      end

    # Información Geográfica:
      def informacion_geografica
        @informacion_geografica ||= (self.informaciones_geograficas.first if self.informaciones_geograficas.count > 0)
      end
      def provincia
        informacion_geografica.GEO_PROVINCIAS if informacion_geografica
      end
      def canton
        informacion_geografica.GEO_CANTONES if informacion_geografica
      end
      def parroquia
        informacion_geografica.GEO_PARROQUIAS if informacion_geografica
      end
      def region_natural
        informacion_geografica.GEO_REGION if informacion_geografica
      end
    #categorizaciones
    def has_all_categorizaciones?
      self.categorizaciones.where(:CAT_NAC_TIPO => "Nacional").exists? and self.categorizaciones.where(:CAT_TIPO => "Internacional").exists? 
    end

  # validations
    validates :AREA_NOMBRE, :presence => true, :uniqueness => true
    validates :AREA_TIPO, :presence => true
    validates :slug_name, :presence => true, :uniqueness => true
    validates :responsable_registro, :presence => true
    validate :fecha_levantamiento_less_than_now

  # callbacks:
    before_validation :set_slug_name

end
