class ConflictoSocioambiental < ActiveRecord::Base
  attr_accessible :AREA_ID,
                  :CON_ESTADO,
                  :CON_FECHA_DESDE,
                  :CON_FECHA_HASTA,
                  :CON_FECHA_LEVANTAMIENTO,
                  :CON_INVOLUCRADO,
                  :CON_TIPO,
                  :causas,
                  :referencia,
                  :responsable_registro,
                  :otra_consecuencia,
                  :conf_soc_pers_invol_morador,
                  :conf_soc_pers_invol_morador_tipo,
                  :conf_soc_pers_invol_a_local,
                  :conf_soc_pers_invol_a_local_tipo,
                  :conf_soc_pers_invol_a_supralocal,
                  :conf_soc_pers_invol_a_supralocal_tipo,
                  :conf_soc_pers_afec_morador,
                  :conf_soc_pers_afec_morador_tipo,
                  :conf_soc_pers_afec_a_local,
                  :conf_soc_pers_afec_a_local_tipo,
                  :conf_soc_pers_afec_a_supralocal,
                  :conf_soc_pers_afec_a_supralocal_tipo

  self.table_name = "CONFLICTOS_SOCIOAMBIENTALES"
  self.primary_key = "CON_ID"

  belongs_to :area_protegida, :foreign_key => :AREA_ID


  # validations:
    validates :referencia, :presence => true
    validates :CON_FECHA_HASTA, :presence => true
    validates :CON_FECHA_DESDE, :presence => true
    validates :CON_ESTADO, :presence => true
    validates :responsable_registro, :presence => true
    validate :otra_consecuencia_if_otro, :presencia_persona
    validate :fecha_levantamiento_less_than_now
  
  # methods:
    def otra_consecuencia_if_otro
      errors.add(:otra_consecuencia, "No puede quedar en blanco") if self.otra_consecuencia.blank? and self.CON_TIPO == "Otros"
    end

    def fecha_levantamiento_less_than_now
      errors.add(:CON_FECHA_LEVANTAMIENTO, "No puede ser mayor a la fecha actual") if self.CON_FECHA_LEVANTAMIENTO and self.CON_FECHA_LEVANTAMIENTO > Time.now.to_date
    end

    def fecha_desde_less_than_now
      errors.add(:CON_FECHA_DESDE, "No puede ser mayor a la fecha actual") if self.CON_FECHA_DESDE and self.CON_FECHA_DESDE > Time.now.to_date
    end

    def fecha_hasta_less_than_now
      errors.add(:CON_FECHA_HASTA, "No puede ser mayor a la fecha actual") if self.CON_FECHA_HASTA and self.CON_FECHA_HASTA > Time.now.to_date
    end

    def presencia_persona
      %w(conf_soc_pers_invol_morador conf_soc_pers_invol_a_local conf_soc_pers_invol_a_supralocal conf_soc_pers_afec_morador conf_soc_pers_afec_a_local conf_soc_pers_afec_a_supralocal).each do |argument|
        if self.send(argument.to_sym).blank?
          self.send("#{argument}_tipo=".to_sym, nil)
        end
      end
    end

end
