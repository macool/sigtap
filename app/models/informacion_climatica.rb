class InformacionClimatica < ActiveRecord::Base
  attr_accessible :AREA_ID,
                  :CLIMA_HUME_DESDE,
                  :CLIMA_HUME_HASTA,
                  :CLIMA_PRECIP_DESDE,
                  :CLIMA_PRECIP_HASTA,
                  :CLIMA_TEMP_DESDE,
                  :CLIMA_TEMP_HASTA

  self.table_name = "INFORMACION_CLIMATICA"
  self.primary_key =  "CLIMA_ID"

  belongs_to :area_protegida, :foreign_key => :AREA_ID

  # validations:
    validates :CLIMA_PRECIP_DESDE, :numericality => {:greater_than => 0, :less_than_or_equal_to => 99999}, :allow_blank => true
    validates :CLIMA_PRECIP_HASTA, :numericality => {:greater_than => 0, :less_than_or_equal_to => 99999}, :allow_blank => true
    validates :CLIMA_HUME_DESDE, :numericality => {:greater_than => 0, :less_than_or_equal_to => 100}, :allow_blank => true
    validates :CLIMA_HUME_HASTA, :numericality => {:greater_than => 0, :less_than_or_equal_to => 100}, :allow_blank => true

end
