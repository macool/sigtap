class OtraInfraestructura < ActiveRecord::Base
  attr_accessible :AMENAZA_ID,
                  :OTR_INF_AREA_COBERTURA,
                  :OTR_INF_TIPO,
                  :tipo

  self.table_name = "OTRAS_INFRAESTRUCTURAS"
  self.primary_key = "OTR_INF_ID"

  belongs_to :amenaza, foreign_key: :AMENAZA_ID

  # validate :at_least_one_tipo
  # validates :OTR_INF_AREA_COBERTURA, :presence => true
  validates :OTR_INF_AREA_COBERTURA, :numericality => {:greater_than => 0}, :allow_blank => true

  # def OTR_INF_TIPO=( array )
  #   if array.include? ""
  #     array.delete ""
  #   end
  #   write_attribute :OTR_INF_TIPO, array.to_json
  # end
def tipo=( array )
    array.delete("") if array.include?("")
    write_attribute(:OTR_INF_TIPO, array.join(","))
  end
  def OTR_INF_TIPO
    read_attribute(:OTR_INF_TIPO).to_s.split(",")
  end

  alias_method :tipo, :OTR_INF_TIPO

  # def at_least_one_tipo
  #   errors.add(:tipo, "Se debe escoger al menos un tipo") if tipo.length == 0
  # end

end
