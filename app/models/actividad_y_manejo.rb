class ActividadYManejo < ActiveRecord::Base
  attr_accessible :AGR_ACT_MANEJO,
                  :AGR_ACT_TIPO,
                  :AGR_ID

  self.table_name = "ACTIVIDAD_Y_MANEJO"
  self.primary_key = "AGR_ACT_ID"

  belongs_to :actividad_agropecuaria, foreign_key: :AGR_ID

  # validates :AGR_ACT_TIPO, :presence => true
  # validates :AGR_ACT_MANEJO, :presence => true

end
