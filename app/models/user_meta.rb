class UserMeta < ActiveRecord::Base
  
  establish_connection "wordpress"
  
  @prefix = Rails.application.config.database_configuration["wordpress"]["table_prefix"]
  
  self.table_name = "#{@prefix}usermeta"
  
  # wordpress fields:
  
  # relationships:
    belongs_to :user
  
  # methods:
    def readonly?
      true
    end
    def before_destroy
      raise ActiveRecord::ReadOnlyRecord
    end
    
  # alias methods:
    alias_method :destroy, :before_destroy
  
end