class Incendio < ActiveRecord::Base
  attr_accessible :AMENAZA_ID,
                  :INC_AREA,
                  :INC_ECOSISTEMA,
                  :INC_ORIGEN

  self.table_name = "INCENDIO"
  self.primary_key = "INC_ID"

  belongs_to :amenaza, foreign_key: :AMENAZA_ID

  def AREA_ID
    self.area_protegida.AREA_ID
  end

  # validates :INC_AREA, :presence => true
  # validates :INC_ECOSISTEMA, :presence => true
  validates :INC_AREA, :numericality => {:greater_than => 0}, :allow_blank => true

end
