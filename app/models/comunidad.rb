# encoding: utf-8
class Comunidad < ActiveRecord::Base

  attr_accessible :AREA_ID,
                  :COM_ACTIVIDADES,
                  :COM_FECHA_LEVANTAMIENTO,
                  :COM_GRUPO_ETNICO_PREDOMINANTE,
                  :COM_HEC_SOCIOBOSQUE,
                  :COM_LIDER_COMUNITARIO,
                  :COM_LOCALIZACION,
                  :COM_NOMBRE,
                  :COM_NUM_FAMILIAS,
                  :COM_NUM_INDIVIDUOS,
                  :COM_SOCIO_BOSQUE,
                  :COM_SUPERFICIE_APROX,
                  :COM_TIPO_ORGA_SOCIAL,
                  :coordenada_norte,
                  :coordenada_este,
                  :altitud_maxima,
                  :altitud_minima,
                  :actividades,
                  :actividades_otros,
                  :pertenece_a_socio_bosque,
                  :responsable_registro

  attr_accessor :provincia, :canton, :parroquia, :actividades

  self.table_name = "COMUNIDADES"
  self.primary_key = "COM_ID"

  belongs_to :area_protegida, foreign_key: :AREA_ID

  # validations:
    validates :COM_NOMBRE, :presence => true
    validates :COM_FECHA_LEVANTAMIENTO, :presence => true
    validates :COM_GRUPO_ETNICO_PREDOMINANTE, :presence => true
    validates :COM_TIPO_ORGA_SOCIAL, :presence => true
    validates :COM_SUPERFICIE_APROX, :presence => true, :numericality => {:greater_than_or_equal_to => 1}
    validates :COM_LIDER_COMUNITARIO, :presence => true
    validates :COM_LOCALIZACION, :presence => true
    validates :COM_NUM_FAMILIAS, :numericality => {:greater_than_or_equal_to => 0, :less_than_or_equal_to => 999}
    validates :COM_NUM_INDIVIDUOS, :numericality => {:greater_than_or_equal_to => 0, :less_than_or_equal_to => 999}
    validates :COM_HEC_SOCIOBOSQUE, :numericality => {:greater_than_or_equal_to => 0}
    validates :responsable_registro, :presence => true
    validate :at_least_one_actividad
    validate :hectareas_en_socio_bosque_if_pertenece_a_socio_bosque
    validate :fecha_levantamiento_less_than_now

  def actividades=( array )
    array.delete("") if array.include?("")
    write_attribute(:COM_ACTIVIDADES, array.join(","))
  end
  # def COM_ACTIVIDADES=( array )
  #   array.delete("") if array.include?("")
  #   write_attribute(:COM_ACTIVIDADES, array.join(","))
  # end
  def COM_ACTIVIDADES
    read_attribute(:COM_ACTIVIDADES).to_s.split(",")
  end

  alias_method :actividades, :COM_ACTIVIDADES

  def at_least_one_actividad
    errors.add(:actividades, "Se debe escoger al menos una actividad") if actividades.length == 0
  end

  def has_actividades_otros?
    actividades.include?("Otros")
  end

  def fecha_levantamiento_less_than_now
    errors.add(:COM_FECHA_LEVANTAMIENTO, "No puede ser mayor a la fecha actual") if self.COM_FECHA_LEVANTAMIENTO and self.COM_FECHA_LEVANTAMIENTO > Time.now.to_date
  end

  def hectareas_en_socio_bosque_if_pertenece_a_socio_bosque
      errors.add(:COM_HEC_SOCIOBOSQUE, "No puede quedar en blanco") if self.pertenece_a_socio_bosque and self.COM_HEC_SOCIOBOSQUE.blank?

  end

end
