class Persona < ActiveRecord::Base
  attr_accessible :ADMIN_ID,
                  :AREA_ID,
                  :CON_ID,
                  :PERSONA_APELLIDOS,
                  :PERSONA_DIRECCION,
                  :PERSONA_MAIL,
                  :PERSONA_NOMBRE,
                  :PERSONA_ROL,
                  :PERSONA_TELEFONO,
                  :TUR_ID

  self.table_name = "PERSONAS"
  self.primary_key = :PERSON_ID

  belongs_to :area_protegida, :foreign_key => :AREA_ID

  has_many :informaciones_administrativas, :class_name => "InformacionAdministrativa",
                                           :foreign_key => :PERSON_ID,
                                           :dependent => :destroy

end
