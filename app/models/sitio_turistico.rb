class SitioTuristico < ActiveRecord::Base
  attr_accessible :AREA_ID,
                  :GEO_ID,
                  :TUR_ACAMPAR_NUM,
                  :TUR_ALOJAMIENTO_NUM,
                  :TUR_ATRACTIVO_PRINCIPAL,
                  :TUR_CAP_CARGA,
                  :TUR_CENTRO_INTERP_NUM,
                  :TUR_DIST_ADMIN_CERCANO,
                  :TUR_DIST_CARRETERA,
                  :TUR_DIST_OTRO,
                  :TUR_DIST_SENDERO,
                  :TUR_EPOCA_LLUVIAS,
                  :TUR_FECHA_LEVANTAMIENTO,
                  :TUR_NOMBRE,
                  :TUR_OTROS_ATRACTIVOS,
                  :TUR_OTROS_SENDEROS_NUM,
                  :TUR_SENDERO_AUTO_NUM,
                  :TUR_SITIO_INGRESO,
                  :TUR_TIPO_CLIMA,
                  :epoca_seca,
                  :coordenada_norte,
                  :coordenada_este,
                  :altitud_maxima,
                  :altitud_minima,
                  :descripcion_recorrido,
                  :responsable_registro,
                  :imagen,
                  :remove_imagen,
                  :imagen_cache

  mount_uploader :imagen, SitioTuristicoImagenUploader

  self.table_name = "SITIOS_TURISTICOS"
  self.primary_key = "TUR_ID"

  belongs_to :area_protegida, foreign_key: :AREA_ID
  belongs_to :informacion_geografica, foreign_key: :GEO_ID

  # validations:
    validates :TUR_NOMBRE, :presence => true
    validates :TUR_ATRACTIVO_PRINCIPAL, :presence => true
    validates :TUR_OTROS_ATRACTIVOS, :presence => true
    validates :TUR_TIPO_CLIMA, :presence => true
    validates :TUR_EPOCA_LLUVIAS, :presence => true
    validates :TUR_CAP_CARGA, :presence => true, :numericality => {:greater_than_or_equal_to => 0, :less_than_or_equal_to => 99999}
    validates :TUR_SITIO_INGRESO, :presence => true
    validates :TUR_FECHA_LEVANTAMIENTO, :presence => true
    validates :TUR_DIST_CARRETERA, :presence => true, :numericality => {:greater_than_or_equal_to => 0}
    validates :TUR_DIST_SENDERO, :presence => true, :numericality => {:greater_than_or_equal_to => 0}
    validates :TUR_DIST_OTRO, :presence => true, :numericality => {:greater_than_or_equal_to => 0}
    validates :TUR_DIST_ADMIN_CERCANO, :presence => true, :numericality => {:greater_than_or_equal_to => 0}
    validates :TUR_ACAMPAR_NUM, :presence => true, :numericality => {:greater_than_or_equal_to => 0}
    validates :TUR_ALOJAMIENTO_NUM, :presence => true, :numericality => {:greater_than_or_equal_to => 0}
    validates :TUR_SENDERO_AUTO_NUM, :presence => true, :numericality => {:greater_than_or_equal_to => 0}
    validates :TUR_OTROS_SENDEROS_NUM, :presence => true, :numericality => {:greater_than_or_equal_to => 0}
    validates :TUR_CENTRO_INTERP_NUM, :presence => true, :numericality => {:greater_than_or_equal_to => 0}
    validates :responsable_registro, :presence => true
    validate :fecha_levantamiento_less_than_now

    def fecha_levantamiento_less_than_now
      errors.add(:TUR_FECHA_LEVANTAMIENTO, "No puede ser mayor a la fecha actual") if self.TUR_FECHA_LEVANTAMIENTO and self.TUR_FECHA_LEVANTAMIENTO > Time.now.to_date
    end
    
end
