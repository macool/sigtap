class User < ActiveRecord::Base
  
  establish_connection "wordpress"
  
  @prefix = Rails.application.config.database_configuration["wordpress"]["table_prefix"]
  
  self.table_name = "#{@prefix}users"
  
  # wordpress fields:
    alias_attribute  "id",             "ID"
    alias_attribute  "login",          "user_login"
    alias_attribute  "pass",           "user_pass"
    alias_attribute  "nicename",       "user_nicename"
    alias_attribute  "email",          "user_email"
    alias_attribute  "url",            "user_url"
    alias_attribute  "created_at",     "user_registered"
    alias_attribute  "activation_key", "user_activation_key"
    alias_attribute  "status",         "user_status"
    alias_attribute  "display_name",   "display_name"
    
  # relationships:
    has_many :user_metas
  
  # methods:
    def readonly?
      true
    end
    def before_destroy
      raise ActiveRecord::ReadOnlyRecord
    end
    def check_password( plaintext_password )
      require 'phpass'
      return Phpass.new(8).check(plaintext_password, pass)
    end
    def wp_capabilities
      @wp_capabilities ||= self.user_metas.where(meta_key: "wp_capabilities").select(:meta_value).first.meta_value
    end
    def is_admin?
      @is_admin ||= wp_capabilities.include?("administrator")
    end
  
  # alias methods:
    alias_method :destroy, :before_destroy
  
end