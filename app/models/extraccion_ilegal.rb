class ExtraccionIlegal < ActiveRecord::Base
  attr_accessible :AMENAZA_ID,
                  :EXT_ILE_CANTIDAD,
                  :EXT_ILE_MAT_TIPO,
                  :EXT_ILE_TIPO,
                  :EXT_ILE_USOS,
                  :amenazas_especies_attributes

  self.table_name = "EXTRACCION_ILEGAL"
  self.primary_key = :EXT_ILE_ID

  belongs_to :amenaza, foreign_key: :AMENAZA_ID

  has_many :amenazas_especies, class_name: "AmenazaEspecie",
                            foreign_key: :EXT_ILE_ID,
                            dependent: :destroy
  accepts_nested_attributes_for :amenazas_especies, :reject_if => lambda { |a| a[:AME_ESP_NOMBRE_CIENTIFICO].blank? }, :allow_destroy => true

  def AREA_ID
    self.area_protegida.AREA_ID
  end

  # validates :EXT_ILE_TIPO, :presence => true
  # validates :EXT_ILE_MAT_TIPO, :presence => true
  # validates :EXT_ILE_USOS, :presence => true
  # validates :EXT_ILE_CANTIDAD, :presence => true
  validates :EXT_ILE_CANTIDAD, :numericality => {:greater_than => 0}, :allow_blank => true


end
