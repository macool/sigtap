# encoding: utf-8
class MineriaIlegal < ActiveRecord::Base
  attr_accessible :AMENAZA_ID,
                  :MIN_ILE_AREA,
                  :MIN_ILE_NUM_PERSONAS,
                  :MIN_ILE_PRACTICA,
                  :MIN_ILE_TIPO,
                  :practica

  self.table_name = "MINERIA_ILEGAL"
  self.primary_key = "MIN_ILE_ID"

  belongs_to :amenaza, foreign_key: :AMENAZA_ID
  
  # validates :MIN_ILE_AREA, :presence => true
  # validates :MIN_ILE_NUM_PERSONAS, :presence => true
  # validates :MIN_ILE_TIPO, :presence => true
  validates :MIN_ILE_AREA, :numericality => {:greater_than => 0}, :allow_blank => true
  validates :MIN_ILE_NUM_PERSONAS, :numericality => {:greater_than => 0}, :allow_blank => true
  # validate :at_least_one_practica




  # def MIN_ILE_PRACTICA=( array )
  #   if array.include? ""
  #     array.delete ""
  #   end
  #   write_attribute :MIN_ILE_PRACTICA, array.to_json
  # end
  def practica=( array )
    array.delete("") if array.include?("")
    write_attribute(:MIN_ILE_PRACTICA, array.join(","))
  end
  def MIN_ILE_PRACTICA
    read_attribute(:MIN_ILE_PRACTICA).to_s.split(",")
  end

  alias_method :practica, :MIN_ILE_PRACTICA

  # def at_least_one_practica
  #   errors.add(:practica, "Se debe escoger al menos una práctica") if practica.length == 0
  # end

end
