class Biodiversidad < ActiveRecord::Base
  attr_accessible :AREA_ID,
                  :BIO_ECOSISTEMAS,
                  :BIO_FORMAS_VEGETALES,
                  :BIO_ZONAS_VIDA,
                  :biodiversidad_especies_attributes

  self.primary_key = :BIO_ID
  self.table_name = "BIODIVERSIDAD"

  belongs_to :area_protegida, :foreign_key => :AREA_ID

  has_many :biodiversidad_especies, :class_name => BiodiversidadEspecie,
                                    :foreign_key => :BIO_ID,
                                    :dependent => :destroy

  accepts_nested_attributes_for :biodiversidad_especies, :allow_destroy => true

  # validations:
    validates :BIO_ECOSISTEMAS, :presence => true
    validates :BIO_FORMAS_VEGETALES, :presence => true
    validates :BIO_ZONAS_VIDA, :presence => true

  # instance methods:
    def biodiversidad_especies_fauna
      self.biodiversidad_especies.where(:BIO_ESP_TIPO => "fauna")
    end
    def biodiversidad_especies_flora
      self.biodiversidad_especies.where(:BIO_ESP_TIPO => "flora")
    end

end
