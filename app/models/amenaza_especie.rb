class AmenazaEspecie < ActiveRecord::Base
  attr_accessible :AME_ESP_CATEGORIA,
                  :AME_ESP_NOMBRE_CIENTIFICO,
                  :AME_ESP_NUMERO,
                  :AME_ESP_VOLUMEN,
                  :DEF_ID,
                  :EXT_ILE_ID

  self.table_name = "AMENAZA_ESPECIES"
  self.primary_key = "AME_ESP_ID"

  belongs_to :deforestacion, foreign_key: :DEF_ID
  belongs_to :extraccion_ilegal, foreign_key: :EXT_ILE_ID

  # validates :AME_ESP_NOMBRE_CIENTIFICO, :presence => true
  # validates :AME_ESP_CATEGORIA, :presence => true
  # validates :AME_ESP_NUMERO, :presence => true
  validates :AME_ESP_NUMERO, :numericality => {:greater_than => 0}, :allow_blank => true
  validates :AME_ESP_VOLUMEN, :numericality => {:greater_than => 0}, :allow_blank => true


end
