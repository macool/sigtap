class InformacionTuristica < ActiveRecord::Base
  attr_accessible :AREA_ID,
                  #:INFO_TUR_INFRAESTRUCTURA,
                  :INFO_TUR_NUM_ANUAL_VISITANTES,
                  :INFO_TUR_SITIOS_REP,
                  :INFO_TUR_SITIO_ACCESO,
                  :detalles

   attr_accessor :detalles
    
   before_validation :set_correct_fields


  self.table_name = "INFORMACION_TURISTICA"
  self.primary_key = "INFO_TUR_ID"

  belongs_to :area_protegida, foreign_key: :AREA_ID

  def detalles
    if not self.INFO_TUR_INFRAESTRUCTURA.blank?
      begin
        JSON.parse self.INFO_TUR_INFRAESTRUCTURA
      rescue
        []
      end
    else
      []
    end
  end

  def detalles=( detalles )
    logger.debug "here"
    @detalles = JSON.parse( detalles )
    nil
  end

  def set_correct_fields
    self.INFO_TUR_INFRAESTRUCTURA = @detalles.to_json
    nil
  end

  # validations: 
    validates :INFO_TUR_SITIOS_REP, :presence => true
    validates :INFO_TUR_SITIO_ACCESO, :presence => true
    validates :INFO_TUR_NUM_ANUAL_VISITANTES, :numericality => {:greater_than => 0}, :allow_blank => true
    validate :infraestructura_not_blank

  def infraestructura_not_blank
    unless self.detalles.length > 0
      errors.add :detalles, "No puede quedar en blanco"
    end
  end

end
