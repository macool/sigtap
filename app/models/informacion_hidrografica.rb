class InformacionHidrografica < ActiveRecord::Base
  attr_accessible :AREA_ID,
                  :HIDRO_CUENCAS_PRESENTES,
                  :HIDRO_HUMEDALES,
                  :HIDRO_RIOS_PRINCIPALES,
                  :hidro_humedales_json

  attr_accessor :hidro_humedales_json


  self.table_name = "INFORMACION_HIDROGRAFICA"
  self.primary_key = "HIDRO_ID"

  validates :HIDRO_CUENCAS_PRESENTES, :presence => true
  validates :HIDRO_RIOS_PRINCIPALES, :presence => true

  belongs_to :area_protegida, foreign_key: :AREA_ID

  def HIDRO_HUMEDALES
    @humedales = self.read_attribute(:HIDRO_HUMEDALES)
    if @humedales.blank?
      []
    else
      JSON.parse @humedales
    end
  end

  def hidro_humedales_json=( humedales_str )
    self.HIDRO_HUMEDALES = humedales_str.split(",").to_json
  end

end
