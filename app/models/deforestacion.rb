class Deforestacion < ActiveRecord::Base
  attr_accessible :AMENAZA_ID,
                  :DEF_AREA,
                  :amenazas_especies_attributes

  self.table_name = "DEFORESTACION"
  self.primary_key = :DEF_ID

  belongs_to :amenaza, foreign_key: :AMENAZA_ID

  has_many :amenazas_especies, class_name: "AmenazaEspecie",
                               dependent: :destroy,
                               foreign_key: :DEF_ID
  accepts_nested_attributes_for :amenazas_especies, :reject_if => lambda { |a| a[:AME_ESP_NOMBRE_CIENTIFICO].blank? }, :allow_destroy => true

  def AREA_ID
    self.area_protegida.AREA_ID
  end

  # validates :DEF_AREA, :presence => true
  validates :DEF_AREA, :numericality => {:greater_than => 0}, :allow_blank => true

end
