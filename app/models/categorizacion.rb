# encoding: utf-8
class Categorizacion < ActiveRecord::Base
  attr_accessible :AREA_ID,
                  # :CAT_INT_DETALLES,
                  # :CAT_NAC_DETALLES,
                  # :CAT_NAC_TIPO,
                  # :CAT_TIPO,
                  :tipo,
                  :detalles,
                  :internacional_otro

  attr_accessor :detalles, :tipo


  self.table_name = "CATEGORIZACION"
  self.primary_key = "CAT_ID"

  belongs_to :area_protegida, foreign_key: :AREA_ID

  before_validation :set_correct_fields

  validate :at_least_one_tipo_is_present, :verify_internacional_o_nacional, :internacional_otro_if_otro, :tipo_unico

  def internacional_otro_if_otro
    if self.detalles.include?("Otro")
      errors.add(:internacional_otro, "No puede quedar en blanco") if self.internacional_otro.blank?
    else
      self.internacional_otro = nil
    end
  end
  
  def tipo
    unless self.CAT_TIPO.blank?
      self.CAT_TIPO
    else
      self.CAT_NAC_TIPO
    end
  end

  def detalles
    if not self.CAT_INT_DETALLES.blank?
      d = JSON.parse self.CAT_INT_DETALLES
      # if d.include?("Otro")
      #   d.push self.internacional_otro
      # end
      return d
    elsif not self.CAT_NAC_DETALLES.blank?
      JSON.parse self.CAT_NAC_DETALLES
    else
      ""
    end
  end

  def tipo=( tipo )
    @tipo = tipo
  end

  def detalles=( detalles )
    logger.debug "here"
    @detalles = JSON.parse( detalles )
    self.tipo = @detalles.delete("tipo").to_s.capitalize
    nil
  end

  def set_correct_fields
    if @tipo == "Nacional"
      self.CAT_NAC_TIPO = @tipo
      self.CAT_NAC_DETALLES = @detalles["nacional"].to_json
      self.CAT_TIPO = nil
      self.CAT_INT_DETALLES = nil
    elsif @tipo == "Internacional"
      self.CAT_TIPO = @tipo
      self.CAT_INT_DETALLES = @detalles["internacional"].to_json
      self.CAT_NAC_TIPO = nil
      self.CAT_NAC_DETALLES = nil
    end
    nil
  end

  def at_least_one_tipo_is_present
    if self.CAT_TIPO.blank? and self.CAT_NAC_TIPO.blank?
      errors.add :detalles, "No se ha especificado el tipo de categorización (Nacional o Internacional)"
    end
  end

  def verify_internacional_o_nacional
    case self.tipo
    when "Internacional"
      errors.add(:detalles, "Se debe escoger al menos un tipo de categorización Internacional") unless self.detalles.length > 0
    when "Nacional"
      errors.add(:detalles, "Se debe escoger al menos un tipo en el Subsistema Patrimonio de Áreas Naturales del Estado") if self.detalles["sistema_nacional_de_areas_protegidas"]["subsistema_patrimonio_de_areas_naturales_del_estado"].blank?
      errors.add(:detalles, "Se debe escoger al menos un tipo en el Sistema Nacional de Áreas Protegidas") if self.detalles["sistema_nacional_de_areas_protegidas"]["detalles"].blank?
    end
  end

  def tipo_unico
    # cantidad_nacional = 0
    # cantidad_internacional = 0
    # self.area_protegida.categorizaciones.each do |categorizacion|
    #   if categorizacion.tipo == "Nacional"
    #     cantidad_nacional = cantidad_nacional + 1
    #   else
    #     cantidad_internacional = cantidad_internacional + 1
    #   end
    errors[:base] << "Ya existe una categorización nacional" if self.area_protegida.categorizaciones.where(:CAT_NAC_TIPO => "Nacional").exists? and self.tipo=="Nacional"
    errors[:base] << "Ya existe una categorización internacional" if self.area_protegida.categorizaciones.where(:CAT_TIPO => "Internacional").exists? and self.tipo == "Internacional"
  end

end
