class MineriaLegal < ActiveRecord::Base
  attr_accessible :AMENAZA_ID,
                  :MIN_LEG_CODIGO,
                  :MIN_LEG_CONSECIONADO,
                  :MIN_LEG_ESTADO,
                  :MIN_LEG_EXT_EXPLOTACION,
                  :MIN_LEG_EXT_CONSECION,
                  :MIN_LEG_TIPO,
                  :estado

  self.table_name = "MINERIA_LEGAL"
  self.primary_key = "MIN_LEG_ID"

  belongs_to :amenaza, foreign_key: :AMENAZA_ID

  # validate :at_least_one_estado
  # validates :MIN_LEG_CODIGO, :presence => true
  # validates :MIN_LEG_CONSECIONADO, :presence => true
  # validates :MIN_LEG_EXT_EXPLOTACION, :presence => true
  # validates :MIN_LEG_EXT_CONSECION, :presence => true
  # validates :MIN_LEG_TIPO, :presence => true
  validates :MIN_LEG_EXT_EXPLOTACION, :numericality => {:greater_than => 0}, :allow_blank => true
  validates :MIN_LEG_EXT_CONSECION, :numericality => {:greater_than => 0}, :allow_blank => true

  def AREA_ID
    self.area_protegida.AREA_ID
  end

  def estado=( array )
    array.delete("") if array.include?("")
    write_attribute(:MIN_LEG_ESTADO, array.join(","))
  end
  def MIN_LEG_ESTADO
    read_attribute(:MIN_LEG_ESTADO).to_s.split(",")
  end

  alias_method :estado, :MIN_LEG_ESTADO

  # def at_least_one_estado
  #   errors.add(:estado, "Se debe escoger al menos un estado") if estado.length == 0
  # end

end
