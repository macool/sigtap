class ActividadAgropecuarium < ActiveRecord::Base
  attr_accessible :AMENAZA_ID,
                  :AGR_EXTENSION,
                  :AGR_TIPO,
                  :actividades_y_manejos_attributes

  self.table_name = "ACTIVIDAD_AGROPECUARIA"
  self.primary_key = :AGR_ID

  belongs_to :amenaza, foreign_key: :AMENAZA_ID

  has_many :actividades_y_manejos, :class_name => "ActividadYManejo",
                                   :foreign_key => :AGR_ID,
                                   :dependent => :destroy
  accepts_nested_attributes_for :actividades_y_manejos, :reject_if => lambda { |a| a[:AGR_ACT_TIPO].blank? }, :allow_destroy => true

  # validates :AGR_TIPO, :presence => true
  # validates :AGR_EXTENSION, :presence => true
  validates :AGR_EXTENSION, :numericality => {:greater_than => 0}, :allow_blank => true

end
