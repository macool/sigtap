class InformacionGeografica < ActiveRecord::Base
  attr_accessible :AMENAZA_ID, 
                  :AREA_ID, 
                  :GEO_CANTONES, 
                  :GEO_COORDENADAS_UTM_E_MAX, 
                  :GEO_COORDENADAS_UTM_E_MIN, 
                  :GEO_COORDENADAS_UTM_N_MAX, 
                  :GEO_COORDENADAS_UTM_N_MIN, 
                  :GEO_DATUM, 
                  :GEO_DESDE, 
                  :GEO_HASTA, 
                  :GEO_LIM_ESTE, 
                  :GEO_LIM_NORTE, 
                  :GEO_LIM_OESTE, 
                  :GEO_LIM_SUR, 
                  :GEO_PARROQUIAS, 
                  :GEO_PROVINCIAS, 
                  :GEO_REGION,
                  :GEO_SUPERFICIE, 
                  :TUR_ID,
                  :altitud_max,
                  :altitud_min

  self.table_name = "INFORMACION_GEOGRAFICA"
  self.primary_key = :GEO_ID

  belongs_to :area_protegida, :foreign_key => :AREA_ID
  belongs_to :amenaza, :foreign_key => :AMENAZA_ID

  has_one :sitio_turistico, :foreign_key => :GEO_ID,
                            :dependent => :destroy

  # validations:
    validates :GEO_PROVINCIAS, :presence => true
    validates :GEO_CANTONES, :presence => true
    validates :GEO_DATUM, :uniqueness => true, :allow_blank => true
    validates :GEO_PARROQUIAS, :presence => true
    validates :GEO_COORDENADAS_UTM_N_MIN, :uniqueness => true, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :GEO_COORDENADAS_UTM_N_MAX, :uniqueness => true, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :GEO_COORDENADAS_UTM_E_MAX, :uniqueness => true, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :GEO_COORDENADAS_UTM_E_MIN, :uniqueness => true, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :GEO_DESDE, :presence => true, :uniqueness => true, :numericality => {:greater_than => 0, :less_than => 100000}
    validates :GEO_HASTA, :presence => true, :uniqueness => true, :numericality => {:greater_than => 0, :less_than => 100000}
    validates :GEO_SUPERFICIE, :presence => true, :numericality => {:greater_than => 0}

end
