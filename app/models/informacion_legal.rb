# encoding: utf-8
class InformacionLegal < ActiveRecord::Base
  attr_accessible :AREA_ID,
                  :LEGAL_FECHA,
                  :LEGAL_NUMERO,
                  :LEGAL_TIPO

  self.table_name = "INFORMACION_LEGAL"
  self.primary_key = "LEGA_ID"

  belongs_to :area_protegida, foreign_key: :AREA_ID

  validates :LEGAL_FECHA, :presence => true
  validates :LEGAL_NUMERO, :presence => true
  validates :LEGAL_TIPO, :presence => true
  validate :fecha_less_than_now
  validate :is_unique_on_ap

  # methods
    def fecha_less_than_now
      errors.add(:LEGAL_FECHA, "No puede ser mayor a la fecha actual") if self.LEGAL_FECHA and self.LEGAL_FECHA > Time.now.to_date
    end
    def is_unique_on_ap
      self.area_protegida.informaciones_legales.each do |informacion_legal|
        if informacion_legal != self and informacion_legal.LEGAL_FECHA == self.LEGAL_FECHA and informacion_legal.LEGAL_NUMERO == self.LEGAL_NUMERO and informacion_legal.LEGAL_TIPO == self.LEGAL_TIPO
          errors[:base] << "Ya existe esa información legal."
        end
      end
    end

end
