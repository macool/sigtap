class InfraestructuraVial < ActiveRecord::Base
  attr_accessible :AMENAZA_ID,
                  :INF_VIA_LONGITUD,
                  :INF_VIA_PUNTOFX,
                  :INF_VIA_PUNTOFY,
                  :INF_VIA_PUNTOOX,
                  :INF_VIA_PUNTOOY,
                  :INF_VIA_TIPO

  self.table_name = "INFRAESTRUCTURA_VIAL"
  self.primary_key = "INF_VIA_ID"

  belongs_to :amenaza, foreign_key: :AMENAZA_ID

  # validates :INF_VIA_LONGITUD, :presence => true
  # validates :INF_VIA_PUNTOFX, :presence => true
  # validates :INF_VIA_PUNTOFY, :presence => true
  # validates :INF_VIA_PUNTOOX, :presence => true
  # validates :INF_VIA_PUNTOOY, :presence => true
  # validates :INF_VIA_TIPO, :presence => true
  validates :INF_VIA_LONGITUD, :numericality => {:greater_than => 0}, :allow_blank => true
  validates :INF_VIA_PUNTOFX, :numericality => {:greater_than => 0}, :allow_blank => true
  validates :INF_VIA_PUNTOFY, :numericality => {:greater_than => 0}, :allow_blank => true
  validates :INF_VIA_PUNTOOX, :numericality => {:greater_than => 0}, :allow_blank => true
  validates :INF_VIA_PUNTOOY, :numericality => {:greater_than => 0}, :allow_blank => true


end
