# encoding: utf-8
class Amenaza < ActiveRecord::Base
  attr_accessible :AMENAZA_CONTRAVENTOR_TIPO,
                  :AMENAZA_FECHA_EVENTO,
                  :AMENAZA_FECHA_LEVANTADA,
                  :AMENAZA_TIPO,
                  :AREA_ID,
                  :deforestaciones_attributes,
                  :extracciones_ilegales_attributes,
                  :incendios_attributes,
                  :actividades_agropecuarias_attributes,
                  :minerias_legales_attributes,
                  :minerias_ilegales_attributes,
                  :informaciones_geograficas_attributes,
                  :infraestructuras_viales_attributes,
                  :otras_infraestructuras_attributes,
                  :especies_introducidas_attributes,
                  :actividades_hidrocarburiferas_attributes,
                  :invasiones_attributes,
                  :datum,
                  :coordenada_norte,
                  :coordenada_este,
                  :responsable,
                  :altitud_maxima,
                  :altitud_minima

  self.table_name = "AMENAZAS"
  self.primary_key = :AMENAZA_ID

  # relationships
    belongs_to :area_protegida, :foreign_key => :AREA_ID

    has_many :invasiones, :class_name => "Invasion",
                          :foreign_key => :AMENAZA_ID,
                          :dependent => :destroy
    accepts_nested_attributes_for :invasiones, :allow_destroy => true

    has_many :informaciones_geograficas, :class_name => "InformacionGeografica",
                                         :foreign_key => :AMENAZA_ID,
                                         :dependent => :destroy
    accepts_nested_attributes_for :informaciones_geograficas, :allow_destroy => true

    has_many :actividades_agropecuarias, :class_name => "ActividadAgropecuarium",
                                         :foreign_key => :AMENAZA_ID,
                                         :dependent => :destroy
    accepts_nested_attributes_for :actividades_agropecuarias, :allow_destroy => true

    has_many :otras_infraestructuras, :class_name => "OtraInfraestructura",
                                      :foreign_key => :AMENAZA_ID,
                                      :dependent => :destroy
    accepts_nested_attributes_for :otras_infraestructuras, :allow_destroy => true

    has_many :actividades_hidrocarburiferas, :class_name => "ActividadHidrocarburifera",
                                             :foreign_key => :AMENAZA_ID,
                                             :dependent => :destroy
    accepts_nested_attributes_for :actividades_hidrocarburiferas, :allow_destroy => true

    has_many :especies_introducidas, :class_name => "EspecieIntroducida",
                                     :foreign_key => :AMENAZA_ID,
                                     :dependent => :destroy
    accepts_nested_attributes_for :especies_introducidas, :allow_destroy => true
    
    has_many :incendios, :class_name => "Incendio",
                         :foreign_key => :AMENAZA_ID,
                         :dependent => :destroy
    accepts_nested_attributes_for :incendios, :allow_destroy => true

    has_many :extracciones_ilegales, :class_name => "ExtraccionIlegal",
                                     :foreign_key => :AMENAZA_ID,
                                     :dependent => :destroy
    accepts_nested_attributes_for :extracciones_ilegales, :allow_destroy => true

    has_many :deforestaciones, :class_name => "Deforestacion",
                               :foreign_key => :AMENAZA_ID,
                               :dependent => :destroy
    accepts_nested_attributes_for :deforestaciones, :allow_destroy => true

    has_many :minerias_ilegales, :class_name => "MineriaIlegal",
                                 :foreign_key => :AMENAZA_ID,
                                 :dependent => :destroy
    accepts_nested_attributes_for :minerias_ilegales, :allow_destroy => true
    
    has_many :minerias_legales, :class_name => "MineriaLegal",
                                :foreign_key => :AMENAZA_ID,
                                :dependent => :destroy
    accepts_nested_attributes_for :minerias_legales, :allow_destroy => true

    has_many :infraestructuras_viales, :class_name => "InfraestructuraVial",
                                       :foreign_key => :AMENAZA_ID,
                                       :dependent => :destroy
    accepts_nested_attributes_for :infraestructuras_viales, :allow_destroy => true


    def fecha_levantamiento_less_than_now
      errors.add(:AMENAZA_FECHA_LEVANTADA, "No puede ser mayor a la fecha actual") if self.AMENAZA_FECHA_LEVANTADA and self.AMENAZA_FECHA_LEVANTADA > Time.now.to_date
    end

    def fecha_evento_less_than_now
      errors.add(:AMENAZA_FECHA_EVENTO, "No puede ser mayor a la fecha actual") if self.AMENAZA_FECHA_EVENTO and self.AMENAZA_FECHA_EVENTO > Time.now.to_date
    end

  # validations:
    validates :AMENAZA_TIPO, :presence => true
    validates :AMENAZA_FECHA_LEVANTADA, :presence => true
    validates :AMENAZA_FECHA_EVENTO, :presence => true
    validates :coordenada_norte, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :coordenada_este, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :altitud_maxima, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :altitud_minima, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :responsable, :presence => true
    validate :fecha_levantamiento_less_than_now
    validate :fecha_evento_less_than_now


  # class methods:
    def self.tipos_de_amenaza
      [
        "Deforestación",
        "Extracción Ilegal",
        "Incendios",
        "Actividad Agropecuaria",
        "Minería Legal en el límite del Área Protegida",
        "Minería Ilegal",
        "Infraestructura Vial",
        "Obras de infraestructura",
        "Especies introducidas",
        "Actividades Hidrocarburíferas",
        "Invasiones"
      ]
    end

end
