class InformacionAdministrativa < ActiveRecord::Base
  
  attr_accessible :ADMIN_ADMIN_N,
                  :ADMIN_ADMIN_O,
                  :ADMIN_FECHA_INICIO,
                  :ADMIN_GUARDA_N,
                  :ADMIN_GUARDA_O,
                  :ADMIN_PLAN_MANEJO,
                  :ADMIN_TECNICO_N,
                  :ADMIN_TECNICO_O,
                  :ADMIN_FECHA_PLAN,
                  :AREA_ID,
                  :PERSON_ID,
                  :administrador_actual,
                  :presupuesto_anual

  self.table_name = "INFORMACION_ADMINISTRATIVA"
  self.primary_key = "ADMIN_ID"

  belongs_to :area_protegida, foreign_key: :AREA_ID
  belongs_to :persona, foreign_key: :PERSON_ID

  # validations:
    validates :administrador_actual, :presence => true
    validates :ADMIN_PLAN_MANEJO, :presence => true 
    validates :ADMIN_GUARDA_O, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :ADMIN_GUARDA_N, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :ADMIN_TECNICO_O, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :ADMIN_TECNICO_N, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :ADMIN_ADMIN_O, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :ADMIN_ADMIN_N, :numericality => {:greater_than => 0}, :allow_blank => true
    validates :presupuesto_anual, :numericality => {:greater_than => 0}, :allow_blank => true
    validate :fechas_no_mayor_a_actual
    validate :admin_fecha_plan_if_plan
    
  # methods:
    def fechas_no_mayor_a_actual
      errors.add(:ADMIN_FECHA_INICIO, "No puede ser mayor a la fecha actual") if self.ADMIN_FECHA_INICIO and self.ADMIN_FECHA_INICIO > Time.now.to_date
      errors.add(:ADMIN_FECHA_PLAN, "No puede ser mayor a la fecha actual") if self.ADMIN_FECHA_PLAN and self.ADMIN_FECHA_PLAN > Time.now.to_date
    end
    def admin_fecha_plan_if_plan
      errors.add(:ADMIN_FECHA_PLAN, "Debe llenarse si existe un plan de manejo") if self.ADMIN_PLAN_MANEJO and self.ADMIN_PLAN_MANEJO != "No" and self.ADMIN_FECHA_PLAN.blank?
    end
  
end
