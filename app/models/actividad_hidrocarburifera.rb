class ActividadHidrocarburifera < ActiveRecord::Base
  attr_accessible :ACT_HID_CODIGO,
                  :ACT_HID_CONCESIONADO,
                  :ACT_HID_ESTADO_CONCESION,
                  :ACT_HID_EXPL_DENTRO_AREA,
                  :ACT_HID_EXT_CONCESION,
                  :ACT_HID_EXT_DENTRO_AREA,
                  :ACT_HID_SISTEMA_TRANSPORTE,
                  :ACT_HID_TOTAL_EXPLOTACION,
                  :AMENAZA_ID,
                  :sistema_otros,
                  :sistema,
                  :estado
                  

  self.table_name = "ACTIVIDADES_HIDROCARBURIFERAS"
  self.primary_key = "ACT_HID_ID"

  belongs_to :amenaza, :foreign_key => :AMENAZA_ID

  # validate :at_least_one_sistema
  # validate :at_least_one_estado
  # validates :ACT_HID_CONCESIONADO, :presence => true
  # validates :ACT_HID_CODIGO, :presence => true
  # validates :ACT_HID_EXT_CONCESION, :presence => true
  # validates :ACT_HID_EXT_DENTRO_AREA, :presence => true
  # validates :ACT_HID_TOTAL_EXPLOTACION, :presence => true
  # validates :ACT_HID_EXPL_DENTRO_AREA, :presence => true
  validates :ACT_HID_EXT_CONCESION, :numericality => {:greater_than => 0}, :allow_blank => true
  validates :ACT_HID_EXT_DENTRO_AREA, :numericality => {:greater_than => 0}, :allow_blank => true
  validates :ACT_HID_TOTAL_EXPLOTACION, :numericality => {:greater_than => 0}, :allow_blank => true
  validates :ACT_HID_EXPL_DENTRO_AREA, :numericality => {:greater_than => 0}, :allow_blank => true


  def AREA_ID
    self.area_protegida.AREA_ID
  end

  def sistema=( array )
    array.delete("") if array.include?("")
    write_attribute(:ACT_HID_SISTEMA_TRANSPORTE, array.join(","))
  end

  def ACT_HID_SISTEMA_TRANSPORTE
    read_attribute(:ACT_HID_SISTEMA_TRANSPORTE).to_s.split(",")
  end

  alias_method :sistema, :ACT_HID_SISTEMA_TRANSPORTE

  def at_least_one_sistema
    errors.add(:sistema, "Se debe escoger al menos un sistema") if sistema.length == 0
  end

  def has_sistema_otros?
    sistema.include?("Otro")
  end

  def estado=( array )
    array.delete("") if array.include?("")
    write_attribute(:ACT_HID_ESTADO_CONCESION, array.join(","))
  end

  def ACT_HID_ESTADO_CONCESION
    read_attribute(:ACT_HID_ESTADO_CONCESION).to_s.split(",")
  end

  alias_method :estado, :ACT_HID_ESTADO_CONCESION

  def at_least_one_estado
    errors.add(:estado, "Se debe escoger al menos un estado") if estado.length == 0
  end

end
