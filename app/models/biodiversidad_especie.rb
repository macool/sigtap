# encoding: utf-8
class BiodiversidadEspecie < ActiveRecord::Base
  attr_accessible :BIO_ESP_NOMBRE,
                  :BIO_ESP_NUM_ESPECIES,
                  :BIO_ESP_NUM_FAMILIAS,
                  :BIO_ESP_REPRESENTATIVA,
                  :BIO_ESP_TIPO,
                  :BIO_ID

  self.table_name = "BIODIVERSIDAD_ESPECIES"
  self.primary_key = "BIO_ESP_ID"

  belongs_to :biodiversidad, :foreign_key => :BIO_ID

  validates :BIO_ESP_NOMBRE, :presence => true
  validates :BIO_ESP_TIPO, :presence => true
  validates :BIO_ESP_REPRESENTATIVA, :presence => true
  validates :BIO_ESP_NUM_ESPECIES, :numericality => {:greater_than_or_equal_to => 0, :less_than_or_equal_to => 99999}, :allow_blank => true
  validates :BIO_ESP_NUM_FAMILIAS, :numericality => {:greater_than_or_equal_to => 0, :less_than_or_equal_to => 99999}, :allow_blank => true
  validate :no_more_than_five_names

  # instance methods:

  private

    def no_more_than_five_names
      if self.BIO_ESP_REPRESENTATIVA.split(";").length > 5
        errors.add :BIO_ESP_REPRESENTATIVA, "No pueden haber más de 5 especies representativas"
      end
    end

end
