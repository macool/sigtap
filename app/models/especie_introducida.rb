# encoding: utf-8
class EspecieIntroducida < ActiveRecord::Base
  attr_accessible :AMENAZA_ID,
                  :ESP_INT_ESPECIE,
                  :ESP_INT_EXT_PLANTACION,
                  :ESP_INT_EXT_POBLACION,
                  :ESP_INT_TIPO

  self.table_name = "ESPECIES_INTRODUCIDAS"
  self.primary_key = "ESP_INT_ID"
  
  # callbacks:
    before_save :set_flora_or_fauna

  # associations:
    belongs_to :amenaza, foreign_key: :AMENAZA_ID
  
  # validations:
    # validates :ESP_INT_ESPECIE, :presence => true
    # validate :flora_or_fauna_required
    validate :flora_or_fauna_greater_than_zero
  
  # methods:
    def flora_or_fauna_required
      if self.ESP_INT_TIPO == "Fauna"
        errors.add(:ESP_INT_EXT_POBLACION, "Se debe especificar la población estimada") if self.ESP_INT_EXT_POBLACION.blank?
      else
        errors.add(:ESP_INT_EXT_PLANTACION, "Se debe especificar la población estimada") if self.ESP_INT_EXT_PLANTACION.blank?        
      end
    end
    def flora_or_fauna_greater_than_zero
      if not self.ESP_INT_EXT_POBLACION.blank? and self.ESP_INT_EXT_POBLACION < 0
        errors.add :ESP_INT_EXT_POBLACION, "Debe ser mayor o igual a 0"
      end
      if not self.ESP_INT_EXT_PLANTACION.blank? and self.ESP_INT_EXT_PLANTACION < 0
        errors.add :ESP_INT_EXT_PLANTACION, "Debe ser mayor o igual a 0"
      end
    end
    def set_flora_or_fauna
      if self.ESP_INT_TIPO == "Fauna"
        self.ESP_INT_EXT_PLANTACION = 0
      else
        self.ESP_INT_EXT_POBLACION = 0
      end
    end

end
