$(document).on "nested:fieldAdded", (event) ->
  window.Helpers.ButtonsHelper.init()
  window.Helpers.FormsHelper.init()
  window.Helpers.InfoTitleHelper.init()
  window.Helpers.DatepickerHelper.init()
  null