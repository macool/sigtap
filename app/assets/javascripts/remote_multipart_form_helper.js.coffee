window.Helpers.RemoteMultipartFormHelper = {

  $file_selector: null
  $form: null

  wantsToSubmitForm: (e) ->
    self = window.Helpers.RemoteMultipartFormHelper

    if self.$file_selector[0].files[0]
      e.preventDefault()
      e.stopPropagation()

      data = new FormData()
      # append file:
      data.append self.$file_selector.attr("name"), self.$file_selector[0].files[0]
      # append other data:
      $( self.$form.serializeArray() ).each ( i, obj ) ->
        data.append obj["name"], obj["value"]

      $submit = self.$form.find("input[type=submit]")
      $submit.val $submit.data("disable-with")

      # do post:
      console.debug "doing post."
      $.ajax({
        url: self.$form.attr("action"),
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "script",
        type: 'POST'
      })

      false
    else
      true

  init: ( file_selector_id ) ->
    @$file_selector = $("##{file_selector_id}")
    @$form = $("form")
    @$form.on "submit", @wantsToSubmitForm
}