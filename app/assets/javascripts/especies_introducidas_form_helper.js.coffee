window.Helpers.EspeciesIntroducidasFormHelper = {
  selectChanged: ->
    $this = $(this)
    $wrapper = $this.closest(".especie_introducida_nested_form")
    if $this.val() is "Fauna"
      $wrapper.find(".fauna_plantacion_wrapper").slideDown()
      $wrapper.find(".flora_plantacion_wrapper").slideUp()
    else
      $wrapper.find(".fauna_plantacion_wrapper").slideUp()
      $wrapper.find(".flora_plantacion_wrapper").slideDown()
    
  init: ->
    $(".especies_introducidas_form_tipo_select").on "change", @selectChanged
    $(".especies_introducidas_form_tipo_select").trigger "change"
}
