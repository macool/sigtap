window.Helpers.RemoteLinksHelper = {
  startedLoading: ->
    $("#is_loading").css("display","inline-block")
    null
  finishedLoading: ->
    $("#is_loading").fadeOut()
    null
  initListener: ->
    $(document).on "click", "a[data-remote=true]", @startedLoading
}

jQuery ->
  window.Helpers.RemoteLinksHelper.initListener()
