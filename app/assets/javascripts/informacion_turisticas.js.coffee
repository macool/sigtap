window.Informacion_turistica = {
  form: null
  form_detalles: null

  wantsToSubmit: ->
    self = window.Informacion_turistica

    result = []
    $(".infraestructura:checked").each ->
        result.push $(this).next().text()
    self.form_detalles.val( JSON.stringify(result) )
    null

  init: ->
    this.form = $("#form_for_informacion_turistica")
    this.form_detalles = this.form.find("#informacion_turistica_detalles")
    this.form.on "submit", this.wantsToSubmit
    null
}

  