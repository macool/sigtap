window.InformacionHidrograficaForm = {
  form: null
  input: null
  isGoingToSubmitForm: ->
    self = window.InformacionHidrograficaForm
    humedales = []
    $(".informacion_hidrografica_form_humedales_checkbox:checked").each ->
      humedales.push $(this).next().text()
    self.input.val humedales.join(",")
  init: ->
    this.form = $("#form_for_informacion_hidrografica")
    this.input = this.form.find("#informacion_hidrografica_hidro_humedales_json")

    this.form.on "submit", this.isGoingToSubmitForm
}