window.Helpers.HidrocarburiferasFormHelper = 
  changedSistema: =>
    if @sistema_select.val() is null or @sistema_select.val().indexOf("Otro") is -1
      @sistema_otros.find("input").val("")
      @sistema_otros.slideUp("fast")
    else
      @sistema_otros.slideDown("fast")
    null

  init: ->
    @sistema_select = $(".sistema_select")
    @sistema_otros = $(".sistema_otros_hidden_field")
    @sistema_select.on "change", @changedSistema
    @sistema_select.trigger "change"
    null

_this = window.Helpers.HidrocarburiferasFormHelper