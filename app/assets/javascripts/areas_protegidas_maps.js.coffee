class AreaProtegidaOnMap
  constructor: (@node) ->
    @$node = $(@node)
    @ap_nombre = @$node.data("area_protegida_nombre")
    @ap_info = null
    @locked = false
    @getAPInfo()

    $(node).on "mouseenter", @enteredAP
    $(node).on "mouseleave", @leftAP
    $(node).on "click", @toggleLockAP
  
  unlock: ->
    @$node.attr "fill", window.Helpers.AreasProtegidasMaps.AP_original_color
    @$node.data "locked", false
    @locked = false
    null

  lock: ->
    @$node.attr "fill", window.Helpers.AreasProtegidasMaps.AP_lock_color
    @$node.data "locked", true
    @locked = true
    null

  toggleLockAP: =>
    # unlock others:
    # if @locked
    #   @unlock()
    # else
    #   AreasProtegidasNodes.unlockAll()
    #   @lock()
    $(".show_more_ap_info").trigger("click")
    null

  displayInfo: ->
    if @ap_info
      AreasProtegidasMapsInfo.setTitle @ap_info["Nombre"]
      AreasProtegidasMapsInfo.setInfoFromObjectWithoutKeys @ap_info, ["Nombre", "id"]
    else
      AreasProtegidasMapsInfo.setTitle $("<div />", { html: @ap_nombre.split("_").join(" "), style: "text-transform:capitalize;"})
      AreasProtegidasMapsInfo.setInfo $("<div />", { html: "(No se ha ingresado información todavía)", class: "has_margin align_center"})

  enteredAP: =>
    unless @locked
      @$node.attr "fill", window.Helpers.AreasProtegidasMaps.AP_paint_color
    @displayInfo()
    @lock()
    null

  leftAP: (e) =>
    unless @$node.data("locked")
      @$node.attr "fill", window.Helpers.AreasProtegidasMaps.AP_original_color
      AreasProtegidasMapsInfo.empty()
    if AreasProtegidasNodes.anyLocked()
      AreasProtegidasNodes.lockedOne().displayInfo()
    @unlock()
    AreasProtegidasMapsInfo.empty()
    null
    

  getAPInfo: ->
    $.getJSON "/areas_protegidas/show/#{@ap_nombre.split(" ").join("_").toLowerCase()}", (data) =>
      @ap_info = data




AreasProtegidasMapsInfo = {
  $map_info_title: null
  $map_info_info: null
  $map_instructions: null
  $map_info: null
  empty: ->
    # @$map_info_title.hide()
    # @$map_info_info.hide()
    @$map_instructions.fadeIn(200)
    @$map_info.fadeOut(100)
    null
  setTitle: ( html ) ->
    @$map_info_title.html html
    @$map_info_title.show()
    @$map_info.fadeIn(100)
    null
  setInfo: ( html ) ->
    @$map_info_info.html html
    @$map_info_info.show()
    @$map_instructions.hide()
    null
  setInfoFromObjectWithoutKeys: ( obj, non_keys ) ->
    html = ""
    for key, val of obj
      unless non_keys.indexOf(key) isnt -1 or not val
        html += "<p></p><div><b>#{key}:</b></div><div class='area_protegida_#{key.toLowerCase()}'>#{val}</div>"
    unless html is ""
      html += "<div class='hidden'><p style='text-align:right;margin:0.5em;'><a data-remote='true' data-disable-with='Cargando..' href='/areas_protegidas/#{obj["id"]}' class='btn btn-mini show_more_ap_info'>Ver más</a></p></div>"
    @setInfo html
  init: ->
    @$map_info = $("#map_info")
    @$map_info_title = $("#map_info_title")
    @$map_info_info = $("#map_info_info")
    @$map_instructions = $("#map_info_instructions")
    null
}

window.Helpers.AreasProtegidasMaps = {

  AP_original_color: "#436917"
  AP_paint_color: "#648938"
  AP_lock_color: "#759950"

  init: ->
    $(".area_protegida").initializeAsAreaProtegida();
    AreasProtegidasMapsInfo.init()
}

AreasProtegidasNodes = {
  nodes: []
  anyLocked: ->
    for node in @nodes
      if node.locked
        return true
    return false
  unlockAll: ->
    for node in @nodes
      if node.locked
        node.unlock()
    null
  lockedOne: ->
    for node in @nodes
      if node.locked
        return node
    null
}

$.fn.initializeAsAreaProtegida = ->
  @each ->
    AreasProtegidasNodes.nodes.push(new AreaProtegidaOnMap( this ))

