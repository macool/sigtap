window.Categorizacion = {
  internacionalCheckbox: null
  internacionalCheckboxes: null
  nacionalCheckbox: null
  form: null
  form_detalles: null
  
  clickedInternacionalCheckbox: ->
    $this = $(this)
    if $this.next().text() is "Otro"
      if $this.is(":checked")
        $("#internacional_otro_div_holder").slideDown()
      else
        $("#internacional_otro_div_holder").slideUp()

  wantsToSubmit: ->
    self = window.Categorizacion
    # result = {
    #   tipo: null,
    #   internacional: [],
    #   nacional: {
    #     sistema_nacional_de_areas_protegidas: {
    #       subsistema_patrimonio_de_areas_naturales_del_estado: null,
    #       detalles: null
    #     }
    #     areas_de_vegetacion_y_bosques_protectores: null
    #   }
    # }
    result = {}
    kind_of_categorizacion = $("input[name=kind_of_categorizacion]:checked").attr("id")
    if kind_of_categorizacion is "kind_of_categorizacion_internac"
      result.tipo = "internacional"
      result.internacional = []
      $(".kind_of_categorizacion_internacional:checked").each ->
        result.internacional.push $(this).next().text()

    else if kind_of_categorizacion is "kind_of_categorizacion_nac"
      result.tipo = "nacional"
      result.nacional = {}
      result.nacional.sistema_nacional_de_areas_protegidas = {}
      result.nacional.sistema_nacional_de_areas_protegidas.subsistema_patrimonio_de_areas_naturales_del_estado = $(".categorizacion_nacional_patrimonio:checked").next().text()
      result.nacional.sistema_nacional_de_areas_protegidas.detalles = $(".categorizacion_nacional_sistema_naciona_areas_protegidas_dos:checked").next().text()
      result.nacional.areas_de_vegetacion_y_bosques_protectores = $(".categorizacion_nacional_areas_vegetacion_y_bosques:checked").next().text()

    self.form_detalles.val( JSON.stringify(result) )
    null

  enable_radios: ( selector ) ->
    selector.next().slideDown().find("input[type='radio'] , input[type='checkbox']").removeAttr "disabled"
    null

  disable_radios: ( selector, fast=false ) ->
    if fast
      selector.next().hide().find("input[type='radio'] , input[type='checkbox']").attr("disabled", "disabled")
    else
      selector.next().slideUp().find("input[type='radio'] , input[type='checkbox']").attr("disabled", "disabled")
    null

  clickedInternacional: ->
    self = window.Categorizacion
    self.enable_radios self.internacionalCheckbox
    self.disable_radios self.nacionalCheckbox
    null

  clickedNacional: ->
    self = window.Categorizacion
    self.disable_radios self.internacionalCheckbox
    self.enable_radios self.nacionalCheckbox
    null

  init: ->
    this.internacionalCheckbox = $("#kind_of_categorizacion_internac")
    this.nacionalCheckbox = $("#kind_of_categorizacion_nac")
    this.form = $("#form_for_categorizacion")
    this.form_detalles = this.form.find("#categorizacion_detalles")
    @internacionalCheckboxes = $(".kind_of_categorizacion_internacional")
    
    @internacionalCheckboxes.on "click", @clickedInternacionalCheckbox
    @clickedInternacionalCheckbox.call( @internacionalCheckboxes[4] )
    
    this.disable_radios(this.internacionalCheckbox, true) unless this.internacionalCheckbox[0].checked
    this.disable_radios(this.nacionalCheckbox, true) unless this.nacionalCheckbox[0].checked

    this.form.on "submit", this.wantsToSubmit
    this.internacionalCheckbox.on "click", this.clickedInternacional
    this.nacionalCheckbox.on "click", this.clickedNacional
    null
}

  