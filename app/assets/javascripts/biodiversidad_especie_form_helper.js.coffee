class window.Helpers.BiodiversidadEspecieFormHelper

  constructor: (@container, @nombre_options) ->
    # @nombre_options: nombre_options
    # @number_id: number
    # # nombre_options: [
    # #   "Anfibios",
    # #   "Crustáceos y Bivalvos",
    # #   "Insectos",
    # #   "Mamíferos",
    # #   "Peces",
    # #   "Reptiles",
    # #   "Vasculares",
    # #   "No Vasculares"
    # # ]
    @first_time = true
    @$tipo_select = @container.find(".tipo_de_especie_select")
    @$nombre_select = @container.find(".nombre_de_especie_select")
    @$tipo_select.on "change", @hasChanged
    @$tipo_select.trigger "change"

  hasChanged: =>
    available_options = @optionsShouldBe()
    if @first_time
      @first_time = false
      @$nombre_select.children().each ->
        unless available_options.indexOf($(this).text()) != -1
          $(this).remove()
    else
      @$nombre_select.html("")
      for text in available_options
        @$nombre_select.append $("<option />", { value: text, text: text } )
    null

  optionsShouldBe: =>
    if @$tipo_select.val() == "Fauna"
      [ "Anfibios",
        "Crustáceos y Bivalvos",
        "Insectos",
        "Mamíferos",
        "Peces",
        "Reptiles" ]  
    else
      [ "Vasculares",
        "No Vasculares" ]
