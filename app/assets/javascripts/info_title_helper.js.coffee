window.Helpers.InfoTitleHelper = {
  init: ->
    $(".info_title").each ->
      $this = $(this)
      unless $this.text().trim()[($this.text().trim().length - 1)] == ":"
        $this.text $this.text().trim() + ":"
    null
}

jQuery ->
  window.Helpers.InfoTitleHelper.init()
  null