window.Helpers.UploaderHelper = {
  dropbox: null
  message: null
  capitulo_id: null
  pages_language: null

  template:  "<div class='preview image_uploader_preview'>" +
                "<span class='imageHolder'>" +
                    "<img />" +
                    "<span class='uploaded'></span>" +
                "</span>" +
                "<div class='progressHolder'>" +
                    "<div class='progress progress-striped active'>" +
                        "<div class='bar' style='width:0%;'></div>" +
                    "</div>" +
                "</div>" +
              "</div>"

  createImage: (file) ->
    self = window.Uploader
    preview = $(self.template)
    image = preview.find("img")
    reader = new FileReader()

    image.css {
      "max-height": "80px",
      "max-width" : "80px"
    }

    reader.onload = (e) ->
      image.attr "src", e.target.result

    reader.readAsDataURL(file)
    preview.appendTo(self.dropbox)
    $.data(file,preview)

  init: ( id ) ->
    self = window.Helpers.UploaderHelper
    self.dropbox = $('#' + id)
    self.message = self.dropbox.find(".message")
    self.capitulo_id = $("#capitulo_id").val()
    self.dropbox.filedrop({
      paramname: 'new_image',
      data: {
        capitulo_id: self.capitulo_id,
        language: ->
          return $("#pages_language").val()
      },
      maxfiles: 300,
      maxfilesize: 5,
      url: '/paginas/create_several_pages',
      uploadFinished: (i, file, response) ->
        $.data(file).addClass("done")
        #console.debug "i:"
        #console.debug i
        #console.debug "file:"
        #console.debug file
        #console.debug "response:"
        #console.debug response
        $.data(file).find(".bar").css("width","100%")
        $.data(file).find(".progress").removeClass("progress-striped").removeClass("active").addClass("progress-success")
      error: (err, file) ->
        #console.debug "error:"
        #console.debug err
        #console.debug "file:"
        #console.debug file
        switch err
          when 'BrowserNotSupported' 
            # console.error "BrowserNotSupported"
            window.Notification.create({ kind: "alert-error", content: "lo sentimos. tu navegador no soporta esta funcionalidad." })
          when 'TooManyFiles' 
            # console.error "TooManyFiles"
            window.Notification.create({ kind: "alert-error", content: "has subido muchos archivos. refresca la página" })
          when 'FileTooLarge' 
            # console.error "FileTooLarge"
            window.Notification.create({ kind: "alert-error", content: "archivo muy grande." })
          when 'Internal Server Error'
            window.Notification.create({ kind: "alert-error", content: "error interno del servidor. por favor vuelve a intentar" })
          else
            #console.debug "unkown error"
            window.Notification.create({ kind: "alert-error", content: "ocurrió un error. por favor vuelve a intentar" })
      beforeEach: (file) ->
        unless file.type.match(/^image\//)
          window.Notification.create({ kind: "alert-error", content: "Sólo se permiten imágenes!" })
          return false
      dragOver: ->
        $("#" + id).css("border","4px solid rgba(82, 168, 236, 0.8)")
      dragLeave: ->
        $("#" + id).css("border","4px solid #b94a48")
      docLeave: ->
        $("#" + id).css("border","4px solid #ccc")
      drop: ->
        $("#" + id).css("border","4px solid #ccc")
      uploadStarted: (i, file, len) ->
        #console.debug "Upload started"
        #console.debug "i:"
        #console.debug i
        #console.debug "file:"
        #console.debug file
        #console.debug "len:"
        #console.debug len
        self.createImage file
      progressUpdated: (i, file, progress) ->
        #console.debug "updated progress"
        #console.debug progress
        $.data(file).find(".bar").css("width",progress + "%")

    })
}