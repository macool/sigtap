window.Helpers.FormsHelper = {
  setFieldsWithErrors: ->
    $fields_with_errors = $(".field_with_errors")
    $fields_with_errors.addClass("control-group error")
    $fields_with_errors.parent().find(".help-inline").each ->
      $(this).show().parent().find(".field_with_errors").last().append this
    null
  setDivsOnLabels: ->
    $labels = $("label")
    $labels.next().wrap("<div class='control-group'/>")
    $labels.wrap("<div class='control-group'/>")
    null
  init: ->
    @setFieldsWithErrors()
    @setDivsOnLabels()
    null
}

jQuery ->
  window.Helpers.FormsHelper.init()
