window.Helpers.ConflictoSocioambientalFormHelper = {
  $select: null
  $otra_consecuencia_holder_div: null
  
  changedConsecuencia: ->
    self = window.Helpers.ConflictoSocioambientalFormHelper
    $this = $(this)
    if $this.val() is "Otros"
      self.$otra_consecuencia_holder_div.slideDown()
    else
      self.$otra_consecuencia_holder_div.slideUp()
  
  init: ->
    @$select = $("#conflicto_socioambiental_CON_TIPO")
    @$otra_consecuencia_holder_div = $("#otra_consecuencia_holder_div")
    
    @$select.on "change", @changedConsecuencia
    @$select.trigger "change"
}
