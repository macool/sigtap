window.Helpers.ComunidadFormHelper = 
  changedActividad: =>
    if @actividades_select.val() is null or @actividades_select.val().indexOf("Otros") is -1
      @actividades_otros.find("input").val("")
      @actividades_otros.slideUp("fast")
    else
      @actividades_otros.slideDown("fast")
    null
  changedSocioBosque: =>
    if @pertenece_a_socio_bosque_check_box.is(":checked")
      @pertenece_a_socio_bosque_check_box_div.slideDown("fast")
    else
      @pertenece_a_socio_bosque_check_box_div.slideUp("fast")
    
  init: ->
    @actividades_select = $(".actividades_select")
    @actividades_otros = $(".actividades_otros_hidden_field")
    @actividades_select.on "change", @changedActividad
    @actividades_select.trigger "change"

    @pertenece_a_socio_bosque_check_box = $(".pertenece_a_socio_bosque_check_box")
    @pertenece_a_socio_bosque_check_box_div = $(".pertenece_a_socio_bosque_check_box_div")
    @pertenece_a_socio_bosque_check_box.on "change", @changedSocioBosque
    @pertenece_a_socio_bosque_check_box.trigger "change"
    null

_this = window.Helpers.ComunidadFormHelper