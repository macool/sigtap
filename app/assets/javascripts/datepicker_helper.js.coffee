window.Helpers.DatepickerHelper = {
  init: ->
    $datepickers = $(".datepicker:not(.dropdown-menu)")
    if $datepickers.length > 0
      $datepickers.datepicker({
        format: 'yyyy-mm-dd'
      })
    null
}

jQuery ->
  window.Helpers.DatepickerHelper.init()
