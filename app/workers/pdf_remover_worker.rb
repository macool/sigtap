class PdfRemoverWorker
  include Sidekiq::Worker

  sidekiq_options queue: "pdf_remover_worker"

  def perform( filename )
    File.delete( filename )
  end
  
  
end