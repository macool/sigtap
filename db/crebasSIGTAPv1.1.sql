/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     10/12/2012 17:40:16                          */
/*==============================================================*/


drop table if exists ACTIVIDADES_HIDROCARBURIFERAS;

drop table if exists ACTIVIDAD_AGROPECUARIA;

drop table if exists ACTIVIDAD_Y_MANEJO;

drop table if exists AMENAZAS;

drop table if exists AMENAZA_ESPECIES;

drop table if exists AREAS_PROTEGIDAS;

drop table if exists BIODIVERSIDAD;

drop table if exists BIODIVERSIDAD_ESPECIES;

drop table if exists CATEGORIZACION;

drop table if exists COMUNIDADES;

drop table if exists CONFLICTOS_SOCIOAMBIENTALES;

drop table if exists DEFORESTACION;

drop table if exists ESPECIES_INTRODUCIDAS;

drop table if exists EXTRACCION_ILEGAL;

drop table if exists INCENDIO;

drop table if exists INFORMACION_ADMINISTRATIVA;

drop table if exists INFORMACION_CLIMATICA;

drop table if exists INFORMACION_GEOGRAFICA;

drop table if exists INFORMACION_HIDROGRAFICA;

drop table if exists INFORMACION_LEGAL;

drop table if exists INFORMACION_TURISTICA;

drop table if exists INFRAESTRUCTURA_VIAL;

drop table if exists INVASIONES;

drop table if exists MINERIA_ILEGAL;

drop table if exists MINERIA_LEGAL;

drop table if exists OTRAS_INFRAESTRCUTURAS;

drop table if exists PERSONAS;

drop table if exists SITIOS_TURISTICOS;

/*==============================================================*/
/* Table: ACTIVIDADES_HIDROCARBURIFERAS                         */
/*==============================================================*/
create table ACTIVIDADES_HIDROCARBURIFERAS
(
   ACT_HID_ID           int not null,
   AMENAZA_ID           int,
   ACT_HID_CONCESIONADO varchar(250),
   ACT_HID_CODIGO       varchar(10),
   ACT_HID_EXT_CONCESION decimal(6,2),
   ACT_HID_EXT_DENTRO_AREA decimal(4,1),
   ACT_HID_TOTAL_EXPLOTACION decimal(4,2),
   ACT_HID_EXPL_DENTRO_AREA decimal(4,2),
   ACT_HID_ESTADO_CONCESION varchar(50),
   ACT_HID_SISTEMA_TRANSPORTE varchar(150),
   primary key (ACT_HID_ID)
);

/*==============================================================*/
/* Table: ACTIVIDAD_AGROPECUARIA                                */
/*==============================================================*/
create table ACTIVIDAD_AGROPECUARIA
(
   AGR_ID               int not null,
   AMENAZA_ID           int,
   AGR_TIPO             varchar(10),
   AGR_EXTENSION        decimal(6,2),
   primary key (AGR_ID)
);

/*==============================================================*/
/* Table: ACTIVIDAD_Y_MANEJO                                    */
/*==============================================================*/
create table ACTIVIDAD_Y_MANEJO
(
   AGR_ACT_ID           int not null,
   AGR_ID               int,
   AGR_ACT_TIPO         varchar(200),
   AGR_ACT_MANEJO       varchar(250),
   primary key (AGR_ACT_ID)
);

/*==============================================================*/
/* Table: AMENAZAS                                              */
/*==============================================================*/
create table AMENAZAS
(
   AMENAZA_ID           int not null,
   INV_ID               int,
   DEF_ID               int,
   AREA_ID              int,
   AMENAZA_FECHA_LEVANTADA date,
   AMENAZA_FECHA_EVENTO date,
   AMENZA_CONTRAVENTOR_TIPO varchar(50),
   AMENAZA_TIPO         varchar(60),
   primary key (AMENAZA_ID)
);

/*==============================================================*/
/* Table: AMENAZA_ESPECIES                                      */
/*==============================================================*/
create table AMENAZA_ESPECIES
(
   AME_ESP_ID           int not null,
   EXT_ILE_ID           int,
   DEF_ID               int,
   AME_ESP_NOMBRE_CIENTIFICO varchar(250),
   AME_ESP_CATEGORIA    varchar(250),
   AME_ESP_NUMERO       int,
   AME_ESP_VOLUMEN      varchar(250),
   primary key (AME_ESP_ID)
);

/*==============================================================*/
/* Table: AREAS_PROTEGIDAS                                      */
/*==============================================================*/
create table AREAS_PROTEGIDAS
(
   AREA_ID              int not null,
   BIO_ID               int,
   GEO_ID               int,
   CLIMA_ID             int,
   CAT_ID               int,
   AREA_NOMBRE          varchar(80),
   AREA_TIPO            varchar(50),
   AREA_OBJETIVOS       varchar(500),
   AREA_FECHA_LEVANTAMIENTO date,
   primary key (AREA_ID)
);

/*==============================================================*/
/* Table: BIODIVERSIDAD                                         */
/*==============================================================*/
create table BIODIVERSIDAD
(
   BIO_ID               int not null,
   AREA_ID              int,
   BIO_ECOSISTEMAS      varchar(250),
   BIO_FORMAS_VEGETALES varchar(250),
   BIO_ZONAS_VIDA       varchar(250),
   primary key (BIO_ID)
);

/*==============================================================*/
/* Table: BIODIVERSIDAD_ESPECIES                                */
/*==============================================================*/
create table BIODIVERSIDAD_ESPECIES
(
   BIO_ESP_ID           int not null,
   BIO_ID               int,
   BIO_ESP_TIPO         varchar(50),
   BIO_ESP_NUM_FAMILIAS int,
   BIO_ESP_NUM_ESPECIES int,
   BIO_ESP_REPESENTATIVA varchar(250),
   BIO_ESP_NOMBRE       varchar(50),
   primary key (BIO_ESP_ID)
);

/*==============================================================*/
/* Table: CATEGORIZACION                                        */
/*==============================================================*/
create table CATEGORIZACION
(
   CAT_ID               int not null,
   AREA_ID              int,
   CAT_TIPO             varchar(20),
   CAT_INT_DETALLES     varchar(1000),
   CAT_NAC_TIPO         varchar(500),
   CAT_NAC_DETALLES     varchar(1000),
   primary key (CAT_ID)
);

/*==============================================================*/
/* Table: COMUNIDADES                                           */
/*==============================================================*/
create table COMUNIDADES
(
   COM_ID               int not null,
   AREA_ID              int,
   COM_FECHA_LEVANTAMIENTO date,
   COM_NOMBRE           varchar(50),
   COM_GRUPO_ETNICO_PREDOMINANTE varchar(100),
   COM_TIPO_ORGA_SOCIAL varchar(100),
   COM_LIDER_COMUNITARIO varchar(250),
   COM_SUPERFICIE_APROX decimal(6,2),
   COM_LOCALIZACION     varchar(10),
   COM_NUM_FAMILIAS     int,
   COM_NUM_INDIVIDUOS   int,
   COM_ACTIVIDADES      varchar(250),
   COM_SOCIO_BOSQUE     varchar(4),
   COM_HEC_SOCIOBOSQUE  decimal(6,2),
   COM_HEC_SOCIOPARAMO  decimal(6,2),
   primary key (COM_ID)
);

/*==============================================================*/
/* Table: CONFLICTOS_SOCIOAMBIENTALES                           */
/*==============================================================*/
create table CONFLICTOS_SOCIOAMBIENTALES
(
   CON_ID               int not null,
   AREA_ID              int,
   CON_FECHA_LEVANTAMIENTO date,
   CON_TIPO             varchar(150),
   CON_FECHA_DESDE      date,
   CON_FECHA_HASTA      date,
   CON_ESTADO           varchar(150),
   CON_INVOLUCRADO      varchar(1000),
   primary key (CON_ID)
);

/*==============================================================*/
/* Table: DEFORESTACION                                         */
/*==============================================================*/
create table DEFORESTACION
(
   DEF_ID               int not null,
   AMENAZA_ID           int,
   DEF_AREA             varchar(100),
   primary key (DEF_ID)
);

/*==============================================================*/
/* Table: ESPECIES_INTRODUCIDAS                                 */
/*==============================================================*/
create table ESPECIES_INTRODUCIDAS
(
   ESP_INT_ID           int not null,
   AMENAZA_ID           int,
   ESP_INT_TIPO         varchar(20),
   ESP_INT_ESPECIE      varchar(250),
   ESP_INT_EXT_PLANTACION decimal(2),
   ESP_INT_EXT_POBLACION int,
   primary key (ESP_INT_ID)
);

/*==============================================================*/
/* Table: EXTRACCION_ILEGAL                                     */
/*==============================================================*/
create table EXTRACCION_ILEGAL
(
   EXT_ILE_ID           int not null,
   AMENAZA_ID           int,
   EXT_ILE_TIPO         varchar(30),
   EXT_ILE_MAT_TIPO     varchar(30),
   EXT_ILE_USOS         varchar(250),
   EXT_ILE_CANTIDAD     int,
   primary key (EXT_ILE_ID)
);

/*==============================================================*/
/* Table: INCENDIO                                              */
/*==============================================================*/
create table INCENDIO
(
   INC_ID               int not null,
   AMENAZA_ID           int,
   INC_AREA             decimal(6,2),
   INC_ECOSISTEMA       varchar(500),
   INC_ORIGEN           varchar(40),
   primary key (INC_ID)
);

/*==============================================================*/
/* Table: INFORMACION_ADMINISTRATIVA                            */
/*==============================================================*/
create table INFORMACION_ADMINISTRATIVA
(
   ADMIN_ID             int not null,
   AREA_ID              int,
   PERSON_ID            int,
   ADMIN_FECHA_INICIO   date,
   ADMIN_PLAN_MANEJO    varchar(30),
   ADMIN_GUARDA_N       int,
   ADMIN_GUARDA_O       int,
   ADMIN_TECNICO_N      int,
   ADMIN_TECNICO_O      int,
   ADMIN_ADMIN_N        int,
   ADMIN_ADMIN_O        int,
   primary key (ADMIN_ID)
);

/*==============================================================*/
/* Table: INFORMACION_CLIMATICA                                 */
/*==============================================================*/
create table INFORMACION_CLIMATICA
(
   CLIMA_ID             int not null,
   AREA_ID              int,
   CLIMA_PRECIP_DESDE   decimal(2),
   CLIMA_PRECIP_HASTA   decimal(2),
   CLIMA_               decimal(2),
   CLIMA_TEMP_HASTA     decimal(2),
   CLIMA_HUME_DESDE     decimal(2),
   CLIMA_HUME_HASTA     decimal(2),
   primary key (CLIMA_ID)
);

/*==============================================================*/
/* Table: INFORMACION_GEOGRAFICA                                */
/*==============================================================*/
create table INFORMACION_GEOGRAFICA
(
   GEO_ID               int not null,
   AMENAZA_ID           int,
   TUR_ID               int,
   AREA_ID              int,
   GEO_REGION           char(50),
   GEO_PROVINCIAS       varchar(250),
   GEO_CANTONES         varchar(500),
   GEO_PARROQUIAS       varchar(1000),
   GEO_DATUM            varchar(50),
   GEO_COORDENADAS_UTM_N_MIN decimal(2),
   GEO_COORDENADAS_UTM_E_MIN decimal(2),
   GEO_COORDENADAS_UTM_N_MAX decimal(2),
   GEO_COORDENADAS_UTM_E_MAX decimal(2),
   GEO_DESDE            int,
   GEO_HASTA            int,
   GEO_LIM_NORTE        varchar(250),
   GEO_LIM_SUR          varchar(250),
   GEO_LIM_ESTE         varchar(250),
   GEO_LIM_OESTE        varchar(250),
   primary key (GEO_ID)
);

/*==============================================================*/
/* Table: INFORMACION_HIDROGRAFICA                              */
/*==============================================================*/
create table INFORMACION_HIDROGRAFICA
(
   HIDRO_ID             int not null,
   AREA_ID              int,
   HIDRO_CUENCAS_PRESENTES varchar(250),
   HIDRO_RIOS_PRINCIPALES varchar(250),
   HIDRO_HUMEDALES      varchar(1000),
   primary key (HIDRO_ID)
);

/*==============================================================*/
/* Table: INFORMACION_LEGAL                                     */
/*==============================================================*/
create table INFORMACION_LEGAL
(
   LEGA_ID              int not null,
   AREA_ID              int,
   LEGAL_TIPO           varchar(30),
   LEGAL_FECHA          date,
   LEGAL_NUMERO         int,
   primary key (LEGA_ID)
);

/*==============================================================*/
/* Table: INFORMACION_TURISTICA                                 */
/*==============================================================*/
create table INFORMACION_TURISTICA
(
   INFO_TUR_ID          int not null,
   AREA_ID              int,
   INFO_TUR_SITIOS_REP  varchar(250),
   INFO_TUR_INFRAESTRUCTURA varchar(100),
   INFO_TUR_SITIO_ACCESO varchar(250),
   INFO_TUR_NUM_ANUAL_VISITANTES int,
   primary key (INFO_TUR_ID)
);

/*==============================================================*/
/* Table: INFRAESTRUCTURA_VIAL                                  */
/*==============================================================*/
create table INFRAESTRUCTURA_VIAL
(
   INF_VIA_ID           int not null,
   AMENAZA_ID           int,
   INF_VIA_PUNTOOX      decimal(4,2),
   INF_VIA_PUNTOOY      decimal(4,2),
   INF_VIA_PUNTOFX      decimal(4,2),
   INF_VIA_PUNTOFY      decimal(4,2),
   INF_VIA_LONGITUD     decimal(4,2),
   INF_VIA_TIPO         varchar(30),
   primary key (INF_VIA_ID)
);

/*==============================================================*/
/* Table: INVASIONES                                            */
/*==============================================================*/
create table INVASIONES
(
   INV_ID               int not null,
   INV_TIPO             varchar(250),
   INV_EXTENSION        decimal(4,2),
   INV_NUM_PERSONAS     int,
   primary key (INV_ID)
);

/*==============================================================*/
/* Table: MINERIA_ILEGAL                                        */
/*==============================================================*/
create table MINERIA_ILEGAL
(
   MIN_ILE_ID           int not null,
   AMENAZA_ID           int,
   MIN_ILE_AREA         decimal(4),
   MIN_ILE_NUM_PERSONAS int,
   MIN_ILE_TIPO         varchar(100),
   MIN_ILE_PRACTICA     varchar(100),
   primary key (MIN_ILE_ID)
);

/*==============================================================*/
/* Table: MINERIA_LEGAL                                         */
/*==============================================================*/
create table MINERIA_LEGAL
(
   MIN_LEG_ID           int not null,
   AMENAZA_ID           int,
   MIN_LEG_CONSECIONADO varchar(100),
   MIN_LEG_CODIGO       varchar(50),
   MIN_LEG_EXT_CONSECION decimal(4),
   MIN_LEG_EXT_EXPLOTACION decimal(4),
   MIN_LEG_TIPO         varchar(50),
   MIN_LEG_ESTADO       varchar(50),
   primary key (MIN_LEG_ID)
);

/*==============================================================*/
/* Table: OTRAS_INFRAESTRCUTURAS                                */
/*==============================================================*/
create table OTRAS_INFRAESTRCUTURAS
(
   OTR_INF_ID           int not null,
   AMENAZA_ID           int,
   OTR_INF_AREA_COBERTURA decimal(4,2),
   OTR_INF_TIPO         varchar(250),
   primary key (OTR_INF_ID)
);

/*==============================================================*/
/* Table: PERSONAS                                              */
/*==============================================================*/
create table PERSONAS
(
   PERSON_ID            int not null,
   TUR_ID               int,
   CON_ID               int,
   AREA_ID              int,
   ADMIN_ID             int,
   PERSONA_NOMBRE       varchar(100),
   PERSONA_APELLIDOS    varchar(100),
   PERSONA_MAIL         varchar(80),
   PERSONA_TELEFONO     varchar(20),
   PERSONA_DIRECCION    varchar(200),
   PERSONA_ROL          varchar(50),
   primary key (PERSON_ID)
);

/*==============================================================*/
/* Table: SITIOS_TURISTICOS                                     */
/*==============================================================*/
create table SITIOS_TURISTICOS
(
   TUR_ID               int not null,
   GEO_ID               int,
   AREA_ID              int,
   TUR_FECHA_LEVANTAMIENTO date,
   TUR_NOMBRE           varchar(100),
   TUR_ATRACTIVO_PRINCIPAL varchar(100),
   TUR_OTROS_ATRACTIVOS varchar(100),
   TUR_TIPO_CLIMA       varchar(100),
   TUR_EPOCA_LLUVIAS    varchar(100),
   TUR_CAP_CARGA        int,
   TUR_SITIO_INGRESO    varchar(50),
   TUR_DIST_SENDERO     decimal(6,2),
   TUR_DIST_CARRETERA   decimal(6,2),
   TUR_DIST_OTRO        decimal(6,2),
   TUR_DIST_ADMIN_CERCANO decimal(6,2),
   TUR_ACAMPAR_NUM      int,
   TUR_ALOJAMIENTO_NUM  int,
   TUR_SENDERO_AUTO_NUM int,
   TUR_OTROS_SENDEROS_NUM int,
   TUR_CENTRO_INTERP_NUM int,
   primary key (TUR_ID)
);

alter table ACTIVIDADES_HIDROCARBURIFERAS add constraint FK_PUEDE_SER_AH foreign key (AMENAZA_ID)
      references AMENAZAS (AMENAZA_ID) on delete restrict on update restrict;

alter table ACTIVIDAD_AGROPECUARIA add constraint FK_PUEDE_SER_AA foreign key (AMENAZA_ID)
      references AMENAZAS (AMENAZA_ID) on delete restrict on update restrict;

alter table ACTIVIDAD_Y_MANEJO add constraint FK_TIENE_AM foreign key (AGR_ID)
      references ACTIVIDAD_AGROPECUARIA (AGR_ID) on delete restrict on update restrict;

alter table AMENAZAS add constraint FK_PUEDE_SER_A foreign key (INV_ID)
      references INVASIONES (INV_ID) on delete restrict on update restrict;

alter table AMENAZAS add constraint FK_PUEDE_SER_D foreign key (DEF_ID)
      references DEFORESTACION (DEF_ID) on delete restrict on update restrict;

alter table AMENAZAS add constraint FK_TIENE_AMENAZAS foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

alter table AMENAZA_ESPECIES add constraint FK_DEFORESTA foreign key (DEF_ID)
      references DEFORESTACION (DEF_ID) on delete restrict on update restrict;

alter table AMENAZA_ESPECIES add constraint FK_EXTRAE foreign key (EXT_ILE_ID)
      references EXTRACCION_ILEGAL (EXT_ILE_ID) on delete restrict on update restrict;

alter table AREAS_PROTEGIDAS add constraint FK_ESTA_UBICADA foreign key (GEO_ID)
      references INFORMACION_GEOGRAFICA (GEO_ID) on delete restrict on update restrict;

alter table AREAS_PROTEGIDAS add constraint FK_INCLUYE foreign key (BIO_ID)
      references BIODIVERSIDAD (BIO_ID) on delete restrict on update restrict;

alter table AREAS_PROTEGIDAS add constraint FK_LE_PERTENECE foreign key (CLIMA_ID)
      references INFORMACION_CLIMATICA (CLIMA_ID) on delete restrict on update restrict;

alter table AREAS_PROTEGIDAS add constraint FK_PERTENECE_A foreign key (CAT_ID)
      references CATEGORIZACION (CAT_ID) on delete restrict on update restrict;

alter table BIODIVERSIDAD add constraint FK_INCLUYE2 foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

alter table BIODIVERSIDAD_ESPECIES add constraint FK_CONTIENE foreign key (BIO_ID)
      references BIODIVERSIDAD (BIO_ID) on delete restrict on update restrict;

alter table CATEGORIZACION add constraint FK_PERTENECE_A2 foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

alter table COMUNIDADES add constraint FK_TIENE_COM foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

alter table CONFLICTOS_SOCIOAMBIENTALES add constraint FK_PUEDE_TENER foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

alter table DEFORESTACION add constraint FK_PUEDE_SER_D2 foreign key (AMENAZA_ID)
      references AMENAZAS (AMENAZA_ID) on delete restrict on update restrict;

alter table ESPECIES_INTRODUCIDAS add constraint FK_PUEDE_SER_EI foreign key (AMENAZA_ID)
      references AMENAZAS (AMENAZA_ID) on delete restrict on update restrict;

alter table EXTRACCION_ILEGAL add constraint FK_PUEDE_SER_E foreign key (AMENAZA_ID)
      references AMENAZAS (AMENAZA_ID) on delete restrict on update restrict;

alter table INCENDIO add constraint FK_PUEDE_SER_IN foreign key (AMENAZA_ID)
      references AMENAZAS (AMENAZA_ID) on delete restrict on update restrict;

alter table INFORMACION_ADMINISTRATIVA add constraint FK_ADMINISTRA foreign key (PERSON_ID)
      references PERSONAS (PERSON_ID) on delete restrict on update restrict;

alter table INFORMACION_ADMINISTRATIVA add constraint FK_GESTIONA foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

alter table INFORMACION_CLIMATICA add constraint FK_LE_PERTENECE2 foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

alter table INFORMACION_GEOGRAFICA add constraint FK_ESTA_UBICADA2 foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

alter table INFORMACION_GEOGRAFICA add constraint FK_ESTA_UBICADA_AMENAZA foreign key (AMENAZA_ID)
      references AMENAZAS (AMENAZA_ID) on delete restrict on update restrict;

alter table INFORMACION_GEOGRAFICA add constraint FK_ESTA_UBICADO2 foreign key (TUR_ID)
      references SITIOS_TURISTICOS (TUR_ID) on delete restrict on update restrict;

alter table INFORMACION_HIDROGRAFICA add constraint FK_PERTENECE foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

alter table INFORMACION_LEGAL add constraint FK_MANEJA foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

alter table INFORMACION_TURISTICA add constraint FK_POSEE foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

alter table INFRAESTRUCTURA_VIAL add constraint FK_PUEDE_SER_IV foreign key (AMENAZA_ID)
      references AMENAZAS (AMENAZA_ID) on delete restrict on update restrict;

alter table MINERIA_ILEGAL add constraint FK_PUEDE_SER_MI foreign key (AMENAZA_ID)
      references AMENAZAS (AMENAZA_ID) on delete restrict on update restrict;

alter table MINERIA_LEGAL add constraint FK_PUEDE_SER_ML foreign key (AMENAZA_ID)
      references AMENAZAS (AMENAZA_ID) on delete restrict on update restrict;

alter table OTRAS_INFRAESTRCUTURAS add constraint FK_PUEDE_SER_OI foreign key (AMENAZA_ID)
      references AMENAZAS (AMENAZA_ID) on delete restrict on update restrict;

alter table PERSONAS add constraint FK_ADMINISTRA2 foreign key (ADMIN_ID)
      references INFORMACION_ADMINISTRATIVA (ADMIN_ID) on delete restrict on update restrict;

alter table PERSONAS add constraint FK_REGISTRA foreign key (CON_ID)
      references CONFLICTOS_SOCIOAMBIENTALES (CON_ID) on delete restrict on update restrict;

alter table PERSONAS add constraint FK_REGISTRA_ST foreign key (TUR_ID)
      references SITIOS_TURISTICOS (TUR_ID) on delete restrict on update restrict;

alter table PERSONAS add constraint FK_TIENE foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

alter table SITIOS_TURISTICOS add constraint FK_ESTA_UBICADO foreign key (GEO_ID)
      references INFORMACION_GEOGRAFICA (GEO_ID) on delete restrict on update restrict;

alter table SITIOS_TURISTICOS add constraint FK_TIENE_ST foreign key (AREA_ID)
      references AREAS_PROTEGIDAS (AREA_ID) on delete restrict on update restrict;

