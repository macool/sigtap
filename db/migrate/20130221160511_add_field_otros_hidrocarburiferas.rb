class AddFieldOtrosHidrocarburiferas < ActiveRecord::Migration
  def up
  	change_table :ACTIVIDADES_HIDROCARBURIFERAS do |t|
      t.string :sistema_otros
    end
  end

  def down
  	change_table :ACTIVIDADES_HIDROCARBURIFERAS do |t|
      t.remove :sistema_otros
    end
  end
end

