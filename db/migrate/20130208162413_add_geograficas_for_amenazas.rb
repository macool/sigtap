class AddGeograficasForAmenazas < ActiveRecord::Migration
  def up
  	change_table :AMENAZAS do |t|
      t.string :datum
      t.float :coordenada_norte
      t.float :coordenada_este
      t.float :altitud
      t.string :provincia
      t.string :canton
      t.string :parroquia
    end
  end

  def down
  	change_table :AMENAZAS do |t|
  	  t.remove :datum
      t.remove :coordenada_norte
      t.remove :coordenada_este
      t.remove :altitud
      t.remove :provincia
      t.remove :canton
      t.remove :parroquia
  	end
  end
end
