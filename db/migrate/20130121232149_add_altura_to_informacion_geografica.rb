class AddAlturaToInformacionGeografica < ActiveRecord::Migration
  def up
    change_table :INFORMACION_GEOGRAFICA do |t|
      t.float :altitud_max
      t.float :altitud_min
    end
  end
  def down
    change_table :INFORMACION_GEOGRAFICA do |t|
      t.remove :altitud_max, :altitud_min
    end
  end
end
