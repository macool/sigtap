class AddResponsableToComunidad < ActiveRecord::Migration
  def up
    change_table :SITIOS_TURISTICOS do |t|
      t.string :responsable_registro
    end
  end
  def down
    change_table :SITIOS_TURISTICOS do |t|
      t.remove :responsable_registro
    end
  end
end
