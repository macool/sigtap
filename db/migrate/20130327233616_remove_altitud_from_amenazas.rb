class RemoveAltitudFromAmenazas < ActiveRecord::Migration
  def up
  	change_table :AMENAZAS do |t|
      t.remove :altitud
    end
  end

  def down
  	change_table :AMENAZAS do |t|
      t.double :altitud
    end
  end
end
