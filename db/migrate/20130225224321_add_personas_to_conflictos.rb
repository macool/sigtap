class AddPersonasToConflictos < ActiveRecord::Migration
  def up
  	change_table :CONFLICTOS_SOCIOAMBIENTALES do |t|
      t.string :conf_soc_pers_invol_morador
      t.string :conf_soc_pers_invol_morador_tipo
      t.string :conf_soc_pers_invol_a_local
      t.string :conf_soc_pers_invol_a_local_tipo
	  t.string :conf_soc_pers_invol_a_supralocal
	  t.string :conf_soc_pers_invol_a_supralocal_tipo

	  t.string :conf_soc_pers_afec_morador
      t.string :conf_soc_pers_afec_morador_tipo
      t.string :conf_soc_pers_afec_a_local
      t.string :conf_soc_pers_afec_a_local_tipo
	  t.string :conf_soc_pers_afec_a_supralocal
	  t.string :conf_soc_pers_afec_a_supralocal_tipo
    end
  end

  def down
  	change_table :CONFLICTOS_SOCIOAMBIENTALES do |t|
      t.remove :conf_soc_pers_invol_morador
      t.remove :conf_soc_pers_invol_morador_tipo
      t.remove :conf_soc_pers_invol_a_local
      t.remove :conf_soc_pers_invol_a_local_tipo
	  t.remove :conf_soc_pers_invol_a_supralocal
	  t.remove :conf_soc_pers_invol_a_supralocal_tipo

	  t.remove :conf_soc_pers_afec_morador
      t.remove :conf_soc_pers_afec_morador_tipo
      t.remove :conf_soc_pers_afec_a_local
      t.remove :conf_soc_pers_afec_a_local_tipo
	  t.remove :conf_soc_pers_afec_a_supralocal
	  t.remove :conf_soc_pers_afec_a_supralocal_tipo
    end
  	
  end
end
