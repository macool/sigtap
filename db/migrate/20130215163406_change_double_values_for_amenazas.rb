class ChangeDoubleValuesForAmenazas < ActiveRecord::Migration
  def up
  	change_table :AMENAZAS do |t|
      t.remove :coordenada_norte
      t.remove :coordenada_este
      t.remove :altitud

    end
  end

  def down
  	change_table :AMENAZAS do |t|
      t.float :coordenada_norte
      t.float :coordenada_este
      t.float :altitud
  	end
  end
end
