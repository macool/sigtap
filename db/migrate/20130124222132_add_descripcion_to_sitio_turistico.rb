class AddDescripcionToSitioTuristico < ActiveRecord::Migration
  def up
    change_table :SITIOS_TURISTICOS do |t|
      t.string :descripcion_recorrido
    end
  end
  def down
    change_table :SITIOS_TURISTICOS do |t|
      t.remove :descripcion_recorrido
    end
  end
end
