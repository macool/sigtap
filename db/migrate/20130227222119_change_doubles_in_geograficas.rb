class ChangeDoublesInGeograficas < ActiveRecord::Migration
  def up
  	change_column :INFORMACION_GEOGRAFICA, :GEO_COORDENADAS_UTM_N_MIN, :decimal, :precision => 10, :scale => 2
  	change_column :INFORMACION_GEOGRAFICA, :GEO_COORDENADAS_UTM_E_MIN, :decimal, :precision => 10, :scale => 2
  	change_column :INFORMACION_GEOGRAFICA, :GEO_COORDENADAS_UTM_N_MAX, :decimal, :precision => 10, :scale => 2
  	change_column :INFORMACION_GEOGRAFICA, :GEO_COORDENADAS_UTM_E_MAX, :decimal, :precision => 10, :scale => 2
  end

  def down
  	change_column :INFORMACION_GEOGRAFICA, :GEO_COORDENADAS_UTM_N_MIN, :decimal, :precision => 2, :scale => 2
  	change_column :INFORMACION_GEOGRAFICA, :GEO_COORDENADAS_UTM_E_MIN, :decimal, :precision => 2, :scale => 2
  	change_column :INFORMACION_GEOGRAFICA, :GEO_COORDENADAS_UTM_N_MAX, :decimal, :precision => 2, :scale => 2
  	change_column :INFORMACION_GEOGRAFICA, :GEO_COORDENADAS_UTM_E_MAX, :decimal, :precision => 2, :scale => 2
  end
end
