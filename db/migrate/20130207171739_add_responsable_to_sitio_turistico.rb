class AddResponsableToSitioTuristico < ActiveRecord::Migration
  def up
    change_table :COMUNIDADES do |t|
      t.string :responsable_registro
    end
  end
  def down
    change_table :COMUNIDADES do |t|
      t.remove :responsable_registro
    end
  end
end
