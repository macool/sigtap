class ChangeDecimalAmenaza2 < ActiveRecord::Migration
  def up
  	change_table :AMENAZAS do |t|
      t.decimal :coordenada_norte, :precision => 10, :scale => 2
      t.decimal :coordenada_este, :precision => 10, :scale => 2
      t.decimal :altitud, :precision => 10, :scale => 2

    end
  end

  def down
  	change_table :AMENAZAS do |t|
      t.remove :coordenada_norte
      t.remove :coordenada_este
      t.remove :altitud
      end
  end
end
