class FixFieldsOnComunidades < ActiveRecord::Migration
  def up
    change_table :COMUNIDADES do |t|
      t.boolean :pertenece_a_socio_bosque
      t.remove :COM_HEC_SOCIOPARAMO, :COM_SOCIO_BOSQUE
    end
  end

  def down
    change_table :COMUNIDADES do |t|
      t.float :COM_HEC_SOCIOPARAMO, :precision => 11, :scale => 2
      t.string :COM_SOCIO_BOSQUE, :limit => 4
      t.remove :pertenece_a_socio_bosque
    end
  end
end
