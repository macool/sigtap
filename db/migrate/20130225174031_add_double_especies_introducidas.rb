class AddDoubleEspeciesIntroducidas < ActiveRecord::Migration
  def up
  	change_column :ESPECIES_INTRODUCIDAS, :ESP_INT_EXT_PLANTACION, :decimal, :precision => 10, :scale => 2
  end

  def down
  	  	change_column :ESPECIES_INTRODUCIDAS, :ESP_INT_EXT_PLANTACION, :decimal, :precision => 2, :scale => 0
  end
end
