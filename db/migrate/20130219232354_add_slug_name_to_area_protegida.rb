class AddSlugNameToAreaProtegida < ActiveRecord::Migration
  def up
    add_column :AREAS_PROTEGIDAS, :slug_name, :string
    add_index :AREAS_PROTEGIDAS, :slug_name
    AreaProtegida.find_each do |ap|
      ap.save
    end
  end
  def down
    remove_index :AREAS_PROTEGIDAS, :slug_name
    remove_column :AREAS_PROTEGIDAS, :slug_name, :string
  end
end
