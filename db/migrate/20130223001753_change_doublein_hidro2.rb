class ChangeDoubleinHidro2 < ActiveRecord::Migration
  def up
  	change_column :ACTIVIDADES_HIDROCARBURIFERAS, :ACT_HID_EXPL_DENTRO_AREA, :decimal, :precision => 10, :scale => 2
  end

  def down
  	change_column :ACTIVIDADES_HIDROCARBURIFERAS, :ACT_HID_EXPL_DENTRO_AREA, :decimal, :precision => 4, :scale => 2
  end
end
