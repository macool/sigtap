class AddFieldComunidadesOtros < ActiveRecord::Migration
  def up
    change_table :COMUNIDADES do |t|
      t.string :actividades_otros
    end
  end

  def down
    change_table :COMUNIDADES do |t|
      t.remove :actividades_otros
    end
  end
end
