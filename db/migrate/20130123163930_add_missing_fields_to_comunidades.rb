class AddMissingFieldsToComunidades < ActiveRecord::Migration
  def up
    change_table :COMUNIDADES do |t|
      t.float :coordenada_norte
      t.float :coordenada_este
      t.float :altitud_maxima
      t.float :altitud_minima
    end
  end
  def down
    change_table :COMUNIDADES do |t|
      t.remove :coordenada_norte, :coordenada_este, :altitud_maxima, :altitud_minima
    end
  end
end
