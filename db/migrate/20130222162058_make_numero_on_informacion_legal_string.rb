class MakeNumeroOnInformacionLegalString < ActiveRecord::Migration
  def up
    change_table :INFORMACION_LEGAL do |t|
      t.change :LEGAL_NUMERO, :string
    end
  end

  def down
    change_table :INFORMACION_LEGAL do |t|
      t.change :LEGAL_NUMERO, :integer
    end
  end
end
