class AddResponsableToAmenazas < ActiveRecord::Migration
  def up
  	change_table :AMENAZAS do |t|
      t.string :responsable
    end
  end

  def down
  	change_table :AMENAZAS do |t|
  	  t.remove :responsable
  	end
  end
end
