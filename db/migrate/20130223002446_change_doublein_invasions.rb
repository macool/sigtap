class ChangeDoubleinInvasions < ActiveRecord::Migration
  def up
  	change_column :INVASIONES, :INV_EXTENSION, :decimal, :precision => 10, :scale => 2
  end

  def down
  	change_column :INVASIONES, :INV_EXTENSION, :decimal, :precision => 4, :scale => 2
  end
end
