class AddAltitudesToAmenazas < ActiveRecord::Migration
  def change
    add_column :AMENAZAS, :altitud_maxima, :decimal, :precision => 10, :scale => 2
    add_column :AMENAZAS, :altitud_minima, :decimal, :precision => 10, :scale => 2
  end
end
