class AddMissingFieldsOfUbicacionToSitiosTuristicos < ActiveRecord::Migration
  def up
    change_table :SITIOS_TURISTICOS do |t|
      t.float :coordenada_norte
      t.float :coordenada_este
      t.float :altitud_maxima
      t.float :altitud_minima
      t.string :provincia
      t.string :canton
      t.string :parroquia
    end
  end
  def down
    change_table :SITIOS_TURISTICOS do |t|
      t.remove :coordenada_norte, :coordenada_este, :altitud_maxima, :altitud_minima, :provincia, :canton, :parroquia
    end
  end
end
