class AddNombreDeAdministradorToInformacionAdministrativa < ActiveRecord::Migration
  def up
    change_table :INFORMACION_ADMINISTRATIVA do |t|
      t.string :administrador_actual
    end
  end
  def down
    change_table :INFORMACION_ADMINISTRATIVA do |t|
      t.remove :administrador_actual
    end
  end
end
