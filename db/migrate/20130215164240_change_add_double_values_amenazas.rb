class ChangeAddDoubleValuesAmenazas < ActiveRecord::Migration
  def up
  	change_table :AMENAZAS do |t|
      t.decimal :coordenada_norte, :precision => 6, :scale => 2
      t.decimal :coordenada_este, :precision => 6, :scale => 2
      t.decimal :altitud, :precision => 6, :scale => 2

    end
  end

  def down
  	change_table :AMENAZAS do |t|
      t.remove :coordenada_norte
      t.remove :coordenada_este
      t.remove :altitud
      end
  end
end
