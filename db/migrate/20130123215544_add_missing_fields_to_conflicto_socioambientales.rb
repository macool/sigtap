class AddMissingFieldsToConflictoSocioambientales < ActiveRecord::Migration
  def up
    change_table :CONFLICTOS_SOCIOAMBIENTALES do |t|
      t.string :provincia
      t.string :canton
      t.string :parroquia
      t.string :referencia
    end
  end
  def down
    change_table :CONFLICTOS_SOCIOAMBIENTALES do |t|
      t.remove :provincia, :canton, :parroquia, :referencia
    end
  end
end
