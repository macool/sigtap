class AddEpocaSecaToSitioTuristico < ActiveRecord::Migration
  def up
    change_table :SITIOS_TURISTICOS do |t|
      t.string :epoca_seca
    end
  end
  def down
    change_table :SITIOS_TURISTICOS do |t|
      t.remove :epoca_seca
    end
  end
end
